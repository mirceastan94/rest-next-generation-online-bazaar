package com.ngob.rest.api.test;

import com.ngob.rest.product.dto.user.JwtAuthResponseDto;
import com.ngob.rest.product.dto.user.LoginUserDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

import static com.ngob.rest.product.utils.Constants.*;
import static org.junit.jupiter.api.Assertions.*;

@Testcontainers
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class CoreTests extends PostgreSQLContainer<CoreTests> {

    private static HttpClient httpClient;
    private static ObjectMapper objectMapper;
    private static String jwtToken;

    @Container
    public static PostgreSQLContainer postgreSQLContainer =
            (PostgreSQLContainer) new PostgreSQLContainer("postgres:11.9")
                    .withDatabaseName("product")
                    .withUsername("postgres")
                    .withPassword("postgres")
                    .withStartupTimeout(Duration.ofSeconds(600));

    @DynamicPropertySource
    static void dataSourceProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
        registry.add("spring.datasource.password", postgreSQLContainer::getPassword);
        registry.add("spring.flyway.url", postgreSQLContainer::getJdbcUrl);
        registry.add("spring.flyway.username", postgreSQLContainer::getUsername);
        registry.add("spring.flyway.password", postgreSQLContainer::getPassword);
    }

    @BeforeAll
    public static void initiateClient() {
        httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .followRedirects(HttpClient.Redirect.NEVER)
                .build();
        objectMapper = new ObjectMapper();
    }

    @Test
    void testLoginUser() throws Exception {
        final var loginUserDto = new LoginUserDto(TEST_USER_EMAIL, TEST_USER_PASSWORD);

        HttpRequest loginRequest = HttpRequest.newBuilder()
                .uri(URI.create(LOCAL_LOGIN_ENDPOINT))
                .POST(HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(loginUserDto)))
                .build();
        HttpResponse<String> httpResponse = httpClient.send(loginRequest, HttpResponse.BodyHandlers.ofString());
        JwtAuthResponseDto jwtAuthResponseDto = objectMapper.readValue(httpResponse.body(), JwtAuthResponseDto.class);

        assertTrue(postgreSQLContainer.isRunning());
        assertEquals(200, httpResponse.statusCode());
        assertNotNull(jwtAuthResponseDto);
        assertEquals("Bearer", jwtAuthResponseDto.getTokenType());
        assertTrue(jwtAuthResponseDto.getAccessToken().length() > 100);
    }

}
