DO $$
BEGIN

    -- creates the reviews table, assuming it wasn't created before
    CREATE TABLE IF NOT EXISTS public.reviews(
        id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
        content VARCHAR(25500) NOT NULL,
        rating FLOAT NOT NULL,
        verified_purchase BOOLEAN NOT NULL DEFAULT FALSE,
        date_of_posting DATE NOT NULL DEFAULT CURRENT_DATE,
        product_id uuid NOT NULL,
        user_id uuid NOT NULL,
        CONSTRAINT fk_product
            FOREIGN KEY(product_id)
                REFERENCES products(id)
                ON DELETE SET NULL,
        CONSTRAINT fk_user
            FOREIGN KEY(user_id)
                REFERENCES users(id)
                ON DELETE SET NULL
    );

END $$;