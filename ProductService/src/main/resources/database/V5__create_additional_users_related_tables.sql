DO $$
BEGIN

    -- creates the user details table, assuming it wasn't created before
    CREATE TABLE IF NOT EXISTS public.user_details(
        id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
        phone VARCHAR(255),
        address VARCHAR(255),
        place VARCHAR(255),
        lang VARCHAR(255),
        FOREIGN KEY (id) REFERENCES public.users (id) DEFERRABLE INITIALLY DEFERRED
    );

    -- creates the payment cards table, assuming it wasn't created before
    CREATE TABLE IF NOT EXISTS public.payment_cards(
        id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
        owner_name VARCHAR(255) NOT NULL,
        number VARCHAR(255) NOT NULL,
        expiry_date VARCHAR(255) NOT NULL,
        cvv_cvc_code INTEGER NOT NULL,
        user_details_id uuid NOT NULL,
        CONSTRAINT fk_user_details
            FOREIGN KEY(user_details_id)
                REFERENCES user_details(id)
                ON DELETE SET NULL
    );

    ALTER TABLE public.users
        ADD FOREIGN KEY (id) REFERENCES public.user_details (id) DEFERRABLE INITIALLY DEFERRED;

END $$;