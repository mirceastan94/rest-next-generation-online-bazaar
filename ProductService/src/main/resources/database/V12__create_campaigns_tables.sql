DO $$

BEGIN

    -- creates the products campaigns table, provided they do not exist before hand
    CREATE TABLE IF NOT EXISTS public.campaigns(
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    name VARCHAR(255) NOT NULL,
    start_date DATE NOT NULL DEFAULT CURRENT_DATE,
    end_date DATE NOT NULL DEFAULT CURRENT_DATE + interval '2 days',
    status VARCHAR(255) NOT NULL DEFAULT 'INACTIVE'
    );

    -- creates the join table between campaigns and products if not already existing
    CREATE TABLE IF NOT EXISTS public.campaign_products(
        campaign_id uuid REFERENCES campaigns(id) ON UPDATE CASCADE ON DELETE CASCADE,
        product_id uuid REFERENCES products(id) ON UPDATE CASCADE ON DELETE CASCADE,
        price NUMERIC NOT NULL,
        CONSTRAINT campaign_product_pk PRIMARY KEY(campaign_id, product_id)
    );

END $$;

