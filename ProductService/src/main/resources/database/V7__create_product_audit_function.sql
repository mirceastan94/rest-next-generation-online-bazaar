CREATE OR REPLACE FUNCTION products_audit_trigger_func()
RETURNS trigger AS $body$
BEGIN
   if (TG_OP = 'INSERT') then
       INSERT INTO products_audit_log (
           product_id,
           old_row_data,
           new_row_data,
           dml_type,
           dml_timestamp
       )
       VALUES(
           NEW.id,
           null,
           to_jsonb(NEW),
           'INSERT',
           CURRENT_TIMESTAMP
       );

       RETURN NEW;
   elsif (TG_OP = 'UPDATE') then
       INSERT INTO products_audit_log (
           product_id,
           old_row_data,
           new_row_data,
           dml_type,
           dml_timestamp
       )
       VALUES(
           NEW.id,
           to_jsonb(OLD),
           to_jsonb(NEW),
           'UPDATE',
           CURRENT_TIMESTAMP
       );

       RETURN NEW;
   elsif (TG_OP = 'DELETE') then
       INSERT INTO products_audit_log (
           product_id,
           old_row_data,
           new_row_data,
           dml_type,
           dml_timestamp
       )
       VALUES(
           OLD.id,
           to_jsonb(OLD),
           null,
           'DELETE',
           CURRENT_TIMESTAMP
       );

       RETURN OLD;
   end if;
END;
$body$
LANGUAGE plpgsql
