DO $$
BEGIN

    -- updates user_details table for picture processing
    ALTER TABLE public.user_details ADD COLUMN IF NOT EXISTS picture bytea;
    ALTER TABLE public.user_details ADD COLUMN IF NOT EXISTS picture_str text;

END $$;