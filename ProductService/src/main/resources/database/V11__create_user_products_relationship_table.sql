DO $$
BEGIN

    -- creates the users products relationship table, assuming it wasn't created before
    CREATE TABLE IF NOT EXISTS public.user_products(
        id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
        user_id uuid,
        product_id uuid
    );

END $$;