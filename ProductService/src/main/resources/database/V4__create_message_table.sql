DO $$
BEGIN

    -- creates the messages table, assuming it wasn't created before
    CREATE TABLE IF NOT EXISTS public.messages(
        id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
        name VARCHAR(255) NOT NULL,
        subject VARCHAR(255) NOT NULL,
        email VARCHAR(255) NOT NULL,
        phone VARCHAR(255) NOT NULL,
        content VARCHAR(255) NOT NULL
    );

END $$;