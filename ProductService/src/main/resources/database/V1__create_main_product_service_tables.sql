DO $$
BEGIN

    -- creates the UUID generation extension
    CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

    -- creates the public schema, in case it was somehow deleted
    CREATE SCHEMA IF NOT EXISTS public;

    -- creates the product table, assuming it wasn't created before
    CREATE TABLE IF NOT EXISTS public.products(
        id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
        name VARCHAR(255) NOT NULL,
        description VARCHAR(255) NOT NULL,
        category VARCHAR(255) NOT NULL,
        sub_category VARCHAR(255) NOT NULL,
        stock_count INTEGER NOT NULL,
        brand VARCHAR(255) NOT NULL,
        price INTEGER NOT NULL,
        discount_price INTEGER,
        rating FLOAT NOT NULL,
        image_location VARCHAR(255) NOT NULL,
        popularity VARCHAR(255) NOT NULL,
        introductory_date DATE NOT NULL DEFAULT CURRENT_DATE,
        is_headline BOOLEAN NOT NULL DEFAULT false
    );

END $$;