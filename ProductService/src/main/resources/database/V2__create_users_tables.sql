DO $$
BEGIN

    -- creates the roles table, assuming it wasn't created before
    CREATE TABLE IF NOT EXISTS public.roles(
        id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
        name VARCHAR(255) NOT NULL
    );

    -- creates the users table, assuming it wasn't created before
    CREATE TABLE IF NOT EXISTS public.users(
        id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
        name VARCHAR(255) NOT NULL,
        previous_email VARCHAR(255),
        email VARCHAR(255) NOT NULL,
        password VARCHAR(255) NOT NULL,
        role_id uuid NOT NULL,
        CONSTRAINT fk_role
            FOREIGN KEY(role_id)
                REFERENCES roles(id)
                ON DELETE SET NULL
    );

END $$;