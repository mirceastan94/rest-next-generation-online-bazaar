DO $$
BEGIN

CREATE TYPE dml_type AS ENUM ('INSERT', 'UPDATE', 'DELETE');

CREATE TABLE IF NOT EXISTS products_audit_log (
    product_id uuid NOT NULL,
    old_row_data jsonb,
    new_row_data jsonb,
    dml_type dml_type NOT NULL,
    dml_timestamp timestamp NOT NULL,
    PRIMARY KEY (product_id, dml_type, dml_timestamp)
);

END $$;
