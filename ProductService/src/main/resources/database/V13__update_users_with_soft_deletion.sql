DO $$
BEGIN

    -- updates the users table
    ALTER TABLE public.users ADD COLUMN IF NOT EXISTS is_deleted bool NOT NULL DEFAULT false;

END $$;