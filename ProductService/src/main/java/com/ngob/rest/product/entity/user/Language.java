package com.ngob.rest.product.entity.user;

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum Language {
    ENGLISH("en"),
    GERMAN("de"),
    ROMANIAN("ro");

    private String label;

    Language(String label) {
        this.label = label;
    }

    public static Language fromLabel(String inputLabel) {
        return Arrays.stream(values()).filter(language ->
                language.label.equalsIgnoreCase(inputLabel)).findFirst().orElse(Language.ENGLISH);
    }

}
