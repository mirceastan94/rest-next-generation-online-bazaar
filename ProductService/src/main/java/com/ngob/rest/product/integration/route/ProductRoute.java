package com.ngob.rest.product.integration.route;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.LoggingLevel;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This is the product route class
 */
@Slf4j
@Component
public class ProductRoute extends BaseRouteBuilder {

    @Autowired
    private Queue productRetrievalQueue;

    public static final String ROUTE_ID = "product-route";
    public static final String ROUTE_PATH = "direct:" + ROUTE_ID;

    @Override
    public void configure() throws Exception {
        super.configure();

        from(ROUTE_PATH)
                .threads(5)
                .doTry()
                .log(LoggingLevel.INFO, log, "Message received via: " + ROUTE_PATH)
                .process("productProcessor")
                .log(LoggingLevel.INFO, log, "Product entity persisted in DB")
                .setHeader("rabbitmq.ROUTING_KEY", constant(productRetrievalQueue.getName()))
                .to("rabbitmq:products.direct?queue=" + productRetrievalQueue.getName()
                    + "&routingKey=" + productRetrievalQueue.getName()
                    +"&autoDelete=false&autoAck=true&BridgeEndpoint=true")
                .end();
    }

}
