package com.ngob.rest.product.dto.product;

import com.ngob.rest.product.integration.utils.ProductObj;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

/**
 * This is the Dto class for Product entities
 *
 * @author Mircea Stan
 */
@Getter
@Setter
@EqualsAndHashCode(of = {"id", "name", "description", "category", "subCategory"})
public class ProductDto extends ProductObj {

    private UUID id;

    @NotBlank
    @Size(max = 255)
    private String name;

    @NotBlank
    @Size(max = 255)
    private String description;

    @NotBlank
    @Size(max = 255)
    private String category;

    @NotBlank
    @Size(max = 255)
    private String subCategory;

    @NotNull
    private Integer stockCount;

    @NotBlank
    @Size(max = 255)
    private String brand;

    @NotNull
    private Integer price;

    private Integer discountPrice;

    private Double rating;

    @NotBlank
    @Size(max = 255)
    private String imageData;

    @NotBlank
    @Size(max = 255)
    private String imageLocation;

    @NotBlank
    private String popularity;

    @NotNull
    private String introductoryDate;

    private Integer reviewsCount = 0;

    @NotNull
    private Boolean isHeadline;

}
