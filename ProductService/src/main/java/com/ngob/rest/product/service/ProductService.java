package com.ngob.rest.product.service;

import com.ngob.rest.product.dto.CriteriaDto;
import com.ngob.rest.product.dto.product.ProductCategoryDto;
import com.ngob.rest.product.dto.product.ProductDto;
import com.ngob.rest.product.integration.utils.ProductObj;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * This is the Product Service interface, used to store, retrieve and update products
 *
 * @author Mircea Stan
 */
public interface ProductService {

    void integrateNewProducts() throws IOException;

    ProductDto getProduct(UUID productId);

    ProductDto getProductWithCampaign(UUID productId, UUID campaignId);

    Long countProductsByCriteria(CriteriaDto criteriaDto);

    List<ProductDto> getProductsByCriteria(CriteriaDto criteriaDto);

    String insertOrUpdateProductFromObj(ProductObj productObj);

    void cacheCurrentProducts();

    List<ProductDto> getCachedProducts();

    Long getProductsCount();

    List<ProductCategoryDto> getProductsCategoriesWithCount();
}
