package com.ngob.rest.product.integration.redis;

/**
 * This is the Redis message publisher interface
 *
 * @author Mircea Stan
 */
public interface MessagePublisher {

    void publish(final String message);

}
