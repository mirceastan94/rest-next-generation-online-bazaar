package com.ngob.rest.product.integration.route;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;

/**
 * This is the Camel BaseRouterBuilder class
 *
 * @author Mircea Stan
 */
public class BaseRouteBuilder extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        errorHandler(deadLetterChannel("mock:dead")
                .maximumRedeliveries(1)
                .redeliveryDelay(1000)
                .retryAttemptedLogLevel(LoggingLevel.WARN));
    }

}
