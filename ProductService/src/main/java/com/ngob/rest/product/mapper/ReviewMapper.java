package com.ngob.rest.product.mapper;

import com.ngob.rest.product.dto.review.ReviewDto;
import com.ngob.rest.product.dto.review.ReviewSummaryDto;
import com.ngob.rest.product.dto.user.UserDto;
import com.ngob.rest.product.entity.Review;
import com.ngob.rest.product.entity.UserProducts;
import com.ngob.rest.product.repository.ProductRepository;
import com.ngob.rest.product.repository.UserProductsRepository;
import com.ngob.rest.product.repository.user.UserRepository;
import com.ngob.rest.product.utils.ReviewsWrapper;
import lombok.SneakyThrows;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.elasticsearch.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * This is the custom mapper for review related conversions
 */
@Component
public class ReviewMapper extends ConfigurableMapper {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserProductsRepository userProductsRepository;

    @Override
    protected void configure(MapperFactory mapperFactory) {

        mapperFactory.classMap(Review.class, ReviewDto.class).byDefault().customize(new CustomMapper<>() {
            @SneakyThrows
            @Override
            public void mapAtoB(Review review, ReviewDto reviewDto, MappingContext context) {
                super.mapAtoB(review, reviewDto, context);
                reviewDto.setProductId(review.getProduct().getId());
                reviewDto.setProductName(review.getProduct().getName());
                reviewDto.setUserDto(userMapper.map(review.getUser(), UserDto.class));
            }
            @Override
            public void mapBtoA(ReviewDto reviewDto, Review review, MappingContext context) {
                super.mapBtoA(reviewDto, review, context);
                final var product = productRepository.findById(reviewDto.getProductId()).orElseThrow(()
                        -> new ResourceNotFoundException("Product", "ID", reviewDto.getProductId()));
                final var user = userRepository.findByEmail(reviewDto.getUserDto().getEmail()).orElseThrow(()
                        -> new ResourceNotFoundException("User", "Email", reviewDto.getProductId()));
                review.setProduct(product);
                review.setUser(user);
                final List<UserProducts> userProducts = userProductsRepository.findByUserId(reviewDto.getUserDto()
                                .getId()).orElse(new ArrayList<>());
                review.setVerifiedPurchase(userProducts.stream().map(UserProducts::getProductId)
                        .anyMatch(productId -> productId.equals(product.getId())));
            }
        }).register();

        mapperFactory.classMap(ReviewsWrapper.class, ReviewSummaryDto.class).field("reviews", "reviewDtos")
                .byDefault().customize(new CustomMapper<>() {
            @SneakyThrows
            @Override
            public void mapAtoB(ReviewsWrapper reviewsWrapper, ReviewSummaryDto reviewDto, MappingContext context) {
                var reviews = reviewsWrapper.getReviews();
                reviewDto.setReviewsCount(reviews.size());
                reviewDto.setAverageRating(reviews.stream().mapToDouble(Review::getRating).average().getAsDouble());
                reviewDto.setExcellentRatingCount(Math.toIntExact(reviews.stream().mapToDouble(Review::getRating).filter(rating ->
                        rating == 5.0D).count()));
                reviewDto.setGoodRatingCount(Math.toIntExact(reviews.stream().mapToDouble(Review::getRating).filter(rating ->
                        rating == 4.0D).count()));
                reviewDto.setAverageRatingCount(Math.toIntExact(reviews.stream().mapToDouble(Review::getRating).filter(rating ->
                        rating == 3.0D).count()));
                reviewDto.setBadRatingCount(Math.toIntExact(reviews.stream().mapToDouble(Review::getRating).filter(rating ->
                        rating == 2.0D).count()));
                reviewDto.setHorribleRatingCount(Math.toIntExact(reviews.stream().mapToDouble(Review::getRating).filter(rating ->
                        rating == 1.0D).count()));
            }
        }).register();
    }

}
