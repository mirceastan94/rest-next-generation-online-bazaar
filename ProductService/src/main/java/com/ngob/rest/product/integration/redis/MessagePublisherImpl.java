package com.ngob.rest.product.integration.redis;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.stereotype.Service;

/**
 * This is the Redis message publisher implementation class
 *
 * @author Mircea Stan
 */
@Service
public class MessagePublisherImpl implements MessagePublisher {

    private RedisTemplate<String,Object> redisTemplate;
    private ChannelTopic channelTopic;

    public MessagePublisherImpl(RedisTemplate<String,Object> redisTemplate, ChannelTopic channelTopic) {
        this.redisTemplate = redisTemplate;
        this.channelTopic = channelTopic;
    }

    public void publish(final String message) {
        redisTemplate.convertAndSend(channelTopic.getTopic(), message);
    }

}
