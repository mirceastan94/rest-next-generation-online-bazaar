package com.ngob.rest.product.entity;

import com.ngob.rest.product.dto.UserProductsDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

/**
 * This is the user products relationship entity class
 *
 * @author Mircea Stan
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserProducts {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(updatable = false, unique = true)
    private UUID id;

    private UUID userId;
    private UUID productId;

    public UserProducts(UUID userId, UUID productId) {
        this.userId = userId;
        this.productId = productId;
    }

}
