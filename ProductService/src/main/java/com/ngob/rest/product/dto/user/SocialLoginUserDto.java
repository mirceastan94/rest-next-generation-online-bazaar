package com.ngob.rest.product.dto.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * This is the social login user DTO
 *
 * @author Mircea Stan
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class SocialLoginUserDto {

    @NotBlank
    private String provider;
    private String id;

    @NotBlank
    @Email
    private String email;
    private String name;
    private String photoUrl;
    private String firstName;
    private String lastName;

    @NotBlank
    private String authToken;

    private String language;
    private String idToken;
    private String authorizationCode;
    private String password;

}
