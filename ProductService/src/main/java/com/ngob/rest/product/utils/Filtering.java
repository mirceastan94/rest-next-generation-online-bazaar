package com.ngob.rest.product.utils;

import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.DatePath;
import com.querydsl.core.types.dsl.NumberPath;
import org.springframework.data.querydsl.QSort;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;

import static com.ngob.rest.product.utils.Constants.*;
import static com.ngob.rest.product.utils.ProductPageableStaticFields.*;

/**
 * This class generates the filtering logic for criteria based products search
 */
public class Filtering {

    private Filtering() {
    }

    public static QSort createOrderSpecifier(Map<String, Expression<? extends Comparable>> orderingFields,
                                             String orderByField, Order order) {
        return new QSort(new OrderSpecifier<>(order, orderingFields.get(orderByField)));
    }

    public static Function<String, BooleanExpression> getDateFilteringFunction(DatePath<LocalDate> filteredField) {
        return criteria -> {
            if (criteria.contains(COMMA)) {
                String[] datesSplitted = criteria.split(COMMA);
                return filteredField.between(LocalDate.parse(datesSplitted[0], DateTimeFormatter.ofPattern(DATE_PATTERN)),
                        LocalDate.parse(datesSplitted[1], DateTimeFormatter.ofPattern(DATE_PATTERN)).plusDays(1));
            } else {
                return filteredField.after(LocalDate.parse(criteria));
            }
        };
    }

    public static Function<String, BooleanExpression> getPriceFilteringFunction(NumberPath<Integer> filteredField) {
        return criteria -> {
            if (criteria.contains(COMMA)) {
                String[] pricesSplitted = criteria.split(COMMA);
                return filteredField.between(Integer.parseInt(pricesSplitted[0]), Integer.parseInt(pricesSplitted[1]));
            } else {
                return filteredField.eq(Integer.parseInt(criteria));
            }
        };
    }

    public static BooleanExpression createProductsFilter(Map<String, String> filterCriteria,
                                                         Map<String, Function<String, BooleanExpression>> filteringFieldsMap) {
        BooleanExpression booleanExpression = null;
        final Set<Map.Entry<String, String>> filterCriteriaEntrySet = filterCriteria.entrySet();
        filterCriteriaEntrySet.removeIf(filterCriteriaEntry -> Objects.isNull(filterCriteriaEntry.getValue()));
        for (Map.Entry<String, String> filterCriteriaEntry : filterCriteriaEntrySet) {
            final var filterCriteriaField = filterCriteriaEntry.getKey();
            Map<String, List<String>> filterCriteriaValuesConfigMap = getFilterCriteriaValuesConfig(filterCriteriaEntry);
            var filterCriteriaValues = Objects.nonNull(filterCriteriaValuesConfigMap.get(filterCriteriaField))
                    ? filterCriteriaValuesConfigMap.get(filterCriteriaField)
                    : Arrays.asList(filterCriteriaEntry.getValue().toLowerCase().replaceAll(DASH, SPACE).split(COMMA));
            for (final String currentFilterCriteria : filterCriteriaValues) {
                if (Objects.nonNull(booleanExpression)) {
                    booleanExpression = (filterCriteriaValues.size() > 1)
                            ? booleanExpression.or(filteringFieldsMap.get(filterCriteriaField).apply(currentFilterCriteria.trim()))
                            : booleanExpression.and(filteringFieldsMap.get(filterCriteriaField).apply(currentFilterCriteria.trim()));
                } else {
                    booleanExpression = filteringFieldsMap.get(filterCriteriaField).apply(currentFilterCriteria.trim());
                }
            }
        }
        return booleanExpression;
    }

    public static BooleanExpression createUsersFilter(Map<String, String> filterCriteria,
                                                         Map<String, Function<String, BooleanExpression>> filteringFieldsMap) {
        BooleanExpression booleanExpression = null;
        final Set<Map.Entry<String, String>> filterCriteriaEntrySet = filterCriteria.entrySet();
        filterCriteriaEntrySet.removeIf(filterCriteriaEntry -> Objects.isNull(filterCriteriaEntry.getValue()));
        for (Map.Entry<String, String> filterCriteriaEntry : filterCriteriaEntrySet) {
            final var filterCriteriaField = filterCriteriaEntry.getKey();
            var filterCriteriaValues = Arrays.asList(filterCriteriaEntry.getValue().toLowerCase().split(COMMA));
            for (final String currentFilterCriteria : filterCriteriaValues) {
                if (Objects.nonNull(booleanExpression)) {
                    booleanExpression = (filterCriteriaValues.size() > 1)
                            ? booleanExpression.or(filteringFieldsMap.get(filterCriteriaField).apply(currentFilterCriteria.trim()))
                            : booleanExpression.and(filteringFieldsMap.get(filterCriteriaField).apply(currentFilterCriteria.trim()));
                } else {
                    booleanExpression = filteringFieldsMap.get(filterCriteriaField).apply(currentFilterCriteria.trim());
                }
            }
        }
        return booleanExpression;
    }

    public static String buildLikeString(String criteria) {
        return PERCENT.concat(criteria).concat(PERCENT);
    }

    public static Map<String, List<String>> getFilterCriteriaValuesConfig(Map.Entry<String, String> filterCriteriaEntry) {
        return Map.of(PRODUCT_INTRODUCTORY_DATE, Collections.singletonList(filterCriteriaEntry.getValue().toLowerCase()),
                PRODUCT_PRICE, Collections.singletonList(filterCriteriaEntry.getValue().toLowerCase()),
                PRODUCT_BRAND, Arrays.asList(filterCriteriaEntry.getValue().toLowerCase().split(COMMA)));
    }
}
