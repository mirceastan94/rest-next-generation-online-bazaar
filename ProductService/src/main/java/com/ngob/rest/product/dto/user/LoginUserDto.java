package com.ngob.rest.product.dto.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * This is the login request class
 *
 * @author Mircea Stan
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LoginUserDto {

    @NotBlank
    @Email
    private String email;

    @NotBlank
    private String password;

}
