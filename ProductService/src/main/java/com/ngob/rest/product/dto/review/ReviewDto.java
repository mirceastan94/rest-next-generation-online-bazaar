package com.ngob.rest.product.dto.review;

import com.ngob.rest.product.dto.user.UserDto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.UUID;

/**
 * This is the Dto class for Review entities
 *
 * @author Mircea Stan
 */
@Getter
@Setter
public class ReviewDto {

    private UUID id;

    @NotBlank
    private String content;

    @NotNull
    private LocalDate dateOfPosting;

    @NotNull
    private Double rating;

    @NotNull
    private Boolean verifiedPurchase;

    @NotNull
    private UserDto userDto;

    @NotBlank
    @Size(max = 255)
    private UUID productId;

    private String productName;

}
