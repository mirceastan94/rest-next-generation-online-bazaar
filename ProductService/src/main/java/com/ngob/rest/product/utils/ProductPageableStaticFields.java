package com.ngob.rest.product.utils;

import com.ngob.rest.product.entity.QProduct;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.dsl.BooleanExpression;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;

import static com.ngob.rest.product.utils.Filtering.*;

/**
 * This is the class which adds the pageable static fields in QueryDSL based queries for products entities
 *
 * @author Mircea Stan
 */
public class ProductPageableStaticFields {

    private ProductPageableStaticFields() {
    }

    private static final String PRODUCT_ID = "id";
    private static final String PRODUCT_NAME = "name";
    private static final String PRODUCT_DESCRIPTION = "description";
    private static final String PRODUCT_CATEGORY = "category";
    private static final String PRODUCT_SUBCATEGORY = "subcategory";
    private static final String PRODUCT_STOCK_COUNT = "stockCount";
    public static final String PRODUCT_BRAND = "brand";
    public static final String PRODUCT_PRICE = "price";
    private static final String PRODUCT_DISCOUNT_PRICE = "discountPrice";
    private static final String PRODUCT_POPULARITY = "popularity";
    public static final String PRODUCT_INTRODUCTORY_DATE = "introductoryDate";
    private static final String PRODUCT_IS_HEADLINE = "isHeadline";

    public static final Map<String, Expression<? extends Comparable>> sortingFieldsMap = new HashMap<>();
    public static final Map<String, Function<String, BooleanExpression>> filteringFieldsMap = new HashMap<>();

    static {
        sortingFieldsMap.put(null, QProduct.product.id);
        sortingFieldsMap.put(PRODUCT_ID, QProduct.product.id);
        sortingFieldsMap.put(PRODUCT_NAME, QProduct.product.name);
        sortingFieldsMap.put(PRODUCT_DESCRIPTION, QProduct.product.description);
        sortingFieldsMap.put(PRODUCT_CATEGORY, QProduct.product.category);
        sortingFieldsMap.put(PRODUCT_SUBCATEGORY, QProduct.product.subCategory);
        sortingFieldsMap.put(PRODUCT_STOCK_COUNT, QProduct.product.stockCount);
        sortingFieldsMap.put(PRODUCT_BRAND, QProduct.product.brand);
        sortingFieldsMap.put(PRODUCT_PRICE, QProduct.product.price);
        sortingFieldsMap.put(PRODUCT_DISCOUNT_PRICE, QProduct.product.discountPrice);
        sortingFieldsMap.put(PRODUCT_POPULARITY, QProduct.product.popularity);
        sortingFieldsMap.put(PRODUCT_INTRODUCTORY_DATE, QProduct.product.introductoryDate);
        sortingFieldsMap.put(PRODUCT_IS_HEADLINE, QProduct.product.isHeadline);
    }

    static {
        filteringFieldsMap.put(PRODUCT_ID, criteria -> QProduct.product.id.eq(UUID.fromString(criteria)));
        filteringFieldsMap.put(PRODUCT_NAME, criteria -> QProduct.product.name.toLowerCase().like(buildLikeString(criteria)));
        filteringFieldsMap.put(PRODUCT_DESCRIPTION, criteria -> QProduct.product.description.toLowerCase().like(buildLikeString(criteria)));
        filteringFieldsMap.put(PRODUCT_CATEGORY, criteria -> QProduct.product.category.toLowerCase().like(buildLikeString(criteria)));
        filteringFieldsMap.put(PRODUCT_SUBCATEGORY, criteria -> QProduct.product.subCategory.toLowerCase().like(buildLikeString(criteria)));
        filteringFieldsMap.put(PRODUCT_STOCK_COUNT, criteria -> QProduct.product.stockCount.eq(Integer.parseInt(criteria)));
        filteringFieldsMap.put(PRODUCT_BRAND, criteria -> QProduct.product.brand.toLowerCase().like(buildLikeString(criteria)));
        filteringFieldsMap.put(PRODUCT_PRICE, getPriceFilteringFunction(QProduct.product.price));
        filteringFieldsMap.put(PRODUCT_DISCOUNT_PRICE, getPriceFilteringFunction(QProduct.product.discountPrice));
        filteringFieldsMap.put(PRODUCT_POPULARITY, criteria -> QProduct.product.popularity.stringValue().toLowerCase().like(buildLikeString(criteria)));
        filteringFieldsMap.put(PRODUCT_INTRODUCTORY_DATE, getDateFilteringFunction(QProduct.product.introductoryDate));
        filteringFieldsMap.put(PRODUCT_IS_HEADLINE, criteria -> QProduct.product.isHeadline.eq(Boolean.valueOf(criteria)));
    }

}
