package com.ngob.rest.product.mapper;

import com.ngob.rest.product.dto.product.ProductDto;
import com.ngob.rest.product.entity.Popularity;
import com.ngob.rest.product.entity.Product;
import lombok.SneakyThrows;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.time.LocalDate;
import java.util.Base64;
import java.util.Objects;

/**
 * This is the custom mapper for product related conversions
 */
@Component
public class ProductMapper extends ConfigurableMapper {

    @Override
    protected void configure(MapperFactory mapperFactory) {

        mapperFactory.classMap(Product.class, ProductDto.class).byDefault().customize(new CustomMapper<>() {
            @SneakyThrows
            @Override
            public void mapAtoB(Product product, ProductDto productDto, MappingContext context) {
                super.mapAtoB(product, productDto, context);
                InputStream is = getClass().getClassLoader().getResourceAsStream(product.getImageLocation());
                productDto.setImageData(Base64.getEncoder().encodeToString(Objects.requireNonNull(is).readAllBytes()));
                productDto.setReviewsCount(product.getReviews().size());
                productDto.setIntroductoryDate(product.getIntroductoryDate().toString());
            }
            @Override
            public void mapBtoA(ProductDto productDto, Product product, MappingContext context) {
                super.mapBtoA(productDto, product, context);
                product.setPopularity(Popularity.valueOf(productDto.getPopularity()));
                product.setIntroductoryDate(LocalDate.parse(productDto.getIntroductoryDate()));
            }
        }).register();
    }

}
