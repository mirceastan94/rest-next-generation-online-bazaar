package com.ngob.rest.product.dto.user;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * This is the JWTAuthentication response class
 *
 * @author Mircea Stan
 */
@RequiredArgsConstructor
@Getter
@Setter
public class JwtAuthResponseDto {

    @NonNull
    private final String accessToken;

    @NonNull
    private final String expiryDate;

    @NonNull
    private final UserDto userDto;

    private final String tokenType = "Bearer";

}
