package com.ngob.rest.product.entity;

/**
 * This is the popularity enum
 *
 * @author Mircea Stan
 */
public enum Popularity {
    Low,
    Medium,
    High
}
