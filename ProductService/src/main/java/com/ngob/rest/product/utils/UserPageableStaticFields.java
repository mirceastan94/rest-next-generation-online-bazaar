package com.ngob.rest.product.utils;

import com.ngob.rest.product.entity.user.QUser;
import com.ngob.rest.product.entity.user.RoleName;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.dsl.BooleanExpression;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;

import static com.ngob.rest.product.utils.Filtering.buildLikeString;

/**
 * This is the class which adds the pageable static fields in QueryDSL based queries for users entities
 *
 * @author Mircea Stan
 */
public class UserPageableStaticFields {

    private UserPageableStaticFields() {
    }

    private static final String USER_ID = "id";
    private static final String USER_NAME = "name";
    private static final String EMAIL = "email";
    private static final String PREVIOUS_EMAIL = "previousEmail";
    private static final String ROLE = "role";
    private static final String IS_DELETED = "isDeleted";

    public static final Map<String, Expression<? extends Comparable>> sortingFieldsMap = new HashMap<>();
    public static final Map<String, Function<String, BooleanExpression>> filteringFieldsMap = new HashMap<>();

    static {
        sortingFieldsMap.put(null, QUser.user.id);
        sortingFieldsMap.put(USER_ID, QUser.user.id);
        sortingFieldsMap.put(USER_NAME, QUser.user.name);
        sortingFieldsMap.put(EMAIL, QUser.user.email);
        sortingFieldsMap.put(ROLE, QUser.user.role.name);
        sortingFieldsMap.put(IS_DELETED, QUser.user.isDeleted);
    }

    static {
        filteringFieldsMap.put(USER_ID, criteria -> QUser.user.id.eq(UUID.fromString(criteria)));
        filteringFieldsMap.put(USER_NAME, criteria -> QUser.user.name.toLowerCase().like(buildLikeString(criteria)));
        filteringFieldsMap.put(EMAIL, criteria -> QUser.user.email.toLowerCase().like(buildLikeString(criteria)));
        filteringFieldsMap.put(PREVIOUS_EMAIL, criteria -> QUser.user.previousEmail.toLowerCase().like(buildLikeString(criteria)));
        filteringFieldsMap.put(ROLE, criteria -> QUser.user.role.name.eq(RoleName.valueOf(criteria)));
        filteringFieldsMap.put(IS_DELETED, criteria -> QUser.user.isDeleted.eq(Boolean.valueOf(criteria)));
    }

}
