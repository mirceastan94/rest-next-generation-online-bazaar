package com.ngob.rest.product.service;

import com.google.gson.Gson;
import com.ngob.rest.product.dto.review.ReviewDto;
import com.ngob.rest.product.dto.review.ReviewSummaryDto;
import com.ngob.rest.product.entity.Review;
import com.ngob.rest.product.mapper.ReviewMapper;
import com.ngob.rest.product.repository.ProductRepository;
import com.ngob.rest.product.repository.ReviewRepository;
import com.ngob.rest.product.utils.ReviewsWrapper;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

/**
 * This is the Product service class, used to review products
 *
 * @author Mircea Stan
 */
@Service
@Transactional
@Log4j2
public class ReviewServiceImpl implements ReviewService {

    private final ReviewRepository reviewRepository;
    private final ProductRepository productRepository;
    private final ReviewMapper reviewMapper;

    ReviewServiceImpl(ReviewRepository reviewRepository, ProductRepository productRepository,
                      ReviewMapper reviewMapper) {
        this.reviewRepository = reviewRepository;
        this.productRepository = productRepository;
        this.reviewMapper = reviewMapper;
    }

    @Override
    public ReviewSummaryDto getReviewsWithStats(UUID productId) {
        log.info(String.format("Retrieving product with ID '%s' ...", productId));
        final var product = this.productRepository.findById(productId)
                .orElseThrow(() -> new ResourceNotFoundException("Product", "ID", productId));
        final Set<Review> reviews = product.getReviews();

        if (reviews.isEmpty()) {
            log.info(String.format("No reviews for product with ID '%s'", productId));
            return new ReviewSummaryDto();
        }

        log.info(String.format("Mapping reviews for product with ID '%s' ...", productId));
        return reviewMapper.map(new ReviewsWrapper(reviews), ReviewSummaryDto.class);
    }

    @Override
    public Integer getReviewsCount(UUID productID) {
        return getReviewsWithStats(productID).getReviewsCount();
    }

    @Override
    public String publishReview(ReviewDto reviewDto) {
        log.info(String.format("Mapping newReview for reviewedProduct with ID '%s' ...", reviewDto.getProductId()));
        final var newReview = reviewMapper.map(reviewDto, Review.class);

        final var reviewedProduct = productRepository.findById(reviewDto.getProductId()).orElseThrow(()
                -> new ResourceNotFoundException("Product", "ID", reviewDto.getProductId()));

        log.info(String.format("Adding new review and recalculating rating for product with ID '%s' ...",
                reviewedProduct.getId()));
        final var existingReviews = reviewedProduct.getReviews();
        existingReviews.add(newReview);
        reviewedProduct.setRating(existingReviews.stream().mapToDouble(Review::getRating).average().getAsDouble());

        log.info(String.format("Inserting new review and rating recalculation update for product with ID '%s' ...",
                reviewedProduct.getId()));
        productRepository.save(reviewedProduct);

        return new Gson().toJson(newReview.getId());
    }

    @Override
    public LinkedHashSet<ReviewDto> getRecentReviews() {
        LinkedHashSet<Review> recentReviews = reviewRepository.findTop5ByOrderByDateOfPostingDesc();
        final var reviewDtos = new LinkedHashSet<ReviewDto>();
        recentReviews.forEach(review -> reviewDtos.add(reviewMapper.map(review, ReviewDto.class)));
        return reviewDtos;
    }

}
