package com.ngob.rest.product.mapper;

import com.ngob.rest.product.dto.contact.ContactMessageDto;
import com.ngob.rest.product.dto.user.*;
import com.ngob.rest.product.entity.contact.Message;
import com.ngob.rest.product.entity.user.*;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * This is the custom mapper for user related conversions
 */
@Component
@Slf4j
public class UserMapper extends ConfigurableMapper {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    protected void configure(MapperFactory mapperFactory) {

        mapperFactory.classMap(RegisterUserDto.class, User.class).byDefault().customize(new CustomMapper<>() {
            @Override
            public void mapAtoB(RegisterUserDto userDto, User user, MappingContext context) {
                super.mapAtoB(userDto, user, context);
                user.setUserPassword(passwordEncoder.encode(userDto.getPassword()));
            }
        }).register();

        mapperFactory.classMap(RoleDto.class, Role.class).byDefault().customize(new CustomMapper<>() {

            @Override
            public void mapAtoB(RoleDto roleDto, Role role, MappingContext context) {
                role.setId(roleDto.getId());
                role.setName(RoleName.valueOf(roleDto.getName()));
            }

            @Override
            public void mapBtoA(Role role, RoleDto roleDto, MappingContext context) {
                roleDto.setId(role.getId());
                roleDto.setName(role.getName().name());
            }
        }).register();

        mapperFactory.classMap(UserDto.class, User.class)
                .field("userDetailsDto", "userDetails")
                .byDefault().customize(new CustomMapper<>() {

            @Override
            public void mapAtoB(UserDto userDto, User user, MappingContext context) {
                super.mapAtoB(userDto, user, context);
                if (Objects.nonNull(userDto.getPassword())) {
                    user.setUserPassword(passwordEncoder.encode(userDto.getPassword()));
                } else {
                    user.setUserPassword(user.getUserPassword());
                }
            }

            @Override
            public void mapBtoA(User user, UserDto userDto, MappingContext context) {
                super.mapBtoA(user, userDto, context);
                userDto.setPassword(null);
                userDto.setRole(user.getRole().getName().name());
            }
        }).register();

        mapperFactory.classMap(SocialLoginUserDto.class, RegisterUserDto.class).byDefault().register();
        mapperFactory.classMap(UserDetailsDto.class, UserDetails.class).fieldMap("picture", "language").exclude()
                .add().byDefault().customize(new CustomMapper<>() {

            @Override
            public void mapAtoB(UserDetailsDto userDetailsDto, UserDetails userDetails, MappingContext context) {
                super.mapAtoB(userDetailsDto, userDetails, context);
                if (Objects.nonNull(userDetailsDto.getPaymentCardDto()) && Objects.nonNull(userDetailsDto.getPaymentCardDto().getNumber())) {
                    Set<PaymentCard> paymentCards = mapperFacade.mapAsSet(new HashSet<>(Collections.singletonList(userDetailsDto.getPaymentCardDto())),
                            PaymentCard.class);
                    paymentCards.forEach(paymentCard -> paymentCard.setUserDetails(userDetails));
                    userDetails.getPaymentCards().clear();
                    userDetails.getPaymentCards().addAll(paymentCards);
                }
                if (Objects.nonNull(userDetailsDto.getPicture())) {
                    userDetails.setPictureStr(userDetailsDto.getPicture());
                }
            }

            @Override
            public void mapBtoA(UserDetails userDetails, UserDetailsDto userDetailsDto, MappingContext context) {
                super.mapBtoA(userDetails, userDetailsDto, context);
                if (Objects.nonNull(userDetails.getPaymentCards()) && !userDetails.getPaymentCards().isEmpty()) {
                    var paymentCardDto = mapperFacade.map(userDetails.getPaymentCards().stream().findFirst().get(),
                            PaymentCardDto.class);
                    userDetailsDto.setPaymentCardDto(paymentCardDto);
                }
                if (Objects.nonNull(userDetails.getPicture())) {
                    userDetailsDto.setPicture(userDetails.getPictureStr());
                }
                userDetailsDto.setLanguage(userDetails.getLanguage().getLabel());
            }
        }).register();

        mapperFactory.classMap(PaymentCardDto.class, PaymentCard.class).byDefault().register();
        mapperFactory.classMap(PaymentCard.class, PaymentCardDto.class).byDefault().register();
        mapperFactory.classMap(ContactMessageDto.class, Message.class).byDefault().register();

    }

}
