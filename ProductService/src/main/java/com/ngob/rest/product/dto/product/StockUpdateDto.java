package com.ngob.rest.product.dto.product;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

/**
 * This is the product(s) stock count update Dto class
 *
 * @author Mircea Stan
 */
@Getter
@Setter
@ToString
public class StockUpdateDto implements Serializable {

    @NotNull
    private UUID productId;

    @NotNull
    private Integer productStockCount;

}
