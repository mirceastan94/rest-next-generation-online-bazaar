package com.ngob.rest.product.entity;

import com.ngob.rest.product.entity.user.User;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.UUID;

/**
 * This is the reviews entity class
 *
 * @author Mircea Stan
 */
@Entity
@Getter
@Setter
@Table(name = "reviews")
public class Review {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(updatable = false, unique = true)
    private UUID id;

    @NotBlank
    private String content;

    @ManyToOne(cascade = CascadeType.MERGE, optional = false)
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne(cascade = CascadeType.MERGE, optional = false)
    @JoinColumn(name = "user_id")
    private User user;

    @NotNull
    private Double rating;

    @NotNull
    private Boolean verifiedPurchase;

    @NotNull
    private LocalDate dateOfPosting;

}
