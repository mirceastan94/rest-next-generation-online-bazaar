package com.ngob.rest.product.controller;

import com.ngob.rest.product.dto.APIResponseDto;
import com.ngob.rest.product.dto.review.ReviewDto;
import com.ngob.rest.product.dto.review.ReviewSummaryDto;
import com.ngob.rest.product.service.ReviewService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Set;
import java.util.UUID;

/**
 * This is the review controller class
 *
 * @author Mircea Stan
 */
@RestController
@RequestMapping("/review")
@CrossOrigin
public class ReviewController {

    private ReviewService reviewService;

    ReviewController(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    /**
     * This method will retrieve the reviews list for a product including stats
     *
     * @return
     */
    @GetMapping("/retrieve/{productId}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<ReviewSummaryDto> retrieveReviews(@PathVariable("productId") UUID productId) {
        return new ResponseEntity<>(reviewService.getReviewsWithStats(productId), HttpStatus.OK);
    }

    /**
     * This method will retrieve the reviews list for a product including stats
     *
     * @return
     */
    @GetMapping("/recent")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Set<ReviewDto>> getRecent() {
        return new ResponseEntity<>(reviewService.getRecentReviews(), HttpStatus.OK);
    }

    /**
     * This method will retrieve the reviews list for a product including stats
     *
     * @return
     */
    @GetMapping("/count/{productId}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Integer> getReviewsCount(@PathVariable("productId") UUID productId) {
        return new ResponseEntity<>(reviewService.getReviewsCount(productId), HttpStatus.OK);
    }

    /**
     * This method will insert a new review in the DB
     *
     * @return
     */
    @PostMapping("/publish")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<APIResponseDto> publishReview(@RequestBody ReviewDto reviewDto) {
        reviewService.publishReview(reviewDto);
        return ResponseEntity.ok(new APIResponseDto(true, "Your message has been processed!"));
    }

}
