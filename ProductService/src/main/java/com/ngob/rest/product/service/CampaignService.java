package com.ngob.rest.product.service;

import com.ngob.rest.product.dto.APIResponseDto;
import com.ngob.rest.product.dto.CampaignDto;

import java.util.List;
import java.util.UUID;

/**
 * This is the campaign service interface
 */
public interface CampaignService {

    APIResponseDto createCampaign(CampaignDto campaignDto);

    CampaignDto getCampaign(UUID campaignId);

    APIResponseDto removeCampaign(UUID campaignId);

    List<CampaignDto> retrieveAllCampaigns();

    void updateCampaignsStatus();

}
