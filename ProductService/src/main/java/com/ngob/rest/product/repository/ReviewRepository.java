package com.ngob.rest.product.repository;

import com.ngob.rest.product.entity.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.LinkedHashSet;
import java.util.UUID;

/**
 * This is the review repository interface
 *
 * @author Mircea Stan
 */
@Repository
public interface ReviewRepository extends JpaRepository<Review, UUID> {

    LinkedHashSet<Review> findTop5ByOrderByDateOfPostingDesc();

}
