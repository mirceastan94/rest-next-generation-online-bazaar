package com.ngob.rest.product.repository;

import com.ngob.rest.product.campaign.Campaign;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * This is the campaign repository interface
 *
 * @author Mircea Stan
 */
@Repository
public interface CampaignRepository extends JpaRepository<Campaign, UUID> {
}
