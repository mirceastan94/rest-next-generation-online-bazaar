package com.ngob.rest.product.controller;

import com.ngob.rest.product.dto.APIResponseDto;
import com.ngob.rest.product.dto.CampaignDto;
import com.ngob.rest.product.service.CampaignService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

/**
 * This is the campaign controller class
 *
 * @author Mircea Stan
 */
@RestController
@RequestMapping("/campaign")
@CrossOrigin
public class CampaignController {

    private CampaignService campaignService;

    CampaignController(CampaignService campaignService) {
        this.campaignService = campaignService;
    }

    /**
     * This method will return a campaign DTO based on a UUID, assuming it exists
     *
     * @param campaignId
     * @return
     */
    @GetMapping("/find/{campaignId}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<CampaignDto> findCampaign(@PathVariable("campaignId") UUID campaignId) {
        return new ResponseEntity<>(campaignService.getCampaign(campaignId), HttpStatus.OK);
    }

    /**
     * This method returns all campaigns present in the table
     *
     * @return
     */
    @GetMapping("/find/all")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<List<CampaignDto>> findAllCampaigns() {
        return new ResponseEntity<>(campaignService.retrieveAllCampaigns(), HttpStatus.OK);
    }

    /**
     * This method creates a new campaign in the database
     *
     * @return
     */
    @PostMapping("/create-or-update")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<APIResponseDto> createOrUpdateCampaign(@Valid @RequestBody CampaignDto campaignDto) {
        return new ResponseEntity<>(campaignService.createCampaign(campaignDto), HttpStatus.OK);
    }

    /**
     * This method removes a campaign from the database
     *
     * @return
     */
    @GetMapping("/delete/{campaignId}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<APIResponseDto> createOrUpdateCampaign(@PathVariable("campaignId") UUID campaignId) {
        return new ResponseEntity<>(campaignService.removeCampaign(campaignId), HttpStatus.OK);
    }

}
