package com.ngob.rest.product.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

/**
 * This is the user product(s) update Dto class, used to signal which products a user bought within an offer
 *
 * @author Mircea Stan
 */
@Getter
@Setter
@ToString
public class UserProductsDto implements Serializable {

    @NotNull
    private UUID userId;

    @NotNull
    private Set<UUID> productsIds;

}