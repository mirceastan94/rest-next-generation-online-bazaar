package com.ngob.rest.product.service.user;

import com.ngob.rest.product.entity.user.Role;
import com.ngob.rest.product.entity.user.RoleName;

import java.util.Optional;

/**
 * This is the role service interface
 *
 * @author Mircea Stan
 */
public interface RoleService {

    Optional<Role> findByName(RoleName roleName);

}
