package com.ngob.rest.product.dto.contact;

import lombok.Getter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * This is the contact message DTO class
 *
 * @author Mircea Stan
 */
@Getter
public class ContactMessageDto {

    @NotBlank
    private String name;

    @NotBlank
    private String subject;

    @NotBlank
    @Email
    private String email;

    @NotBlank
    private String phone;

    @NotBlank
    private String content;

}
