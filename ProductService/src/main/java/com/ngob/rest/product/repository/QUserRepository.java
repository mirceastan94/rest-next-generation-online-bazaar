package com.ngob.rest.product.repository;

import com.ngob.rest.product.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * This is the QueryDSL based User repository interface
 *
 * @author Mircea Stan
 */
@Repository
public interface QUserRepository extends JpaRepository<User, UUID>, QuerydslPredicateExecutor<User> {
}
