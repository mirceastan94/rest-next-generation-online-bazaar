package com.ngob.rest.product.config;

import com.ngob.rest.product.integration.redis.MessagePublisher;
import com.ngob.rest.product.integration.redis.MessagePublisherImpl;
import com.ngob.rest.product.integration.redis.MessageSubscriber;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.jms.listener.adapter.MessageListenerAdapter;

/**
 * This is the redis instance configuration class
 *
 * @author Mircea Stan
 */
@Configuration
public class RedisConfiguration {

    @Bean
    JedisConnectionFactory jedisConnectionFactory() {
        return new JedisConnectionFactory();
    }

    @Bean
    public RedisTemplate<String, Object> redisTemplate() {
        final RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(jedisConnectionFactory());
        redisTemplate.setValueSerializer(new GenericToStringSerializer<>(Object.class));
        return redisTemplate;
    }

    @Bean
    public ChannelTopic createProductsTopic() {
        return new ChannelTopic("Products");
    }

    @Bean
    public MessagePublisher redisPublishes() {
        return new MessagePublisherImpl(redisTemplate(), createProductsTopic());
    }

    @Bean
    public MessageListenerAdapter messageListenerAdapter() {
        return new MessageListenerAdapter(new MessageSubscriber());
    }


}
