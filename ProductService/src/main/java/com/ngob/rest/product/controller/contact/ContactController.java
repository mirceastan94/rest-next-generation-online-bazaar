package com.ngob.rest.product.controller.contact;

import com.ngob.rest.product.dto.APIResponseDto;
import com.ngob.rest.product.dto.contact.ContactMessageDto;
import com.ngob.rest.product.service.contact.ContactService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * This is the contact controller class
 *
 * @author Mircea Stan
 */
@RestController
@RequestMapping("/")
@CrossOrigin
public class ContactController {

    private final ContactService contactService;

    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }

    @PostMapping(value = "/contact")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<APIResponseDto> postContactMessage(@RequestBody final ContactMessageDto contactMessageDto) {
        contactService.postContactMessage(contactMessageDto);
        return ResponseEntity.ok(new APIResponseDto(true, "Your message has been processed!"));
    }

}
