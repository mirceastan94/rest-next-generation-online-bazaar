package com.ngob.rest.product.integration.route;

import com.ngob.rest.product.integration.jaxb.EnvelopeJAXB;
import com.ngob.rest.product.integration.json.EnvelopeJSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.LoggingLevel;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * This is the envelope route class
 */
@Slf4j
@Component
public class EnvelopeRoute extends BaseRouteBuilder {

    @Autowired
    private Queue productIntegrationQueue;

    @Autowired
    private Queue acknowledgeQueue;

    @Override
    public void configure() throws Exception {
        super.configure();

        final var jaxbContext = JAXBContext.newInstance(EnvelopeJAXB.class);
        final var jaxbDataFormat = new JaxbDataFormat(jaxbContext);
        jaxbDataFormat.setEncoding("UTF-8");

        from("rabbitmq:products.direct?autoDelete=false&queue=" + productIntegrationQueue.getName())
                .threads(5)
                .doTry()
                .log(LoggingLevel.INFO, log, "Message received via: " + productIntegrationQueue)
                .choice()
                .when(header(HttpHeaders.CONTENT_TYPE).isEqualToIgnoreCase(APPLICATION_JSON_VALUE)).
                    unmarshal().json(JsonLibrary.Jackson, EnvelopeJSON.class).process(exchange -> {
                        exchange.getIn().setHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_VALUE);
                        exchange.getIn().setBody(exchange.getIn().getBody(EnvelopeJSON.class).getProducts());
                    })
                    .split((body()))
                    .to(ProductRoute.ROUTE_PATH)
                    .log(LoggingLevel.INFO, log, "Product(s) JSON objects sent to route: " + ProductRoute.ROUTE_PATH)
                    .endChoice()
                .when(header(HttpHeaders.CONTENT_TYPE).isEqualToIgnoreCase(MediaType.APPLICATION_XML_VALUE))
                    .unmarshal(jaxbDataFormat)
                    .process(exchange -> {
                        exchange.getIn().setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML_VALUE);
                        exchange.getIn().setBody(exchange.getIn().getBody(EnvelopeJAXB.class).getProducts());
                    })
                    .split(body())
                    .to(ProductRoute.ROUTE_PATH)
                    .log(LoggingLevel.INFO, log, "Product(s) JAXB objects sent to route: " + ProductRoute.ROUTE_PATH)
                    .endChoice()
                .otherwise()
                .setHeader("rabbitmq.ROUTING_KEY", constant(acknowledgeQueue.getName()))
                .to("rabbitmq:products.direct?queue=" + acknowledgeQueue.getName()
                        + "&routingKey=" + acknowledgeQueue.getName()
                        +"&autoDelete=false&autoAck=true&BridgeEndpoint=true")
                .log(LoggingLevel.ERROR, log, "Invalid header type, sent to ack queue!")
                .end();
    }
}
