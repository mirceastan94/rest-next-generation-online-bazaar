package com.ngob.rest.product.service.contact;

import com.ngob.rest.product.dto.contact.ContactMessageDto;
import com.ngob.rest.product.entity.contact.Message;
import com.ngob.rest.product.mapper.UserMapper;
import com.ngob.rest.product.repository.contact.MessageRepository;
import lombok.extern.log4j.Log4j2;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

/**
 * This is the Contact Service class, used to send messages to the administrator(s)
 *
 * @author Mircea Stan
 */
@Service
@Transactional
@Log4j2
public class ContactServiceImpl implements ContactService {

    private final MapperFacade mapperFacade;
    private final MessageRepository messageRepository;

    ContactServiceImpl(UserMapper mapperFacade, MessageRepository messageRepository) {
        this.mapperFacade = mapperFacade;
        this.messageRepository = messageRepository;
    }

    public void postContactMessage(@RequestBody @Valid ContactMessageDto contactMessageDto) {
        log.info("Mapping and persisting the new contact message...");

        final var newMessage = mapperFacade.map(contactMessageDto, Message.class);
        messageRepository.saveAndFlush(newMessage);

        log.info(String.format("New message persisted in the database %s", newMessage.getId().toString()));
    }

}
