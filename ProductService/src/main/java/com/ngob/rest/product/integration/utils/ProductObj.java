package com.ngob.rest.product.integration.utils;

import lombok.Getter;

import java.io.Serializable;

/**
 * This is the abstract class for Products integration object
 */
@Getter
public abstract class ProductObj implements Serializable {

    private String name;
    private String description;
    private String category;
    private String subCategory;
    private Integer stockCount;
    private String brand;
    private Integer price;
    private Integer discountPrice;
    private String imageLocation;
    private String popularity;
    private String introductoryDate;
    private Boolean isHeadline;
}
