package com.ngob.rest.product.config;

import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import lombok.Getter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.http.HttpClient;
import java.time.Duration;
import java.util.concurrent.Executors;

/**
 * This is the social login config class
 *
 * @author Mircea Stan
 */
@Getter
@Configuration
public class SocialLoginConfig {

    @Bean
    public NetHttpTransport createNetHttpTransport() {
        return new NetHttpTransport();
    }

    @Bean
    public GsonFactory createNetGoogleIdTokenVerifier() {
        return new GsonFactory();
    }

    @Bean
    public HttpClient createHttpClient() {
        return HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .connectTimeout(Duration.ofMinutes(1))
                .executor(Executors.newFixedThreadPool(3))
                .build();
    }

}
