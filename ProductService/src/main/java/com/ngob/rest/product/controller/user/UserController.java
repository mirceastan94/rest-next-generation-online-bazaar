package com.ngob.rest.product.controller.user;

import com.ngob.rest.product.dto.APIResponseDto;
import com.ngob.rest.product.dto.CriteriaDto;
import com.ngob.rest.product.dto.user.*;
import com.ngob.rest.product.security.LoggedUser;
import com.ngob.rest.product.security.UserPrincipal;
import com.ngob.rest.product.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.UUID;

/**
 * This is the user controller class
 *
 * @author Mircea Stan
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired // optional in Spring >= 4.3 for a single constructor
    public UserController(final UserService userService) {
        this.userService = userService;
    }

    /**
     * This method will login a user based on the found credentials
     *
     * @param loginUserDTO
     * @return
     */
    @PostMapping("/login")
    public ResponseEntity<JwtAuthResponseDto> authenticateUser(@Valid @RequestBody LoginUserDto loginUserDTO) {
        final JwtAuthResponseDto jsonWebToken = userService.loginUser(loginUserDTO);

        return ResponseEntity.ok(jsonWebToken);
    }

    /**
     * This method will login a user based on the found credentials
     *
     * @param socialLoginUserDto
     * @return
     */
    @PostMapping("/social-login")
    public ResponseEntity<JwtAuthResponseDto> authenticateUserSocially(
            @Valid @RequestBody SocialLoginUserDto socialLoginUserDto) throws URISyntaxException,
            GeneralSecurityException, InterruptedException, IOException {
        final JwtAuthResponseDto jsonWebToken = userService.loginUserSocially(socialLoginUserDto);

        return ResponseEntity.ok(jsonWebToken);
    }

    /**
     * This method registers a new user with the found information
     *
     * @param registerUserDTO
     * @return
     */
    @PostMapping("/register")
    public ResponseEntity<APIResponseDto> registerUser(@Valid @RequestBody RegisterUserDto registerUserDTO) {

        userService.createUser(registerUserDTO);

        final var locationURI = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/users/{email}")
                .buildAndExpand(registerUserDTO.getName()).toUri();

        final String userRegisteredMsg = "User with mail '" +
                registerUserDTO.getEmail() + "' has been registered successfully!";
        logger.info(userRegisteredMsg);

        return ResponseEntity.created(locationURI).body(new APIResponseDto(true, userRegisteredMsg));
    }

    /**
     * This method shows the user details of the logged one
     *
     * @param currentUser
     * @return
     */
    @GetMapping("/profile/me")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<UserDto> getCurrentUser(@LoggedUser UserPrincipal currentUser) {
        return getUserProfile(currentUser.getEmail());
    }

    /**
     * This method counts all users present in the database
     * @return
     */
    @GetMapping("/count")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Long> getUsersCount() {
        return new ResponseEntity<>(userService.getUsersCount(), HttpStatus.OK);
    }

    /**
     * This method shows the user details of the logged one
     *
     * @param email
     * @return
     */
    @GetMapping("/profile")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<UserDto> getUserProfile(@RequestParam("email") String email) {
        return new ResponseEntity<>(userService.getUserProfile(email), HttpStatus.OK);
    }

    /**
     * This method shows the user details of the logged one
     *
     * @param updatedUserDto
     * @return
     */
    @PostMapping("/profile")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<UserUpdateResponseDto> updateUserProfile(@Valid @RequestBody UserDto updatedUserDto) {
        return new ResponseEntity<>(userService.updateUserProfile(updatedUserDto), HttpStatus.OK);
    }

    /**
     * This method will return the total number of users which respect a specific filter criteria
     *
     * @return
     */
    @PostMapping("/count-by-criteria")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Long> getProductsCountWithCriteria(@Valid @RequestBody CriteriaDto criteriaDto) {
        return new ResponseEntity<>(userService.countUsersWithCriteria(criteriaDto), HttpStatus.OK);
    }

    /**
     * This method returns a list of users based on different search criteria
     */
    @PostMapping("/find-by-criteria")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<List<UserDto>> findUsersWithCriteria(@Valid @RequestBody CriteriaDto criteriaDto) {
        return new ResponseEntity<>(userService.getUsersByCriteria(criteriaDto), HttpStatus.OK);
    }

    /**
     * This method updates the user based
     */
    @PostMapping("/update-role")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<APIResponseDto> changeUserRole(@Valid @RequestBody UserRoleDto userRoleDto) {
        return new ResponseEntity<>(userService.updateUserRole(userRoleDto), HttpStatus.OK);
    }

    /**
     * This method updates the user based
     */
    @GetMapping("/delete/{userId}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<APIResponseDto> deleteUser(@PathVariable("userId") UUID userId) {
        return new ResponseEntity<>(userService.deleteUser(userId), HttpStatus.OK);
    }

}
