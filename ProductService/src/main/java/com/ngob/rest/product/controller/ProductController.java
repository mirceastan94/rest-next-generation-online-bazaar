package com.ngob.rest.product.controller;

import com.ngob.rest.product.dto.CriteriaDto;
import com.ngob.rest.product.dto.product.ProductCategoryDto;
import com.ngob.rest.product.dto.product.ProductDto;
import com.ngob.rest.product.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

/**
 * This is the product controller class
 *
 * @author Mircea Stan
 */
@RestController
@RequestMapping("/")
@CrossOrigin
public class ProductController {

    private ProductService productService;

    ProductController(ProductService productService) {
        this.productService = productService;
    }

    /**
     * This method will return a product DTO based on a UUID, assuming it exists
     *
     * @param productId
     * @return
     */
    @GetMapping("/find/{productId}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<ProductDto> getProduct(@PathVariable("productId") UUID productId) {
        return new ResponseEntity<>(productService.getProduct(productId), HttpStatus.OK);
    }

    /**
     * This method will return a product DTO based on a UUID, as well as a campaign UUID, assuming it exists
     *
     * @param productId
     * @return
     */
    @GetMapping("/find/{productId}/campaign/{campaignId}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<ProductDto> getProduct(@PathVariable("productId") UUID productId,
                                                 @PathVariable("campaignId") UUID campaignId) {
        return new ResponseEntity<>(productService.getProductWithCampaign(productId, campaignId), HttpStatus.OK);
    }

    /**
     * This method will return the total number of products which respect a specific filter criteria
     *
     * @return
     */
    @PostMapping("/count-by-criteria")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Long> getProductsCountWithCriteria(@Valid @RequestBody CriteriaDto criteriaDto) {
        return new ResponseEntity<>(productService.countProductsByCriteria(criteriaDto), HttpStatus.OK);
    }

    /**
     * This method will return a list of ProductDTOs based on specified criteria
     *
     * @return
     */
    @PostMapping("/find-by-criteria")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<List<ProductDto>> findProductsWithCriteria(@Valid @RequestBody CriteriaDto criteriaDto) {
        return new ResponseEntity<>(productService.getProductsByCriteria(criteriaDto), HttpStatus.OK);
    }

    /**
     * This method will insert a new product in the DB
     *
     * @return
     */
    @PostMapping("/create")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<String> insertProduct(@RequestBody ProductDto productObj) {
        return new ResponseEntity<>(productService.insertOrUpdateProductFromObj(productObj), HttpStatus.OK);
    }

    /**
     * This method counts all products present in the database
     *
     * @return
     */
    @GetMapping("/count")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Long> getProductsCount() {
        return new ResponseEntity<>(productService.getProductsCount(), HttpStatus.OK);
    }

    /**
     * This method returns all products categories and their products count
     *
     * @return
     */
    @GetMapping("/categories-with-count")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<List<ProductCategoryDto>> getProductsCategoriesWithCount() {
        return new ResponseEntity<>(productService.getProductsCategoriesWithCount(), HttpStatus.OK);
    }

    /**
     * This method will cache current products found in the DB
     *
     * @return
     */
    @GetMapping("/cache")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Void> cacheProducts() {
        productService.cacheCurrentProducts();
        return ResponseEntity.ok().build();
    }

    /**
     * This method will retrieve cached products from the Redis instance
     *
     * @return
     */
    @GetMapping("/cache/find-all")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<List<ProductDto>> retrieveCachedProducts() {
        return new ResponseEntity<>(productService.getCachedProducts(), HttpStatus.OK);
    }

}
