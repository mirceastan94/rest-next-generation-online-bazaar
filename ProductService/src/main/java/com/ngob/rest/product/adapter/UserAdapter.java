package com.ngob.rest.product.adapter;

import com.ngob.rest.product.entity.user.User;


/**
 * This is the user adapter class, used for entity/MQ conversion
 *
 * @author Mircea Stan
 */
public class UserAdapter {

    private UserAdapter() {
    }

    public static User toMqUser(User user) {
        user.getUserDetails().setUser(null);
        user.getUserDetails().getPaymentCards().stream().findFirst()
                .ifPresent(paymentCard -> paymentCard.setUserDetails(null));

        return user;
    }

}
