package com.ngob.rest.product.repository;

import com.ngob.rest.product.dto.product.ProductCategoryDto;
import com.ngob.rest.product.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * This is the product repository interface
 *
 * @author Mircea Stan
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, UUID> {

    Optional<Product> findByName(String name);

    @Query(value = "SELECT category as name, count(*) as value FROM products p GROUP BY category ORDER by value DESC",
            nativeQuery = true)
    List<ProductCategoryDto> getProductCategoryDtos();
}
