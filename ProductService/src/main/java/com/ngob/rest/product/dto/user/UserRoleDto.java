package com.ngob.rest.product.dto.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

/**
 * This is the user role Dto
 *
 * @author Mircea Stan
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserRoleDto {

    @NotNull
    private UUID userId;

    @NotEmpty
    private String role;

}
