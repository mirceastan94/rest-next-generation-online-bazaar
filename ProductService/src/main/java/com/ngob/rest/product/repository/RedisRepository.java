package com.ngob.rest.product.repository;

import com.ngob.rest.product.dto.product.ProductDto;
import com.ngob.rest.product.entity.Product;

import java.util.Map;

/**
 * This is the Redis repository interface for products caching
 *
 * @author Mircea Stan
 */
public interface RedisRepository {

    Map<Object, ProductDto> findAllProducts();

    Product findProduct(String id);

    void insertProduct(ProductDto product);

    void deleteProduct(String id);

}
