package com.ngob.rest.product.dto.user;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * This is the register request class
 *
 * @author Mircea Stan
 */
@Getter
@Setter
public class RegisterUserDto {

    @NotBlank
    @Size(min = 4, max = 50)
    private String name;

    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    @NotBlank
    @Size(min = 6, max = 20)
    private String password;

    private String language;

    private RoleDto roleDto;

}
