package com.ngob.rest.product.service;

import com.ngob.rest.product.dto.review.ReviewDto;
import com.ngob.rest.product.dto.review.ReviewSummaryDto;

import java.util.LinkedHashSet;
import java.util.UUID;

/**
 * This is the Product Service interface, used to review products
 *
 * @author Mircea Stan
 */
public interface ReviewService {

    ReviewSummaryDto getReviewsWithStats(UUID productID);

    Integer getReviewsCount(UUID productID);

    String publishReview(ReviewDto reviewDto);

    LinkedHashSet<ReviewDto> getRecentReviews();

}
