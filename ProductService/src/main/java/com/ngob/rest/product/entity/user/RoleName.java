package com.ngob.rest.product.entity.user;

/**
 * This is the role name enum
 *
 * @author Mircea Stan
 */
public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
