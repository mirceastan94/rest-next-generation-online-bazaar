package com.ngob.rest.product.dto;

import lombok.Getter;

/**
 * This is the general clients API response class
 *
 * @author Mircea Stan
 */
@Getter
public class APIResponseDto {
    private Boolean success;
    private String message;

    public APIResponseDto(Boolean success, String message) {
        this.success = success;
        this.message = message;
    }

}