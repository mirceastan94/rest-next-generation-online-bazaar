package com.ngob.rest.product.dto.product;

/**
 * This is the product category Dto projection
 *
 * @author Mircea Stan
 */
public interface ProductCategoryDto {

    String getName();
    Long getValue();

}
