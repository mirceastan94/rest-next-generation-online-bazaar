package com.ngob.rest.product.utils;

import com.ngob.rest.product.entity.Review;
import lombok.Getter;

import java.util.Set;

/**
 * This is the Reviews Wrapper util class used for mapping purposes
 *
 * @author Mircea Stan
 */
@Getter
public class ReviewsWrapper {

    private final Set<Review> reviews;

    public ReviewsWrapper(Set<Review> reviews) {
        this.reviews = reviews;
    }

}
