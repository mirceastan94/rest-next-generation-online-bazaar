package com.ngob.rest.product.dto;

import com.querydsl.core.types.Order;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * This is the product criteria Dto class
 *
 * @author Mircea Stan
 */
@Data
public class CriteriaDto {

    private Map<String, String> filterCriteria = new HashMap<>();
    private Integer page;
    private Integer size;
    private String sortByField;
    private Order order;
    private UUID campaignId;

}
