package com.ngob.rest.product.integration.jaxb;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * This is the envelope JAXB class
 */
@XmlRootElement(name = "envelope")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"products"})
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class EnvelopeJAXB {

    @XmlElementWrapper(name = "products")
    @XmlElement(name = "product")
    private List<ProductJAXB> products;

}
