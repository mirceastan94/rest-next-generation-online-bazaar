package com.ngob.rest.product.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
/**
 * This is the properties config class
 *
 * @author Mircea Stan
 */
public class AppProperties {

    @Value("${google.client.id}")
    private String googleClientId;

    @Value("${google.client.secret}")
    private String googleClientPassword;

    @Value("${facebook.graph.scheme}")
    private String facebookGraphScheme;

    @Value("${facebook.graph.host}")
    private String facebookGraphHost;

    @Value("${facebook.graph.path}")
    private String facebookGraphPath;

    @Value("${facebook.app.id}")
    private String facebookAppId;

    @Value("${facebook.app.secret}")
    private String facebookAppSecret;
}
