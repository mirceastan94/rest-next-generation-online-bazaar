package com.ngob.rest.product.repository.contact;

import com.ngob.rest.product.entity.contact.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * This is the message Jpa repository interface
 *
 * @author Mircea Stan
 */
public interface MessageRepository extends JpaRepository<Message, UUID> {
}