package com.ngob.rest.product.service.user;

import com.ngob.rest.product.dto.APIResponseDto;
import com.ngob.rest.product.dto.CriteriaDto;
import com.ngob.rest.product.dto.user.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.UUID;

/**
 * This is the user service interface
 *
 * @author Mircea Stan
 */
public interface UserService {

    void createUser(RegisterUserDto user);

    boolean existsUser(String email);

    JwtAuthResponseDto loginUser(LoginUserDto loginUserDTO);

    JwtAuthResponseDto loginUserSocially(SocialLoginUserDto loginUserDto) throws IOException, GeneralSecurityException, URISyntaxException, InterruptedException;

    UserDto getUserProfile(String email);

    UserUpdateResponseDto updateUserProfile(UserDto updatedUserDto);

    Long getUsersCount();

    List<UserDto> getUsersByCriteria(CriteriaDto criteriaDto);

    Long countUsersWithCriteria(CriteriaDto criteriaDto);

    APIResponseDto updateUserRole(UserRoleDto userRoleDto);

    APIResponseDto deleteUser(UUID userId);

}
