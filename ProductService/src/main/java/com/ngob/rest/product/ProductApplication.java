package com.ngob.rest.product;

import com.ngob.rest.product.entity.user.*;
import com.ngob.rest.product.repository.user.RoleRepository;
import com.ngob.rest.product.repository.user.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import static com.ngob.rest.product.utils.Constants.*;

/**
 * This is the "Bootstrap" class of the products service
 */
@Log4j2
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class, ManagementWebSecurityAutoConfiguration.class})
@EnableScheduling
public class ProductApplication implements CommandLineRunner {

    @Autowired
    private PlatformTransactionManager txManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    public static void main(String[] args) {
        SpringApplication.run(ProductApplication.class, args);
    }

    @Override
    public void run(String... args) {
        final var txTemplate = new TransactionTemplate(txManager);
        txTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                final Role adminRole = roleRepository.findByName(RoleName.ROLE_ADMIN)
                        .orElseThrow(() -> new ResourceNotFoundException("Admin role not set in the database!"));
                final Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                        .orElseThrow(() -> new ResourceNotFoundException("User role not set in the database!"));
                if (!userRepository.existsByEmail(DEFAULT_ADMIN_EMAIL)) {
                    final var defaultAdmin = User.builder()
                            .email(DEFAULT_ADMIN_EMAIL)
                            .name(DEFAULT_ADMIN_NAME)
                            .userPassword(passwordEncoder.encode(DEFAULT_ADMIN_PASSWORD))
                            .role(adminRole)
                            .isDeleted(Boolean.FALSE)
                            .build();
                    userRepository.saveAndFlush(defaultAdmin);
                    final var userDetails = new UserDetails(defaultAdmin.getId(), Language.ENGLISH);
                    defaultAdmin.setUserDetails(userDetails);
                    userRepository.saveAndFlush(defaultAdmin);
                }
                if (!userRepository.existsByEmail(TEST_USER_EMAIL)) {
                    final var testUser = User.builder()
                            .email(TEST_USER_EMAIL)
                            .name(TEST_USER_NAME)
                            .userPassword(passwordEncoder.encode(TEST_USER_PASSWORD))
                            .role(userRole)
                            .isDeleted(Boolean.FALSE)
                            .build();
                    userRepository.saveAndFlush(testUser);
                    final var userDetails = new UserDetails(testUser.getId(), Language.ENGLISH);
                    testUser.setUserDetails(userDetails);
                    userRepository.save(testUser);
                }
            }
        });
    }

}
