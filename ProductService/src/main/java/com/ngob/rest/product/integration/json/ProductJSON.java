package com.ngob.rest.product.integration.json;

import com.ngob.rest.product.integration.utils.ProductObj;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
/**
 * This is the product JSON class
 */
public class ProductJSON extends ProductObj {

    private String name;
    private String description;
    private String category;
    private String subCategory;
    private Integer stockCount;
    private String brand;
    private Integer price;
    private Integer discountPrice;
    private String imageLocation;
    private String popularity;
    private String introductoryDate;
    private Boolean isHeadline;

}
