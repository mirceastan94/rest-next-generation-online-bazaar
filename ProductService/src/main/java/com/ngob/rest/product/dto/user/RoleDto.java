package com.ngob.rest.product.dto.user;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
/**
 * This is the role dto class
 *
 * @author Mircea Stan
 */
public class RoleDto {

    private UUID id;
    private String name;

}
