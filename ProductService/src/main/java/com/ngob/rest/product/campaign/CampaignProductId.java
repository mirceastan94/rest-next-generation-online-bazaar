package com.ngob.rest.product.campaign;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * This is the campaign product join table embedded id class
 *
 * @author Mircea Stan
 */
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class CampaignProductId implements Serializable {

    @Column(name = "campaign_id")
    private UUID campaignId;

    @Column(name = "product_id")
    private UUID productId;

    @Override
    public boolean equals(Object comparedObj) {
        if (comparedObj == null || getClass() != comparedObj.getClass()) {
            return false;
        }

        if (this == comparedObj) {
            return true;
        }

        final var comparedCampaignProductId = (CampaignProductId) comparedObj;
        return Objects.equals(this.campaignId, comparedCampaignProductId.campaignId)
                && Objects.equals(this.productId, comparedCampaignProductId.productId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(campaignId, productId);
    }

}
