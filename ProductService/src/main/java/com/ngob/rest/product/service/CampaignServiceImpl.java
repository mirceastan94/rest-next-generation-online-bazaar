package com.ngob.rest.product.service;

import com.ngob.rest.product.campaign.Campaign;
import com.ngob.rest.product.campaign.CampaignStatus;
import com.ngob.rest.product.dto.APIResponseDto;
import com.ngob.rest.product.dto.CampaignDto;
import com.ngob.rest.product.mapper.CampaignMapper;
import com.ngob.rest.product.repository.CampaignRepository;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.ResourceNotFoundException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * This is the campaign service implementation class
 */
@Service
@Transactional
@Slf4j
public class CampaignServiceImpl implements CampaignService {

    CampaignMapper campaignMapper;
    CampaignRepository campaignRepository;

    CampaignServiceImpl(CampaignMapper campaignMapper, CampaignRepository campaignRepository) {
        this.campaignMapper = campaignMapper;
        this.campaignRepository = campaignRepository;
    }

    @Override
    public APIResponseDto createCampaign(CampaignDto campaignDto) {
        Campaign campaign;

        if (Objects.nonNull(campaignDto.getId())) {
            campaign = campaignRepository.findById(campaignDto.getId()).orElseThrow(() ->
                    new ResourceNotFoundException("Campaign", "ID", campaignDto.getId()));
        } else {
            campaign = new Campaign();
        }

        campaignMapper.map(campaignDto, campaign);
        campaign = campaignRepository.save(campaign);

        log.info("Campaign with ID '{}' has just been created/updated!", campaign.getId());
        return new APIResponseDto(true, "The new campaign has been created/updated successfully!");
    }

    @Override
    public CampaignDto getCampaign(UUID campaignId) {
        final var campaign = campaignRepository.findById(campaignId).orElseThrow(() ->
                new ResourceNotFoundException("Campaign", "ID", campaignId));
        return campaignMapper.map(campaign, CampaignDto.class);
    }

    @Override
    public APIResponseDto removeCampaign(UUID campaignId) {
        campaignRepository.deleteById(campaignId);
        return new APIResponseDto(true, "The new campaign has been deleted successfully!");
    }

    @Override
    public List<CampaignDto> retrieveAllCampaigns() {
        final var campaigns = campaignRepository.findAll();
        return campaignMapper.mapAsList(campaigns, CampaignDto.class);
    }

    @Scheduled(fixedRate = 60 * 60 * 1000, initialDelay = 0)
    public void updateCampaignsStatus() {
        final var campaigns = campaignRepository.findAll();
        campaigns.forEach(campaign -> {
            if (LocalDate.now().isAfter(campaign.getStartDate()) && LocalDate.now().isBefore(campaign.getEndDate())) {
                campaign.setStatus(CampaignStatus.ACTIVE);
            } else {
                campaign.setStatus(CampaignStatus.INACTIVE);
            }
        });
        campaignRepository.saveAll(campaigns);
    }
}
