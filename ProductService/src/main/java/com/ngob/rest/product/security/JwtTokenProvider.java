package com.ngob.rest.product.security;

import com.ngob.rest.product.dto.user.JwtAuthResponseDto;
import com.ngob.rest.product.dto.user.UserDetailsDto;
import com.ngob.rest.product.dto.user.UserDto;
import io.jsonwebtoken.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.util.Date;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * This is the JwtTokenProvider class
 *
 * @author Mircea Stan
 */
@Component
public class JwtTokenProvider {

    @Value("${jwt.secret}")
    private String jwtSecret;

    @Value("${jwt.expirationInMs}")
    private int jwtExpirationInMs;

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    public JwtAuthResponseDto generateAuthTokenResponse(Authentication authentication, UserDetailsDto userDetailsDto) {

        final var userPrincipal = (UserPrincipal) authentication.getPrincipal();

        final var now = new Date();
        final var expiryDate = new Date(now.getTime() + jwtExpirationInMs);

        final String jwt = Jwts.builder()
                .setSubject(userPrincipal.getId().toString())
                .setIssuedAt(new Date())
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
        final var userDto = new UserDto(userPrincipal.getId(), userPrincipal.getName(), StringUtils.EMPTY,
                userPrincipal.getEmail(), Boolean.FALSE, userPrincipal.getAuthorities().stream().map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining()), StringUtils.EMPTY, userDetailsDto);
        return new JwtAuthResponseDto(jwt, expiryDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().toString(), userDto);
    }

    public UUID getUserIdFromJWT(String token) {
        var claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();

        return UUID.fromString(claims.getSubject());
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (MalformedJwtException ex) {
            logger.error("Invalid JWT token!");
        } catch (ExpiredJwtException ex) {
            logger.error("Expired JWT token!");
        } catch (UnsupportedJwtException ex) {
            logger.error("Unsupported JWT token!");
        } catch (IllegalArgumentException ex) {
            logger.error("JWT claims string is empty!");
        }
        return false;
    }
}
