package com.ngob.rest.product.service.contact;

import com.ngob.rest.product.dto.contact.ContactMessageDto;

/**
 * This is the Contact Service interface, used to send messages to the administrator(s)
 *
 * @author Mircea Stan
 */
public interface ContactService {

    void postContactMessage(ContactMessageDto products);
}
