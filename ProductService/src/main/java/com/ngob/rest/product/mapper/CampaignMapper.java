package com.ngob.rest.product.mapper;

import com.ngob.rest.product.campaign.Campaign;
import com.ngob.rest.product.campaign.CampaignProduct;
import com.ngob.rest.product.campaign.CampaignStatus;
import com.ngob.rest.product.dto.CampaignDto;
import com.ngob.rest.product.dto.product.ProductDto;
import com.ngob.rest.product.repository.ProductRepository;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.elasticsearch.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * This is the custom mapper for campaign related conversions
 */
@Component
public class CampaignMapper extends ConfigurableMapper {

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductRepository productRepository;

    @Override
    protected void configure(MapperFactory mapperFactory) {

        mapperFactory.classMap(CampaignDto.class, Campaign.class).customize(new CustomMapper<>() {
            @Override
            public void mapAtoB(CampaignDto campaignDto, Campaign campaign, MappingContext context) {
                super.mapAtoB(campaignDto, campaign, context);
                if (Objects.nonNull(campaignDto.getCampaignStatus())) {
                    campaign.setStatus(CampaignStatus.fromName(campaignDto.getCampaignStatus()));
                }
                mapCampaignProductsToEntity(campaignDto.getProductDtos(), campaign);
            }

            @Override
            public void mapBtoA(Campaign campaign, CampaignDto campaignDto, MappingContext context) {
                super.mapBtoA(campaign, campaignDto, context);
                campaignDto.setCampaignStatus(campaign.getStatus().getName().toLowerCase());
                mapCampaignProductsToDto(campaign.getProducts(), campaignDto);
            }
        }).byDefault().register();
    }

    private void mapCampaignProductsToEntity(List<ProductDto> productDtos, Campaign campaign) {
        List<CampaignProduct> campaignProducts = campaign.getProducts();
        productDtos.forEach(productDto -> {
            final var product = productRepository.findById(productDto.getId()).orElseThrow(() ->
                    new ResourceNotFoundException("Product", "ID", productDto.getId()));
            final var price = Objects.nonNull(productDto.getDiscountPrice()) ? productDto.getDiscountPrice()
                    : (int) (productDto.getPrice() * 0.9);
            if (campaignProducts.stream().noneMatch(campaignProduct ->
                    campaignProduct.getProduct().getId().equals(productDto.getId()))) {
                campaignProducts.add(new CampaignProduct(campaign, product, price));
            } else {
                final var updatedProduct = campaignProducts.stream().filter(campaignProduct ->
                        campaignProduct.getProduct().getId().equals(productDto.getId())).findFirst().get();
                int existingProductIdx = campaignProducts.indexOf(updatedProduct);
                updatedProduct.setPrice(price);
                campaignProducts.set(existingProductIdx, updatedProduct);
            }
        });
    }

    private void mapCampaignProductsToDto(List<CampaignProduct> campaignProducts, CampaignDto campaignDto) {
        List<ProductDto> campaignProductsDtos = Objects.nonNull(campaignDto.getProductDtos())
                && !campaignDto.getProductDtos().isEmpty() ? campaignDto.getProductDtos() : new ArrayList<>();
        campaignProducts.forEach(campaignProduct -> {
            final var productDto = productMapper.map(campaignProduct.getProduct(), ProductDto.class);
            productDto.setDiscountPrice(campaignProduct.getPrice());
            campaignProductsDtos.add(productDto);
        });
        campaignDto.setProductDtos(campaignProductsDtos);
    }

}
