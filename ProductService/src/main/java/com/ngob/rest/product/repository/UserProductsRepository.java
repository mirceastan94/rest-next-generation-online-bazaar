package com.ngob.rest.product.repository;

import com.ngob.rest.product.entity.UserProducts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * This is the user products repository interface
 *
 * @author Mircea Stan
 */
@Repository
public interface UserProductsRepository extends JpaRepository<UserProducts, UUID> {

    Optional<List<UserProducts>> findByUserId(UUID userId);
}
