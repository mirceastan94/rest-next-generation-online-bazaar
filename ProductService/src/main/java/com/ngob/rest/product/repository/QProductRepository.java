package com.ngob.rest.product.repository;

import com.ngob.rest.product.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

/**
 * This is the QueryDSL product repository interface
 *
 * @author Mircea Stan
 */
@Repository
public interface QProductRepository extends JpaRepository<Product, UUID>, QuerydslPredicateExecutor<Product> {

    Optional<Product> findByName(String name);
}
