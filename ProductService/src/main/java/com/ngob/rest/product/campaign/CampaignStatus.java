package com.ngob.rest.product.campaign;

import lombok.Getter;

import java.util.Arrays;

/**
 * This is the campaign status enum
 *
 * @author Mircea Stan
 */
@Getter
public enum CampaignStatus {

    ACTIVE("Active"),
    INACTIVE("Inactive");

    private final String name;

    private CampaignStatus(String name) {
        this.name = name;
    }

    public static CampaignStatus fromName(String inputName) {
        return Arrays.stream(values()).filter(status ->
                status.name.equalsIgnoreCase(inputName)).findFirst().orElse(CampaignStatus.INACTIVE);
    }


}
