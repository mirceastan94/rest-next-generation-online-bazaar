package com.ngob.rest.product.integration.redis;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * This is the Redis message listener class
 *
 * @author Mircea Stan
 */
@Service
@Slf4j
public class MessageSubscriber implements MessageListener {

    public static final List<String> messageList = new ArrayList<>();

    @Override
    public void onMessage(Message message, byte[] pattern) {
        messageList.add(message.toString());
        log.info(String.format("Message received: %s", new String(message.getBody())));
    }

}
