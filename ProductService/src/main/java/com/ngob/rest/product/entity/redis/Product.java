package com.ngob.rest.product.entity.redis;

import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;

/**
 * This is the Redis Product object class
 *
 * @author Mircea Stan
 */
@RedisHash("Product")
public class Product implements Serializable {
}
