package com.ngob.rest.product.adapter;

import com.ngob.rest.product.entity.Popularity;
import com.ngob.rest.product.entity.Product;
import com.ngob.rest.product.integration.utils.ProductObj;

import java.time.LocalDate;
import java.util.Objects;

/**
 * This is the product adapter class
 *
 * @author Mircea Stan
 */
public class ProductAdapter {

    private ProductAdapter() {
    }

    public static void toProduct(ProductObj productObj, Product product) {
        product.setName(productObj.getName());
        product.setDescription(productObj.getDescription());
        product.setCategory(productObj.getCategory());
        product.setSubCategory(productObj.getSubCategory());
        product.setStockCount(Objects.nonNull(product.getStockCount()) ? product.getStockCount()
                + productObj.getStockCount() : productObj.getStockCount());
        product.setBrand(productObj.getBrand());
        product.setPrice(productObj.getPrice());
        product.setDiscountPrice(productObj.getDiscountPrice());
        product.setImageLocation(productObj.getImageLocation());
        product.setPopularity(Popularity.valueOf(productObj.getPopularity()));
        product.setIntroductoryDate(LocalDate.parse(productObj.getIntroductoryDate()));
        if (Objects.nonNull(productObj.getIsHeadline())) {
            product.setIsHeadline(productObj.getIsHeadline());
        }
    }
}
