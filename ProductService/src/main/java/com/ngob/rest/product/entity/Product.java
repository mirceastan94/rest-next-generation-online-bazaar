package com.ngob.rest.product.entity;

import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * This is the products entity class
 *
 * @author Mircea Stan
 */
@Entity
@Getter
@Setter
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(updatable = false, unique = true)
    @Expose
    private UUID id;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Review> reviews = new HashSet<>();

    @NotBlank
    @Size(max = 255)
    @Expose
    private String name;

    @NotBlank
    @Size(max = 255)
    @Expose
    private String description;

    @NotBlank
    @Size(max = 255)
    @Expose
    private String category;

    @NotBlank
    @Size(max = 255)
    @Expose
    private String subCategory;

    @NotNull
    @Expose
    private Integer stockCount;

    @NotBlank
    @Size(max = 255)
    @Expose
    private String brand;

    @NotNull
    @Expose
    private Integer price;

    @Expose
    private Integer discountPrice;

    @NotNull
    @Expose
    private Double rating = 0.0;

    @NotBlank
    @Size(max = 255)
    @Expose
    private String imageLocation;

    @Enumerated(EnumType.STRING)
    private Popularity popularity;

    @NotNull
    @Expose
    private LocalDate introductoryDate;

    @NotNull
    @Expose
    private Boolean isHeadline = false;

}
