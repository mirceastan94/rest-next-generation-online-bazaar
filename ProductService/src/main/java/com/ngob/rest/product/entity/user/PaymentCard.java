package com.ngob.rest.product.entity.user;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.UUID;

/**
 * This is the payment cards entity class
 *
 * @author Mircea Stan
 */
@Entity
@Getter
@Setter
@Table(name = "payment_cards")
public class PaymentCard implements Serializable {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(updatable = false, unique = true)
    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY)
    private UserDetails userDetails;

    @NotBlank
    @Size(max = 255)
    private String ownerName;

    @NotBlank
    @Size(max = 20)
    private String number;

    @NotBlank
    @Size(max = 255)
    private String expiryDate;

    @NotNull
    private Integer cvvCvcCode;

}
