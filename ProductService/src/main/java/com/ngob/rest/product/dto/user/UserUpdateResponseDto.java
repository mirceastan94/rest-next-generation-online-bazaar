package com.ngob.rest.product.dto.user;

import lombok.Getter;
import lombok.Setter;

/**
 * This is the upser profile update response class
 *
 * @author Mircea Stan
 */
@Getter
@Setter
public class UserUpdateResponseDto {

    private Boolean success = Boolean.TRUE;
    private String message = "Your request has been processed.";
    private Boolean loginRequired = Boolean.FALSE;

}
