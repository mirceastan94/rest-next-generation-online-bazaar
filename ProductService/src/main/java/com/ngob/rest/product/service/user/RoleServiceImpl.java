package com.ngob.rest.product.service.user;

import com.ngob.rest.product.entity.user.Role;
import com.ngob.rest.product.entity.user.RoleName;
import com.ngob.rest.product.repository.user.RoleRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * This is the role service implementation
 *
 * @author Mircea Stan
 */
@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    private RoleRepository roleRepository;

    public RoleServiceImpl (RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Optional<Role> findByName(RoleName roleName){
        return roleRepository.findByName(roleName);
    }

}
