package com.ngob.rest.product.repository;

import com.ngob.rest.product.dto.product.ProductDto;
import com.ngob.rest.product.entity.Product;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Map;

/**
 * This is the Redis repository class used for caching products
 *
 * @author Mircea Stan
 */
@Repository
public class RedisRepositoryImpl implements RedisRepository {

    public static final String KEY = "Products";

    private RedisTemplate<String, Object> redisTemplate;
    private HashOperations hashOperations;

    public RedisRepositoryImpl(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @PostConstruct
    private void init() {
        hashOperations = redisTemplate.opsForHash();
    }

    @Override
    public Map<Object, ProductDto> findAllProducts() {
        return hashOperations.entries(KEY);
    }

    @Override
    public Product findProduct(String id) {
        return (Product) hashOperations.get(KEY, id);
    }

    @Override
    public void insertProduct(ProductDto productDto) {
        hashOperations.put(KEY, productDto.getId(), productDto);
    }

    @Override
    public void deleteProduct(String id) {
        hashOperations.delete(KEY, id);
    }

}
