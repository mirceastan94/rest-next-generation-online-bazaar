package com.ngob.rest.product.utils;

/**
 * This is the Constants util class
 *
 * @author Mircea Stan
 */
public class Constants {

    private Constants() {
    }

    public static final String DATE_PATTERN = "yyyy-MM-dd";
    public static final String GOOGLE = "google";
    public static final String FACEBOOK = "facebook";
    public static final String COMMA = ",";
    public static final String PERCENT = "%";
    public static final String DASH = "-";
    public static final String SPACE = " ";
    public static final String DEFAULT_ADMIN_NAME = "NGOB";
    public static final String DEFAULT_ADMIN_EMAIL = "ngob@gmail.com";
    public static final String DEFAULT_ADMIN_PASSWORD = "ngob";
    public static final String TEST_USER_NAME = "Test";
    public static final String TEST_USER_EMAIL = "test@yahoo.com";
    public static final String TEST_USER_PASSWORD = "test";
    public static final String LOCAL_LOGIN_ENDPOINT = "http://localhost:8080/product/user/login";

}
