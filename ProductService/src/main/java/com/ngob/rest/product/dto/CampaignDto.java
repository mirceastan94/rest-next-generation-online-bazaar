package com.ngob.rest.product.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ngob.rest.product.dto.product.ProductDto;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

/**
 * This is the campaign Dto class
 *
 * @author Mircea Stan
 */
@Data
public class CampaignDto {

    private UUID id;

    @NotNull
    private String name;

    @NotNull
    private LocalDate startDate;

    @NotNull
    private LocalDate endDate;

    @JsonProperty("status")
    private String campaignStatus;

    @NotNull
    @JsonProperty("campaignProducts")
    private List<ProductDto> productDtos;

}
