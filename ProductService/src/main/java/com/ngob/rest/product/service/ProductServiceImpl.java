package com.ngob.rest.product.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.GsonBuilder;
import com.ngob.rest.product.adapter.ProductAdapter;
import com.ngob.rest.product.campaign.CampaignStatus;
import com.ngob.rest.product.dto.CriteriaDto;
import com.ngob.rest.product.dto.UserProductsDto;
import com.ngob.rest.product.dto.product.ProductCategoryDto;
import com.ngob.rest.product.dto.product.ProductDto;
import com.ngob.rest.product.dto.product.StockUpdateDto;
import com.ngob.rest.product.entity.Product;
import com.ngob.rest.product.entity.UserProducts;
import com.ngob.rest.product.integration.json.EnvelopeJSON;
import com.ngob.rest.product.integration.utils.ProductObj;
import com.ngob.rest.product.mapper.ProductMapper;
import com.ngob.rest.product.repository.ProductRepository;
import com.ngob.rest.product.repository.QProductRepository;
import com.ngob.rest.product.repository.RedisRepositoryImpl;
import com.ngob.rest.product.repository.UserProductsRepository;
import com.ngob.rest.product.utils.Filtering;
import com.ngob.rest.product.utils.ProductPageableStaticFields;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.ResourceNotFoundException;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.ngob.rest.product.utils.Constants.DATE_PATTERN;

/**
 * This is the Product Service class, used to store, retrieve and update products
 *
 * @author Mircea Stan
 */
@Service
@Transactional
@Log4j2
public class ProductServiceImpl implements ProductService {

    private final CampaignService campaignService;
    private final RedisRepositoryImpl redisRepositoryImpl;
    private final QProductRepository qProductRepository;
    private final ProductRepository productRepository;
    private final UserProductsRepository userProductsRepository;
    private final ProductMapper productMapper;
    private final RabbitTemplate rabbitTemplate;
    private final Queue productIntegrationQueue;
    private final Queue productRetrievalQueue;

    ProductServiceImpl(CampaignService campaignService, RedisRepositoryImpl redisRepositoryImpl,
                       QProductRepository qProductRepository, ProductRepository productRepository,
                       UserProductsRepository userProductsRepository, ProductMapper productMapper,
                       RabbitTemplate rabbitTemplate, Queue productIntegrationQueue, Queue productRetrievalQueue) {
        this.campaignService = campaignService;
        this.redisRepositoryImpl = redisRepositoryImpl;
        this.qProductRepository = qProductRepository;
        this.productRepository = productRepository;
        this.userProductsRepository = userProductsRepository;
        this.productMapper = productMapper;
        this.rabbitTemplate = rabbitTemplate;
        this.productIntegrationQueue = productIntegrationQueue;
        this.productRetrievalQueue = productRetrievalQueue;
    }

    @Scheduled(fixedRate = 60 * 60 * 1000, initialDelay = 0)
    public void integrateNewProducts() throws IOException {
        log.info("Reading products file...");

        var objectMapper = new ObjectMapper();
        final var jsonEnvelope = objectMapper.readValue(ResourceUtils
                .getFile("classpath:static\\products\\products.json"), EnvelopeJSON.class);
        rabbitTemplate.convertAndSend(productIntegrationQueue.getName(), jsonEnvelope, messagePostProcessor -> {
            messagePostProcessor.getMessageProperties().getHeaders().putIfAbsent(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            return messagePostProcessor;
        });
        log.info("Products integration started...");

//        <------- XML parsing and processing alternative ------->
//        final var jaxbEnvelope = JAXB.unmarshal(ResourceUtils.getFile("classpath:static\\products\\products.xml"),
//                EnvelopeJAXB.class);
//        rabbitTemplate.convertAndSend(productIntegrationQueue.getName(), jaxbEnvelope, messagePostProcessor -> {
//            // JAXB class instance content will be converted automatically into a JSON, hence the JSON content-type below
//            messagePostProcessor.getMessageProperties().getHeaders().putIfAbsent(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
//            return messagePostProcessor;
//        });
    }

    public String insertOrUpdateProductFromObj(ProductObj productObj) {
        final var product = productRepository.findByName(productObj.getName()).orElse(new Product());
        ProductAdapter.toProduct(productObj, product);
        product.setIntroductoryDate(LocalDate.parse(productObj.getIntroductoryDate(), DateTimeFormatter.ofPattern(DATE_PATTERN)));
        productRepository.saveAndFlush(product);
        return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(product);
    }

    @Override
    public ProductDto getProduct(UUID productId) {
        log.info(String.format("Retrieving product with ID '%s'", productId));
        final var product = productRepository.findById(productId).orElseThrow(() -> new ResourceNotFoundException("Product", "ID", productId));
        final var productDto = productMapper.map(product, ProductDto.class);

        log.info(String.format("Product with ID '%s' retrieved and mapped", productId));
        return productDto;
    }

    @Override
    public ProductDto getProductWithCampaign(UUID productId, UUID campaignId) {
        log.info(String.format("Retrieving product with ID '%s' from campaign with ID '%s'", productId, campaignId));
        ProductDto productDto;
        final var searchedCampaign = campaignService.getCampaign(campaignId);
        final var campaignProducts = searchedCampaign.getProductDtos();
        if (campaignProducts.stream().anyMatch(currentCampaignProduct -> currentCampaignProduct.getId().equals(productId))
                && searchedCampaign.getCampaignStatus().equalsIgnoreCase(CampaignStatus.ACTIVE.getName())) {
            productDto = campaignProducts.stream().filter(currentCampaignProduct ->
                    currentCampaignProduct.getId().equals(productId)).findFirst().get();
        } else {
            ProductDto regularProduct = getProduct(productId);
            productDto = productMapper.map(regularProduct, ProductDto.class);
        }
        log.info(String.format("Product with ID '%s' retrieved and mapped", productId));
        return productDto;
    }

    @Override
    public Long countProductsByCriteria(CriteriaDto criteriaDto) {
        return qProductRepository.count(Filtering.createProductsFilter(criteriaDto.getFilterCriteria(), ProductPageableStaticFields.filteringFieldsMap));
    }

    @Override
    public List<ProductDto> getProductsByCriteria(CriteriaDto criteriaDto) {
        log.info("Retrieving products by requested criteria...'");
        final Pageable pageable = PageRequest.of(criteriaDto.getPage(), criteriaDto.getSize(),
                Filtering.createOrderSpecifier(ProductPageableStaticFields.sortingFieldsMap, criteriaDto.getSortByField(),
                        criteriaDto.getOrder()));

        final BooleanExpression filterList = Filtering.createProductsFilter(criteriaDto.getFilterCriteria(),
                ProductPageableStaticFields.filteringFieldsMap);

        final List<Product> searchedProducts = qProductRepository.findAll(Objects.nonNull(filterList) ? filterList :
                Expressions.TRUE.isTrue(), pageable).getContent();
        log.info("Product founds, mapping...'");
        List<ProductDto> productDtos = productMapper.mapAsList(searchedProducts, ProductDto.class);

        if (Objects.nonNull(criteriaDto.getCampaignId())) {
            final var campaignProducts = campaignService.getCampaign(criteriaDto.getCampaignId())
                    .getProductDtos();
            productDtos.forEach(productDto -> {
                if (campaignProducts.stream().anyMatch(campaignProduct ->
                        campaignProduct.getId().equals(productDto.getId()))) {
                    final var campaignProduct = campaignProducts.stream().filter(currentCampaignProduct ->
                            currentCampaignProduct.getId().equals(productDto.getId())).findFirst().get();
                    productDto.setDiscountPrice(campaignProduct.getDiscountPrice());
                }
            });
        }
        return productDtos;
    }

    @Override
    public List<ProductCategoryDto> getProductsCategoriesWithCount() {
        return productRepository.getProductCategoryDtos();
    }

    @RabbitListener(queues = "${queues.products.stock.update.queue}")
    public void receivedProduct(final HashSet<StockUpdateDto> stockUpdateDtos) {
        log.info("Products stock update received: {}", stockUpdateDtos);
        final var productsToBeUpdated = new HashSet<Product>();
        stockUpdateDtos.forEach(stockUpdateDto -> {
            final var product = productRepository.findById(stockUpdateDto.getProductId())
                    .orElseThrow(() -> new ResourceNotFoundException("Product", "ID", stockUpdateDto.getProductId()));
            product.setStockCount(stockUpdateDto.getProductStockCount());
            productsToBeUpdated.add(product);
        });
        productRepository.saveAll(productsToBeUpdated);
        log.info("Updated Products persisted in DB: {}", productsToBeUpdated);

        productsToBeUpdated.forEach(product -> rabbitTemplate
                .convertAndSend(productRetrievalQueue.getName(), productMapper.map(product, ProductDto.class)));
    }

    @RabbitListener(queues = "${queues.user.products.relationship.queue}")
    public void receivedUserProductDto(final UserProductsDto userProductsDto) {
        log.info("User products DTO received: {}", userProductsDto);
        final var userProducts = new HashSet<UserProducts>();
        final var userId = userProductsDto.getUserId();
        userProductsDto.getProductsIds().forEach(productId ->
                userProducts.add(new UserProducts(userId, productId)));
        userProductsRepository.saveAll(userProducts);
        log.info("Added user products relationship(s)!");
    }

    @Override
    @Scheduled(fixedRate = 60 * 60 * 1000, initialDelay = 10000)
    public void cacheCurrentProducts() {
        final var products = productRepository.findAll();
        final var productsDtos = productMapper.mapAsList(products, ProductDto.class);
        productsDtos.forEach(redisRepositoryImpl::insertProduct);
    }

    @Override
    public List<ProductDto> getCachedProducts() {
        return new ArrayList<>(redisRepositoryImpl.findAllProducts().values());
    }

    @Override
    public Long getProductsCount() {
        return productRepository.count();
    }

}
