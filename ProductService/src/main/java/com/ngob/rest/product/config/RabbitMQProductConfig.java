package com.ngob.rest.product.config;

import com.rabbitmq.client.ConnectionFactory;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * This is the RabbitMQ product config class
 */
@Configuration
public class RabbitMQProductConfig {

    @Value("${queues.users.register.queue}")
    private String userRegisterQueue;

    @Value("${queues.users.login.queue}")
    private String userLoginQueue;

    @Value("${queues.products.integration.queue}")
    private String productIntegrationQueue;

    @Value("${queues.products.retrieval.queue}")
    private String productRetrievalQueue;

    @Value("${queues.products.stock.update.queue}")
    private String productsStockUpdateQueue;

    @Value("${queues.user.products.relationship.queue}")
    private String userProductsRelationshipQueue;

    @Value("${queues.ack.queue}")
    private String acknowledgeQueue;

    @Value("${spring.rabbitmq.host}")
    private String host;

    @Value("${spring.rabbitmq.username}")
    private String username;

    @Value("${spring.rabbitmq.password}")
    private String password;

    private AmqpAdmin amqpAdmin;

    public RabbitMQProductConfig(AmqpAdmin amqpAdmin) {
        this.amqpAdmin = amqpAdmin;
    }

    @Bean(name = "userRegisterQueue")
    public Queue createUserRegisterQueue() {
        return new Queue(userRegisterQueue);
    }

    @Bean(name = "userLoginQueue")
    public Queue createUserLoginQueue() {
        return new Queue(userLoginQueue);
    }

    @Bean(name = "productIntegrationQueue")
    public Queue createProductIntegrationQueue() {
        return new Queue(productIntegrationQueue);
    }

    @Bean(name = "productRetrievalQueue")
    public Queue createProductRetrievalQueue() {
        return new Queue(productRetrievalQueue);
    }

    @Bean(name = "productStockUpdateQueue")
    public Queue createProductsStockUpdateQueue() {
        return new Queue(productsStockUpdateQueue);
    }

    @Bean(name = "userProductsRelationshipQueue")
    public Queue createUserProductsRelationshipQueue() {
        return new Queue(userProductsRelationshipQueue);
    }

    @Bean(name = "acknowledgeQueue")
    public Queue createAcknowledgeQueue() {
        return new Queue(acknowledgeQueue);
    }

    @Bean
    public DirectExchange createUsersInfoExchange() {
        return new DirectExchange("users.info");
    }

    @Bean
    public DirectExchange createProductsInfoExchange() {
        return new DirectExchange("products.info");
    }

    @PostConstruct
    public void initializeQueues() {
        amqpAdmin.declareQueue(createUserRegisterQueue());
        amqpAdmin.declareQueue(createUserLoginQueue());
        amqpAdmin.declareQueue(createProductIntegrationQueue());
        amqpAdmin.declareQueue(createProductRetrievalQueue());
        amqpAdmin.declareQueue(createProductsStockUpdateQueue());
        amqpAdmin.declareQueue(createUserProductsRelationshipQueue());
        amqpAdmin.declareQueue(createAcknowledgeQueue());

        amqpAdmin.declareBinding(BindingBuilder.bind(createUserRegisterQueue()).to(createUsersInfoExchange())
                .with("register"));
        amqpAdmin.declareBinding(BindingBuilder.bind(createUserLoginQueue()).to(createUsersInfoExchange())
                .with("login"));
        amqpAdmin.declareBinding(BindingBuilder.bind(createProductIntegrationQueue()).to(createProductsInfoExchange())
                .with("integration"));
        amqpAdmin.declareBinding(BindingBuilder.bind(createProductRetrievalQueue()).to(createProductsInfoExchange())
                .with("retrieval"));
        amqpAdmin.declareBinding(BindingBuilder.bind(createProductsStockUpdateQueue()).to(createProductsInfoExchange())
                .with("stock"));
        amqpAdmin.declareBinding(BindingBuilder.bind(createUserProductsRelationshipQueue()).to(createProductsInfoExchange())
                .with("relationship"));
        amqpAdmin.declareBinding(BindingBuilder.bind(createAcknowledgeQueue()).to(createProductsInfoExchange())
                .with("ack"));
    }

    @Bean
    public ConnectionFactory createConnectionFactory() {
        final var connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(host);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        return connectionFactory;
    }

    @Bean
    public Jackson2JsonMessageConverter productJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

}
