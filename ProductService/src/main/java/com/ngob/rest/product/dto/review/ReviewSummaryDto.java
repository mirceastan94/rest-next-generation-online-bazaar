package com.ngob.rest.product.dto.review;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

/**
 * This is the Dto class for product Reviews with stats
 *
 * @author Mircea Stan
 */
@Getter
@Setter
public class ReviewSummaryDto {

    Set<ReviewDto> reviewDtos = new HashSet<>();
    Integer reviewsCount = 0;
    Double averageRating = 0.0D;

    @JsonProperty("excellent")
    Integer excellentRatingCount = 0;

    @JsonProperty("good")
    Integer goodRatingCount = 0;

    @JsonProperty("average")
    Integer averageRatingCount = 0;

    @JsonProperty("bad")
    Integer badRatingCount = 0;

    @JsonProperty("horrible")
    Integer horribleRatingCount = 0;

}
