package com.ngob.rest.product.service.user;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.ngob.rest.product.config.AppProperties;
import com.ngob.rest.product.dto.APIResponseDto;
import com.ngob.rest.product.dto.CriteriaDto;
import com.ngob.rest.product.dto.user.*;
import com.ngob.rest.product.entity.user.Language;
import com.ngob.rest.product.entity.user.RoleName;
import com.ngob.rest.product.entity.user.User;
import com.ngob.rest.product.entity.user.UserDetails;
import com.ngob.rest.product.exception.BadRequestException;
import com.ngob.rest.product.exception.InternalServerErrorException;
import com.ngob.rest.product.exception.UnprocessableEntityException;
import com.ngob.rest.product.mapper.UserMapper;
import com.ngob.rest.product.repository.QUserRepository;
import com.ngob.rest.product.repository.user.UserRepository;
import com.ngob.rest.product.security.JwtTokenProvider;
import com.ngob.rest.product.utils.Filtering;
import com.ngob.rest.product.utils.UserPageableStaticFields;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import lombok.extern.log4j.Log4j2;
import org.apache.http.client.utils.URIBuilder;
import org.elasticsearch.ResourceNotFoundException;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.*;

import static com.ngob.rest.product.adapter.UserAdapter.toMqUser;
import static com.ngob.rest.product.utils.Constants.FACEBOOK;
import static com.ngob.rest.product.utils.Constants.GOOGLE;

/**
 * This is the user service implementation
 *
 * @author Mircea Stan
 */
@Service
@Transactional
@Log4j2
public class UserServiceImpl implements UserService {

    private final RoleService roleService;
    private final UserRepository userRepository;
    private final QUserRepository qUserRepository;
    private final UserMapper userMapper;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider tokenProvider;
    private final RabbitTemplate rabbitTemplate;
    private final Queue userRegisterQueue;
    private final Queue userLoginQueue;
    private final NetHttpTransport netHttpTransport;
    private final GsonFactory gsonFactory;
    private final HttpClient httpClient;
    private final AppProperties appProperties;

    @PersistenceContext
    private EntityManager entityManager;

    public static final String ANOTHER_USER_WITH_THE_SAME_EMAIL_EXISTS_IN_THE_DATABASE_ALREADY
            = "Another user with the same email exists in the database already!";

    public static final String SEND_NEW_USER_REGISTER_ON_QUEUE = "Sending newly created user to users register queue...";
    public static final String SEND_UPDATED_USER_REGISTER_ON_QUEUE = "Sending updated user to users register queue...";
    public static final String SEND_SOCIAL_USER_REGISTER_ON_QUEUE = "Sending social logged in user details to users register queue...";

    public static final String NEW_USER_REGISTER_QUEUE_MESSAGE_SENT = "Registered user sent via MQ!";
    public static final String UPDATED_USER_REGISTER_QUEUE_MESSAGE_SENT = "Updated user sent via MQ!";
    public static final String SOCIAL_USER_REGISTER_QUEUE_MESSAGE_SENT = "Social login user sent via MQ!";


    public static final String USER = "User";
    public static final String EMAIL = "Email";

    public UserServiceImpl(RoleService roleService, UserRepository userRepository, QUserRepository qUserRepository,
                           UserMapper userMapper, AuthenticationManager authenticationManager,
                           JwtTokenProvider tokenProvider, RabbitTemplate rabbitTemplate, Queue userRegisterQueue,
                           Queue userLoginQueue, NetHttpTransport netHttpTransport, GsonFactory gsonFactory,
                           HttpClient httpClient, AppProperties appProperties) {
        this.roleService = roleService;
        this.userRepository = userRepository;
        this.qUserRepository = qUserRepository;
        this.userMapper = userMapper;
        this.authenticationManager = authenticationManager;
        this.tokenProvider = tokenProvider;
        this.rabbitTemplate = rabbitTemplate;
        this.userRegisterQueue = userRegisterQueue;
        this.userLoginQueue = userLoginQueue;
        this.netHttpTransport = netHttpTransport;
        this.gsonFactory = gsonFactory;
        this.httpClient = httpClient;
        this.appProperties = appProperties;
    }

    @Override
    public void createUser(RegisterUserDto registerUserDto) {
        // verifies if the email is already present in the users table
        if (existsUser(registerUserDto.getEmail())) {
            log.error(String.format("User with email '%s' already exists!", registerUserDto.getEmail()));
            throw new UnprocessableEntityException(ANOTHER_USER_WITH_THE_SAME_EMAIL_EXISTS_IN_THE_DATABASE_ALREADY);
        }

        // retrieves the user role from the roles table
        final var userRole = roleService.findByName(RoleName.ROLE_USER).orElseThrow(() ->
                new ResourceNotFoundException("Role", "name", "N/A"));

        // instantiates a new user
        var newUser = userMapper.map(registerUserDto, User.class);
        newUser.setRole(userRole);
        userRepository.saveAndFlush(newUser);
        newUser.setUserDetails(new UserDetails(newUser.getId(), Objects.nonNull(registerUserDto.getLanguage())
                ? Language.fromLabel(registerUserDto.getLanguage()) : Language.ENGLISH));
        userRepository.saveAndFlush(newUser);

        // sends the future authentication object to the users information queue
        sendUserDetailsToOrdersService(newUser, SEND_NEW_USER_REGISTER_ON_QUEUE, NEW_USER_REGISTER_QUEUE_MESSAGE_SENT);
    }

    @Override
    public JwtAuthResponseDto loginUser(LoginUserDto loginUserDto) {
        log.info(String.format("Logging user with email '%s'...", loginUserDto.getEmail()));

        // retrieves the typed credentials
        final var authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginUserDto.getEmail(), loginUserDto.getPassword()));

        // generates a new JWT token
        SecurityContextHolder.getContext().setAuthentication(authentication);
        log.info(String.format("User with email '%s' has just logged in!", loginUserDto.getEmail()));

        // sends the authentication object to the users information queue
        log.info("Sending current authentication details to users login queue...");
        rabbitTemplate.convertAndSend(userLoginQueue.getName(), Map.of("email", loginUserDto.getEmail(),
                "password", loginUserDto.getPassword()));
        log.info("Authentication details sent!");

        // retrieves user entity from DB with the profile picture
        final var loggedUser = userRepository.findByEmail(loginUserDto.getEmail()).orElseThrow(()
                -> new ResourceNotFoundException(USER, EMAIL, loginUserDto.getEmail()));

        log.info("Generating updated token response...");
        return tokenProvider.generateAuthTokenResponse(authentication, userMapper.map(loggedUser.getUserDetails(), UserDetailsDto.class));
    }

    @Override
    public JwtAuthResponseDto loginUserSocially(SocialLoginUserDto loginUserDto) throws IOException, GeneralSecurityException,
            URISyntaxException, InterruptedException {
        // chooses the social login provider authentication method
        switch (loginUserDto.getProvider().toLowerCase()) {
            case GOOGLE: {
                return loginWithGoogle(loginUserDto);
            }
            case FACEBOOK: {
                return loginWithFacebook(loginUserDto);
            }
            default:
                throw new InternalServerErrorException("Social login provider not found!");
        }
    }

    public JwtAuthResponseDto loginWithGoogle(SocialLoginUserDto socialLoginUserDto) throws IOException,
            GeneralSecurityException {
        log.info(String.format("Logging user on Google with email '%s'...", socialLoginUserDto.getEmail()));

        // generates the verifier which will prove whether the user token is valid
        final GoogleIdTokenVerifier tokenVerifier = new GoogleIdTokenVerifier.Builder(netHttpTransport, gsonFactory)
                .setAudience(Collections.singletonList(appProperties.getGoogleClientId())).build();
        final var idToken = GoogleIdToken.parse(tokenVerifier.getJsonFactory(), socialLoginUserDto.getIdToken());
        final var isTokenValid = idToken != null && tokenVerifier.verify(idToken);

        // if it is, go on with the authentication
        if (isTokenValid) {
            log.info(String.format("Google token for user with email '%s' validated!", socialLoginUserDto.getEmail()));
            return performSocialAuthentication(socialLoginUserDto);
        } else {
            throw new BadRequestException("User token is invalid!");
        }
    }

    public JwtAuthResponseDto loginWithFacebook(SocialLoginUserDto socialLoginUserDto) throws IOException,
            URISyntaxException, InterruptedException {
        log.info(String.format("Logging user on Facebook with email '%s'...", socialLoginUserDto.getEmail()));

        // building the Facebook token check URI and interrogate the API
        var facebookGraphURI = new URIBuilder()
                .setScheme(appProperties.getFacebookGraphScheme())
                .setHost(appProperties.getFacebookGraphHost())
                .setPath(appProperties.getFacebookGraphPath())
                .setParameter("access_token", socialLoginUserDto.getAuthToken())
                .build();

        var facebookRequest = HttpRequest.newBuilder().GET().uri(facebookGraphURI).build();
        var facebookHttpResp = httpClient.send(facebookRequest, HttpResponse.BodyHandlers.ofString());
        final Object facebookRespObj = new Gson().fromJson(facebookHttpResp.body(), Object.class);
        final Map<String, String> facebookRespMap = (LinkedTreeMap) facebookRespObj;

        // if the token gets validated, go on with the authentication
        if (facebookRespMap.containsKey("id") && facebookRespMap.get("id")
                .equalsIgnoreCase(socialLoginUserDto.getId())) {
            log.info(String.format("Facebook token for user with email '%s' validated!",
                    socialLoginUserDto.getEmail()));
            return performSocialAuthentication(socialLoginUserDto);
        } else {
            throw new BadRequestException(String.format("Facebook token for user with email '%s' " +
                    "could not be validated!", socialLoginUserDto.getEmail()));
        }
    }

    public JwtAuthResponseDto performSocialAuthentication(SocialLoginUserDto loginUserDto) {
        // stores the user in the database if it was not saved already
        if (!existsUser(loginUserDto.getEmail())) {
            loginUserDto.setPassword(appProperties.getGoogleClientPassword());
            createUser(userMapper.map(loginUserDto, RegisterUserDto.class));
        }

        // retrieves the typed credentials
        final var authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginUserDto.getEmail(), appProperties.getGoogleClientPassword()));

        // generates a new JWT token
        SecurityContextHolder.getContext().setAuthentication(authentication);
        log.info(String.format("User with email '%s' has just logged in!", loginUserDto.getEmail()));


        // retrieves user entity from DB with the profile picture
        final var loggedUser = userRepository.findByEmail(loginUserDto.getEmail()).orElseThrow(()
                -> new ResourceNotFoundException(USER, EMAIL, loginUserDto.getEmail()));

        sendUserDetailsToOrdersService(toMqUser(loggedUser), SEND_SOCIAL_USER_REGISTER_ON_QUEUE, SOCIAL_USER_REGISTER_QUEUE_MESSAGE_SENT);

        entityManager.detach(loggedUser);

        // sends the authentication object to the users information queue
        log.info("Sending current authentication details to users login queue...");
        rabbitTemplate.convertAndSend(userLoginQueue.getName(), Map.of("email", loginUserDto.getEmail(),
                "password", appProperties.getGoogleClientPassword()));
        log.info("Authentication details sent!");

        log.info("Generating updated token response...");
        return tokenProvider.generateAuthTokenResponse(authentication, userMapper.map(loggedUser.getUserDetails(), UserDetailsDto.class));
    }

    @Override
    public UserDto getUserProfile(String email) {
        final var user = userRepository.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException(USER, EMAIL, email));
        return userMapper.map(user, UserDto.class);
    }

    @Override
    public UserUpdateResponseDto updateUserProfile(UserDto updatedUserDto) {
        final var userUpdateResponseDto = new UserUpdateResponseDto();
        if (Objects.nonNull(updatedUserDto.getPreviousEmail()) && updatedUserDto.getEmailChanged() &&
                (!updatedUserDto.getEmail().equalsIgnoreCase(updatedUserDto.getPreviousEmail()))) {
            if (existsUser(updatedUserDto.getEmail())) {
                log.error(String.format("User with email '%s' already exists, update aborted!", updatedUserDto.getEmail()));
                userUpdateResponseDto.setSuccess(Boolean.FALSE);
                userUpdateResponseDto.setMessage(ANOTHER_USER_WITH_THE_SAME_EMAIL_EXISTS_IN_THE_DATABASE_ALREADY);
                throw new UnprocessableEntityException(ANOTHER_USER_WITH_THE_SAME_EMAIL_EXISTS_IN_THE_DATABASE_ALREADY);
            } else {
                userUpdateResponseDto.setLoginRequired(Boolean.TRUE);
            }
        }
        final var existingUser = userRepository.findByEmail(Objects.nonNull(updatedUserDto.getPreviousEmail())
                        && updatedUserDto.getEmailChanged() ? updatedUserDto.getPreviousEmail() : updatedUserDto.getEmail())
                .orElseThrow(() -> new ResourceNotFoundException(USER, EMAIL, updatedUserDto.getPreviousEmail()));

        log.info(String.format("Updating user profile with email '%s'", updatedUserDto.getEmail()));
        userMapper.map(updatedUserDto, existingUser);
        final var userDetailsDto = updatedUserDto.getUserDetailsDto();
        if (Objects.nonNull(userDetailsDto.getPicture())) {
            existingUser.getUserDetails().setPicture(userDetailsDto.getPicture().getBytes(StandardCharsets.UTF_8));
        }
        if (Objects.nonNull(userDetailsDto.getLanguage())) {
            existingUser.getUserDetails().setLanguage(Language.fromLabel(userDetailsDto.getLanguage()));
        }

        log.info(String.format("Persisting user profile with email '%s'", updatedUserDto.getEmail()));
        userRepository.saveAndFlush(existingUser);

        // sends the updated authentication object to the users information queue
        sendUserDetailsToOrdersService(toMqUser(existingUser), SEND_UPDATED_USER_REGISTER_ON_QUEUE, UPDATED_USER_REGISTER_QUEUE_MESSAGE_SENT);

        // detaches the current existingUser as it's going to be changed for MQ transfer
        entityManager.detach(existingUser);

        return userUpdateResponseDto;
    }

    @Override
    public Long getUsersCount() {
        return userRepository.count();
    }

    @Override
    public List<UserDto> getUsersByCriteria(CriteriaDto criteriaDto) {
        log.info("Retrieving users by requested criteria...'");
        final Pageable pageable = PageRequest.of(criteriaDto.getPage(), criteriaDto.getSize(),
                Filtering.createOrderSpecifier(UserPageableStaticFields.sortingFieldsMap, criteriaDto.getSortByField(),
                        criteriaDto.getOrder()));

        final BooleanExpression filterList = Filtering.createUsersFilter(criteriaDto.getFilterCriteria(),
                UserPageableStaticFields.filteringFieldsMap);

        final List<User> searchedUsers = qUserRepository.findAll(Objects.nonNull(filterList) ? filterList :
                Expressions.TRUE.isTrue(), pageable).getContent();

        log.info("Users table has been searched, mapping found users if existing...'");
        return userMapper.mapAsList(searchedUsers, UserDto.class);
    }

    @Override
    public Long countUsersWithCriteria(CriteriaDto criteriaDto) {
        return qUserRepository.count(Filtering.createUsersFilter(criteriaDto.getFilterCriteria(),
                UserPageableStaticFields.filteringFieldsMap));
    }

    public APIResponseDto updateUserRole(UserRoleDto userRoleDto) {
        User userToUpdate = userRepository.findById(userRoleDto.getUserId()).orElseThrow(() ->
                new ResourceNotFoundException(USER, "ID", userRoleDto.getUserId()));
        if (!RoleName.valueOf(userRoleDto.getRole()).equals(userToUpdate.getRole().getName())) {
            final var role = roleService.findByName(RoleName.valueOf(userRoleDto.getRole())).orElseThrow(() ->
                    new ResourceNotFoundException("Role", "Name", userRoleDto.getRole()));
            userToUpdate.setRole(role);
            userRepository.save(userToUpdate);
            return new APIResponseDto(true, "User with ID '%s' has been updated with a new role!");
        }
        return new APIResponseDto(false, "User with ID '%s' already had the desired role!");
    }

    @Override
    public APIResponseDto deleteUser(UUID userId) {
        User userToSoftDelete = userRepository.findById(userId).orElseThrow(() ->
                new ResourceNotFoundException(USER, "ID", userId));
        userToSoftDelete.setIsDeleted(Boolean.TRUE);
        userRepository.save(userToSoftDelete);
        return new APIResponseDto(false, "User with ID '%s' has been deleted!");
    }

    private void sendUserDetailsToOrdersService(User user, String requestMessage, String acknowledgeMessage) {
        log.info(requestMessage);
        rabbitTemplate.convertAndSend(userRegisterQueue.getName(), user);
        log.info(acknowledgeMessage);
    }

    @Override
    public boolean existsUser(String email) {
        return userRepository.existsByEmail(email);
    }

}

