package com.ngob.rest.product.integration.processor;

import com.ngob.rest.product.entity.Product;
import com.ngob.rest.product.exception.InternalServerErrorException;
import com.ngob.rest.product.integration.jaxb.ProductJAXB;
import com.ngob.rest.product.integration.json.ProductJSON;
import com.ngob.rest.product.service.ProductService;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

/**
 * This is the product processor class
 */
@Component
public class ProductProcessor implements Processor {

    @Autowired
    private ProductService productService;

    @Override
    public void process(Exchange exchange) {
        switch (exchange.getIn().getHeader(HttpHeaders.CONTENT_TYPE).toString()) {
            case MediaType.APPLICATION_JSON_VALUE: {
                exchange.getIn().setBody(productService.insertOrUpdateProductFromObj(exchange.getIn().getBody(ProductJSON.class)));
                break;
            }
            case MediaType.APPLICATION_ATOM_XML_VALUE: {
                exchange.getIn().setBody(productService.insertOrUpdateProductFromObj(exchange.getIn().getBody(ProductJAXB.class)));
                break;
            }
            default: {
                throw new InternalServerErrorException("Invalid content-type used for persisting a product in the DB");
            }
        }
    }
}
