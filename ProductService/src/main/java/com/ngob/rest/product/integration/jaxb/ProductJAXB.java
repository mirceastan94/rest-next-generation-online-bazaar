package com.ngob.rest.product.integration.jaxb;

import com.ngob.rest.product.integration.utils.ProductObj;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.*;

/**
 * This is the product JAXB class
 */
@XmlRootElement(name = "product")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"name", "description", "category", "subCategory", "stockCount", "brand", "price", "discountPrice",
        "imageLocation", "popularity", "introductoryDate", "isHeadline"})
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
public class ProductJAXB extends ProductObj {

    @XmlElement(required = true)
    private String name;

    @XmlElement(required = true)
    private String description;

    @XmlElement(required = true)
    private String category;

    @XmlElement(required = true)
    private String subCategory;

    @XmlElement(required = true)
    private Integer stockCount;

    @XmlElement(required = true)
    private String brand;

    @XmlElement(required = true)
    private Integer price;

    private Integer discountPrice;

    @XmlElement(required = true)
    private String imageLocation;

    @XmlElement(required = true)
    private String popularity;

    @XmlElement(required = true)
    private String introductoryDate;

    @XmlElement()
    private Boolean isHeadline;

}
