package com.ngob.rest.product.campaign;

import com.ngob.rest.product.entity.Product;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * This is the campaign - product join table entity class
 *
 * @author Mircea Stan
 */
@Entity(name = "campaign_products")
@NoArgsConstructor
@Getter
@Setter
public class CampaignProduct {

    @EmbeddedId
    private CampaignProductId campaignProductId = new CampaignProductId();

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("campaignId")
    @JoinColumn(name = "campaign_id")
    private Campaign campaign;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("productId")
    @JoinColumn(name = "product_id")
    private Product product;

    @NotNull
    private Integer price;

    public CampaignProduct(Campaign campaign, Product product, Integer price) {
        this.campaign = campaign;
        this.product = product;
        this.price = price;
    }

    @Override
    public boolean equals(Object comparedObject) {
        if (comparedObject == null || getClass() != comparedObject.getClass())
            return false;

        if (this == comparedObject)
            return true;

        final var comparedCampaignProduct = (CampaignProduct) comparedObject;
        return Objects.equals(campaign, comparedCampaignProduct.campaign)
                && Objects.equals(product, comparedCampaignProduct.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(campaign, product);
    }

}
