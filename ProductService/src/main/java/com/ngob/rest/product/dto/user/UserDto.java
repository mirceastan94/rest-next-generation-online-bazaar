package com.ngob.rest.product.dto.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

/**
 * This is the user Dto
 *
 * @author Mircea Stan
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    private UUID id;
    private String name;
    private String previousEmail;
    private String email;
    private Boolean emailChanged;
    private String role;
    private String password;
    private UserDetailsDto userDetailsDto;

}
