DO $$
BEGIN

    -- inserts the two possible user roles in the according table
    INSERT INTO roles (id, name) VALUES (uuid_generate_v4(), 'ROLE_USER') ON CONFLICT DO NOTHING;
    INSERT INTO roles (id, name) VALUES (uuid_generate_v4(), 'ROLE_ADMIN') ON CONFLICT DO NOTHING;

END $$;