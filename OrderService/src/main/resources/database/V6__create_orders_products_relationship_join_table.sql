DO $$
BEGIN

    -- creates the join table between orders and products if not already existing
    CREATE TABLE IF NOT EXISTS public.order_product(
        order_id uuid REFERENCES orders(id) ON UPDATE CASCADE ON DELETE CASCADE,
        product_id uuid REFERENCES products(id) ON UPDATE CASCADE ON DELETE CASCADE,
        quantity INTEGER NOT NULL,
        total_price NUMERIC NOT NULL,
        CONSTRAINT order_product_pk PRIMARY KEY(order_id, product_id)
    );

END $$;