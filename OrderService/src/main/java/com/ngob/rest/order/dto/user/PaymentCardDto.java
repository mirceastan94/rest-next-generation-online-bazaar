package com.ngob.rest.order.dto.user;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

/**
 * This is the payment card Dto
 *
 * @author Mircea Stan
 */
@Getter
@Setter
public class PaymentCardDto {

    private UUID id;
    private String ownerName;
    private String number;
    private String expiryDate;
    private Integer cvvCvcCode;

}
