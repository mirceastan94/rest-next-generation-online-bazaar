package com.ngob.rest.order.handler;

import com.ngob.rest.order.dto.order.OrderRequestDto;
import com.ngob.rest.order.dto.order.OrderResponseDto;
import com.ngob.rest.order.entity.order.Order;
import com.ngob.rest.order.mapper.OrderMapper;
import com.ngob.rest.order.mapper.elastic.ElasticOrderMapper;
import com.ngob.rest.order.repository.OrderRepository;
import com.ngob.rest.order.repository.user.UserRepository;
import com.ngob.rest.order.service.elastic.OrderElasticService;
import com.ngob.rest.order.service.product.ProductService;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.function.IntBinaryOperator;

/**
 * This is the persistence handler class
 *
 * @author Mircea Stan
 */
@Slf4j
@NoArgsConstructor
public class PersistenceHandler extends OrderHandler {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrderElasticService orderElasticService;

    @Autowired
    private ProductService productService;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private ElasticOrderMapper elasticOrderMapper;

    @Override
    public void handleCurrentOperation(OrderRequestDto orderRequestDto, OrderResponseDto orderResponseDto)
            throws IOException {
        log.info("Initiating persistence stage...");
        final var orderUser = userRepository.findByEmail(orderRequestDto.getUserDto().getEmail()).orElseGet(() ->
                userRepository.findByEmail(orderRequestDto.getUserDto().getPreviousEmail()).orElseThrow(() ->
                        new ResourceNotFoundException("User", "Email", orderRequestDto.getUserDto().getEmail())));
        final var order = orderMapper.map(orderRequestDto, Order.class);
        order.setUser(orderUser);
        orderRepository.saveAndFlush(order);

        log.info("Order persistence in the database completed, moving on to ElasticSearch indexing...");
        final var elasticOrder = elasticOrderMapper.map(orderRequestDto, com.ngob.rest.order.entity.elastic.Order.class);
        elasticOrder.setId(order.getId().toString());
        orderElasticService.indexOrder(elasticOrder);

        orderResponseDto.setDateOfPurchase(order.getDateOfPurchase());
        orderResponseDto.setOrderId(order.getId().toString());

        log.info("Persistence completed, updating stock count on products service via MQ...");
        final IntBinaryOperator decreaseStockCountOperator =
                (productStockCount, orderQuantity) -> productStockCount - orderQuantity;
        productService.updateProductsStockCount(order.getProducts(), decreaseStockCountOperator);
        productService.updateUserProductsRelationship(orderUser.getId(), orderResponseDto.getInStockCartProductsDtos());
        log.info("Order processing completed successfully!");
    }

}
