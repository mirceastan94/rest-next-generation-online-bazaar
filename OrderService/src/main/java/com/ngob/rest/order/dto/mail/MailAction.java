package com.ngob.rest.order.dto.mail;

import lombok.Getter;

/**
 * This is the user language enum
 *
 * @author Mircea Stan
 */
@Getter
public enum MailAction {

    CONFIRM("Confirmation"),
    CANCEL("Cancellation");

    private String label;

    MailAction(String label) {
        this.label = label;
    }

}
