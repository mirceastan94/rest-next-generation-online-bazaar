package com.ngob.rest.order.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngob.rest.order.entity.elastic.Order;
import com.ngob.rest.order.entity.order.OrderStatus;
import com.ngob.rest.order.exception.InternalServerErrorException;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.ngob.rest.order.utils.Utils.LONDON_ZONE;

/**
 * This class is the Dao class for the elastic order entities
 */
@Repository
@Log4j2
public class OrderElasticDaoImpl implements OrderElasticDao {

    private final RestHighLevelClient restHighLevelClient;
    private final ObjectMapper objectMapper;
    private final ResourceLoader resourceLoader;

    @Value("${elasticsearch.index.orders}")
    private String ES_ORDER_INDEX;

    private final static String ES_MAPPINGS_PATH = "classpath:/static/mappings/es.json";

    @Autowired
    OrderElasticDaoImpl(final RestHighLevelClient restHighLevelClient, final ObjectMapper objectMapper,
                        final ResourceLoader resourceLoader) {
        this.restHighLevelClient = restHighLevelClient;
        this.objectMapper = objectMapper;
        this.resourceLoader = resourceLoader;
    }

    @Override
    public void initializeIndexStructure() throws IOException, URISyntaxException {
        final var getIndexRequest = new GetIndexRequest(ES_ORDER_INDEX);
        if (restHighLevelClient.indices().exists(getIndexRequest, RequestOptions.DEFAULT)) {
            log.warn(String.format("Index '%s' was already generated!", ES_ORDER_INDEX));
        } else {
            final var mappingsStr = readResource(ES_MAPPINGS_PATH);
            final var createIndexRequest = new CreateIndexRequest(ES_ORDER_INDEX);
            createIndexRequest.settings(Settings.builder()
                    .put("index.number_of_shards", 3)
                    .put("index.number_of_replicas", 2));
            createIndexRequest.mapping(mappingsStr, XContentType.JSON);

            final var createIndexResponse = restHighLevelClient.indices().create(createIndexRequest,
                    RequestOptions.DEFAULT);
            if (!createIndexResponse.isAcknowledged() || !createIndexResponse.isShardsAcknowledged()) {
                throw new InternalServerErrorException(String.format("Index '%s' wasn't computed correctly!", ES_ORDER_INDEX));
            }
            log.info(String.format("Index '%s' has been successfully computed!", ES_ORDER_INDEX));
        }
    }

    public List<Order> findAllOrders() throws IOException {
        log.info("Searching all orders within the index...");
        final var searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());

        return getOrders(searchSourceBuilder);
    }

    @Override
    public List<Order> findOrdersBy(String searchedParameter, String searchedValue) throws IOException {
        log.info("Searching custom orders within the index...");
        final var searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchQuery(searchedParameter, searchedValue));

        return getOrders(searchSourceBuilder);
    }

    public String indexNewOrder(final Order order) throws IOException {
        log.info("Indexing the new order document to the index...");
        final var indexRequest = new IndexRequest(ES_ORDER_INDEX);
        indexRequest.id(order.getId());
        indexRequest.source(objectMapper.writeValueAsString(order), XContentType.JSON);

        final var indexResponse = restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
        if (indexResponse.status().getStatus() != 200 && indexResponse.status().getStatus() != 201) {
            log.error(String.format("New order indexing failed with status '%s'", indexResponse.status().getStatus()));
            throw new InternalServerErrorException("New order indexing failed!");
        }

        return indexResponse.getIndex();
    }

    public String updateIndexedOrder(final Order order) throws IOException {
        log.info("Indexing the new order document to the index...");
        final var updateRequest = new UpdateRequest(ES_ORDER_INDEX, order.getId());
        updateRequest.doc(objectMapper.writeValueAsString(order), XContentType.JSON);

        final var updateResponse = restHighLevelClient.update(updateRequest, RequestOptions.DEFAULT);
        if (updateResponse.status().getStatus() != 200 && updateResponse.status().getStatus() != 201) {
            log.error(String.format("Existing order update failed with status '%s'", updateResponse.status().getStatus()));
            throw new InternalServerErrorException("Updating indexed order failed!");
        }

        return updateResponse.getIndex();
    }

    @Scheduled(cron = "0 0 10 * * *", zone = LONDON_ZONE)
    public void updateOrdersStatus() throws IOException {
        final var allOrders = this.findAllOrders();
        allOrders.stream()
                .filter(order -> !order.getStatus().getStatusName().equals(OrderStatus.COMPLETED.getStatusName()))
                .forEach(order -> {
                    if (order.getDateOfPurchase().plusDays(1).isBefore(LocalDateTime.now(ZoneId.of(LONDON_ZONE)))) {
                        order.setStatus(OrderStatus.PENDING);
                    }
                    if (order.getDateOfPurchase().plusDays(2).isBefore(LocalDateTime.now(ZoneId.of(LONDON_ZONE)))) {
                        order.setStatus(OrderStatus.COMPLETED);
                    }
                });
        // Ought to be replaced with "BulkRequest" for optimal performance in case of many orders present
        for (Order currentOrder : allOrders) {
            this.updateIndexedOrder(currentOrder);
        }
    }

    public String readResource(String mappingsPath) throws IOException {
        final var resource = resourceLoader.getResource(mappingsPath);
        return Files.readString(Path.of(resource.getFile().getPath()), StandardCharsets.UTF_8);
    }

    /**
     * This method retrieves the orders from the ES index based on a custom defined builder
     *
     * @param searchSourceBuilder
     * @return
     * @throws IOException
     */
    private List<Order> getOrders(SearchSourceBuilder searchSourceBuilder) throws IOException {
        final var searchRequest = new SearchRequest(ES_ORDER_INDEX);
        searchRequest.source(searchSourceBuilder);

        final var searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        final var hits = searchResponse.getHits().getHits();
        if (hits.length == 0) {
            return new ArrayList<>();
        }
        final var orders = Arrays.stream(hits)
                .map(hit -> objectMapper.convertValue(hit.getSourceAsMap(), Order.class))
                .collect(Collectors.toList());

        log.info("Orders retrieved from the ES index and mapped to their Java counterpart objects");
        return orders;
    }

}
