package com.ngob.rest.order.handler;

import com.ngob.rest.order.dto.order.OrderRequestDto;
import com.ngob.rest.order.dto.order.OrderResponseDto;
import com.ngob.rest.order.service.payment.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

/**
 * This is the payment handler class
 *
 * @author Mircea Stan
 */
@Slf4j
public class PaymentHandler extends OrderHandler {

    @Autowired
    private PaymentService paymentService;

    protected OrderHandler nextOrderHandler;

    public PaymentHandler(OrderHandler nextOrderHandler) {
        this.nextOrderHandler = nextOrderHandler;
    }

    @Override
    public void handleCurrentOperation(OrderRequestDto orderRequestDto, OrderResponseDto orderResponseDto) throws IOException {
        log.info("Initiating payment processing...");

// TODO This is just a sample of a Paypal payment mechanism using a Sandbox key, a real system must be implemented,
//  which must also include the approval of the customer, technically called Payer in this API, as part of the created payment Order
//
//        try {
//            var paymentOrder = paymentService.createOrder(orderRequestDto.getTotalCartPrice());
//            paymentOrder = paymentService.captureOrder(paymentOrder.id());
//            orderResponseDto.setOrderId(paymentOrder.id());
//            log.info("Payment completed successfully, continuing with the next handler...");
//            nextOrderHandler.handleCurrentOperation(orderRequestDto, orderResponseDto);
//        } catch (final IOException ex) {
//            log.error(ex.getMessage());
//            orderResponseDto.setSuccess(false);
//            orderResponseDto.setErrorMessage(PAYMENT_ERROR);
//            log.error("Payment unsuccessful, order processing aborted!");
//        }

        log.info("Payment completed successfully, continuing with the next handler...");
        nextOrderHandler.handleCurrentOperation(orderRequestDto, orderResponseDto);
    }

}
