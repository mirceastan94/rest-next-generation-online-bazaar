package com.ngob.rest.order.service.elastic;

import com.ngob.rest.order.dto.order.OrderDto;
import com.ngob.rest.order.entity.elastic.Order;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

/**
 * This is the elastic order service interface
 *
 * @author Mircea Stan
 */
public interface OrderElasticService {

    void initializeIndexStructure() throws IOException, URISyntaxException;
    List<OrderDto> getAllOrders() throws IOException;
    List<OrderDto> getOrdersByParam(String searchedParameter, String searchedValue) throws IOException;
    String indexOrder(Order orderRequestDto) throws IOException;
}
