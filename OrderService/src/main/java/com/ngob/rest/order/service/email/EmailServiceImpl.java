package com.ngob.rest.order.service.email;

import com.ngob.rest.order.dto.APIResponseDto;
import com.ngob.rest.order.dto.mail.MailAction;
import com.ngob.rest.order.dto.mail.SendEmailDto;
import com.ngob.rest.order.entity.order.Order;
import com.ngob.rest.order.entity.user.User;
import com.ngob.rest.order.repository.OrderRepository;
import org.elasticsearch.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.AbstractMap;
import java.util.Locale;
import java.util.Map;

import static com.ngob.rest.order.utils.Utils.*;

/**
 * This class is utilised to send Emails to users regarding their order(s) processing confirmation
 */
@Service
public class EmailServiceImpl implements EmailService {

    @Value("${spring.mail.username}")
    private String mailAddress;

    @Value("${host.url}")
    private String hostURL;

    private JavaMailSender mailSender;
    private TemplateEngine templateEngine;
    private ResourceBundleMessageSource resourceBundleMessageSource;
    private OrderRepository orderRepository;

    public EmailServiceImpl(JavaMailSender mailSender, TemplateEngine templateEngine,
                            ResourceBundleMessageSource resourceBundleMessageSource,
                            OrderRepository orderRepository) {
        this.mailSender = mailSender;
        this.templateEngine = templateEngine;
        this.resourceBundleMessageSource = resourceBundleMessageSource;
        this.orderRepository = orderRepository;
    }

    public APIResponseDto sendOrderEmail(SendEmailDto emailDto) throws MessagingException {
        final var order = orderRepository.findById(emailDto.getOrderId()).orElseThrow(() ->
                new ResourceNotFoundException("Order", "ID", emailDto.getOrderId()));
        final var userToBeEmailed = order.getUser();

        final var langLocale = userToBeEmailed.getUserDetails().getLanguage().getLabel();
        final var userLocale = new Locale(langLocale);

        final MimeMessage orderMailMessage = emailDto.getAction().equalsIgnoreCase(MailAction.CONFIRM.getLabel())
                ? generateConfirmationMail(order, userToBeEmailed, userLocale)
                : generateCancellationEmail(order, userToBeEmailed, userLocale);

        mailSender.send(orderMailMessage);
        return new APIResponseDto(true, String.format("Mail sent to user: %s", userToBeEmailed.getEmail()));
    }

    private MimeMessage generateConfirmationMail(Order order, User userToBeEmailed, Locale userLocale)
            throws MessagingException {
        final String mailTitle = resourceBundleMessageSource.getMessage(ORDER_CONFIRMATION_TITLE_PATH, null, userLocale);
        final String mailHeadsUp = resourceBundleMessageSource.getMessage(ORDER_CONFIRMATION_HEADS_UP_PATH, null,
                userLocale);
        final String mailDescription = resourceBundleMessageSource.getMessage(ORDER_CONFIRMATION_DESCRIPTION_PATH,
                null, userLocale);
        final String mailOrderPrice = resourceBundleMessageSource.getMessage(ORDER_CONFIRMATION_PRICE_PATH,
                null, userLocale);
        final String mailOrderDeliveryDate = resourceBundleMessageSource.getMessage(ORDER_CONFIRMATION_DELIVERY_DATE_PATH,
                null, userLocale);
        final String mailOrderStatusDetails = resourceBundleMessageSource.getMessage(ORDER_CONFIRMATION_STATUS_DETAILS_PATH,
                null, userLocale);
        final String mailWrapUp = resourceBundleMessageSource.getMessage(ORDER_CONFIRMATION_WRAP_UP_PATH,
                null, userLocale);
        final String mailOutro = resourceBundleMessageSource.getMessage(ORDER_CONFIRMATION_OUTRO_PATH,
                null, userLocale);

        final Map<String, Object> confirmationMailValuesMap = Map.ofEntries(
                new AbstractMap.SimpleEntry<>(MAIL_USER_NAME_LABEL, userToBeEmailed.getName()),
                new AbstractMap.SimpleEntry<>(MAIL_ORDER_ID_LABEL, order.getId()),
                new AbstractMap.SimpleEntry<>(MAIL_ORDER_PRICE_LABEL, order.getPrice()),
                new AbstractMap.SimpleEntry<>(MAIL_ORDER_LINK_LABEL,
                        hostURL.concat(ORDERS_URI_LABEL).concat(order.getId().toString())),
                new AbstractMap.SimpleEntry<>(MAIL_ORDER_DELIVERY_DATE_LABEL,
                        order.getDateOfPurchase().plusDays(2).toLocalDate()),
                new AbstractMap.SimpleEntry<>("mailHeadsUp", mailHeadsUp),
                new AbstractMap.SimpleEntry<>("mailDescription", mailDescription),
                new AbstractMap.SimpleEntry<>("mailOrderPrice", mailOrderPrice),
                new AbstractMap.SimpleEntry<>("mailOrderDeliveryDate", mailOrderDeliveryDate),
                new AbstractMap.SimpleEntry<>("mailOrderStatusDetails", mailOrderStatusDetails),
                new AbstractMap.SimpleEntry<>("mailWrapUp", mailWrapUp),
                new AbstractMap.SimpleEntry<>("mailOutro", mailOutro));

        final var context = new Context(userLocale);
        context.setVariables(confirmationMailValuesMap);
        final String mailHTMLContent = this.templateEngine.process(MAIL_ORDER_CONFIRMATION_TEMPLATE_NAME, context);

        return getMimeMessage(userToBeEmailed, mailTitle, mailHTMLContent);
    }

    private MimeMessage generateCancellationEmail(Order order, User userToBeEmailed, Locale userLocale)
            throws MessagingException {
        final String mailTitle = resourceBundleMessageSource.getMessage(ORDER_CANCELLATION_TITLE_PATH, null, userLocale);
        final String mailHeadsUp = resourceBundleMessageSource.getMessage(ORDER_CANCELLATION_HEADS_UP_PATH, null,
                userLocale);
        final String mailDescription = resourceBundleMessageSource.getMessage(ORDER_CANCELLATION_DESCRIPTION_PATH,
                null, userLocale);
        final String mailWrapUp = resourceBundleMessageSource.getMessage(ORDER_CANCELLATION_WRAP_UP_PATH,
                null, userLocale);
        final String mailOutro = resourceBundleMessageSource.getMessage(ORDER_CANCELLATION_OUTRO_PATH,
                null, userLocale);

        final Map<String, Object> orderMailValuesMap = Map.of(
                MAIL_USER_NAME_LABEL, userToBeEmailed.getName(),
                MAIL_ORDER_ID_LABEL, order.getId(),
                MAIL_ORDER_LINK_LABEL, hostURL.concat(ORDERS_URI_LABEL).concat(order.getId().toString()),
                "mailHeadsUp", mailHeadsUp, "mailDescription", mailDescription,
                "mailWrapUp", mailWrapUp, "mailOutro", mailOutro);

        final var context = new Context(userLocale);
        context.setVariables(orderMailValuesMap);
        final String mailHTMLContent = this.templateEngine.process(MAIL_ORDER_CANCELLATION_TEMPLATE_NAME, context);

        return getMimeMessage(userToBeEmailed, mailTitle, mailHTMLContent);
    }

    private MimeMessage getMimeMessage(User userToBeEmailed, String mailTitle, String mailHTMLContent)
            throws MessagingException {
        final var orderMailMessage = this.mailSender.createMimeMessage();
        final var messageHelper = new MimeMessageHelper(orderMailMessage, true, StandardCharsets.UTF_8.name());
        messageHelper.setFrom(new InternetAddress(mailAddress));
        messageHelper.setTo(new InternetAddress(userToBeEmailed.getEmail()));
        messageHelper.setSubject(mailTitle);
        messageHelper.setText(mailHTMLContent, true);
        messageHelper.addInline(LOGO, new ClassPathResource(MAIL_BRAND_LOGO_PATH));
        return orderMailMessage;
    }

}
