package com.ngob.rest.order.dto.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

/**
 * This is the product summary Dto class
 */
@EqualsAndHashCode(callSuper = false, of = {"id", "name", "category", "subCategory"})
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductSummaryDto {

    private String id;
    private String name;
    private String description;
    private String category;
    private String subCategory;
    private String brand;
    private Integer price;
    private Integer discountPrice;
    private String imageData;

}
