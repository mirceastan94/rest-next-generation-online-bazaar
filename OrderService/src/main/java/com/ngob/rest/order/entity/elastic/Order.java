package com.ngob.rest.order.entity.elastic;

import com.ngob.rest.order.entity.order.OrderStatus;
import lombok.*;
import org.springframework.data.annotation.Id;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * This is the elastic order class
 *
 * @author Mircea Stan
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Order {

    @Id
    private String id;
    private OrderStatus status = OrderStatus.CONFIRMED;
    private BigDecimal price;
    private LocalDateTime dateOfPurchase = LocalDateTime.now();
    private Cart cart = new Cart();
    private User user = new User();

}
