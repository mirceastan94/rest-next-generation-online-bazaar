package com.ngob.rest.order.dto.user;

import lombok.Getter;
import lombok.Setter;

/**
 * This is the JWTAuthentication response class
 *
 * @author Mircea Stan
 */
@Getter
@Setter
public class JwtAuthResponseDto {
    private String accessToken;
    private String tokenType = "Bearer";

    public JwtAuthResponseDto(String accessToken) {
        this.accessToken = accessToken;
    }

}
