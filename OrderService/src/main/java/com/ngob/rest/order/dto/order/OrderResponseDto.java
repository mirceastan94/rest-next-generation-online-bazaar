package com.ngob.rest.order.dto.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ngob.rest.order.dto.product.CartProductDto;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * This is the Order Response Dto class
 */
@Getter
@Setter
public class OrderResponseDto {

    private String orderId;
    @JsonProperty("outOfStockCartProducts")
    private Set<CartProductDto> outOfStockCartProductsDtos = new HashSet<>();
    @JsonProperty("inStockCartProducts")
    private Set<CartProductDto> inStockCartProductsDtos = new HashSet<>();
    private BigDecimal totalCartPrice;
    private LocalDateTime dateOfPurchase;
    private boolean success = true;
    private String errorMessage;

}
