package com.ngob.rest.order.dto.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.util.UUID;

/**
 * This is the product Dto class
 */
@EqualsAndHashCode(callSuper = false, of = {"id", "name", "category", "subCategory"})
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDto {

    private UUID id;
    private String name;
    private String description;
    private String category;
    private String subCategory;
    private Integer stockCount;
    private String brand;
    private Integer price;
    private Integer discountPrice;
    private String imageLocation;

}
