package com.ngob.rest.order.service.payment;

import com.paypal.core.PayPalHttpClient;
import com.paypal.http.HttpResponse;
import com.paypal.orders.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.ngob.rest.order.utils.Utils.CURRENCY;
import static com.ngob.rest.order.utils.Utils.PAYMENT_INTENT_CHECKOUT;

/**
 * This is the payment service implementation class
 *
 * @author Mircea Stan
 */
@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private PayPalHttpClient payPalHttpClient;

    @Override
    public Order createOrder(BigDecimal cartPrice) throws IOException {
        final List<PurchaseUnitRequest> purchaseUnits = new ArrayList<>();
        purchaseUnits.add(new PurchaseUnitRequest().amountWithBreakdown(new AmountWithBreakdown().currencyCode(CURRENCY).value(cartPrice.toPlainString())));

        final var createRequest = new OrderRequest();
        createRequest.checkoutPaymentIntent(PAYMENT_INTENT_CHECKOUT);
        createRequest.purchaseUnits(purchaseUnits);

        final OrdersCreateRequest orderRequest = new OrdersCreateRequest().requestBody(createRequest);
        final HttpResponse<Order> orderResponse = payPalHttpClient.execute(orderRequest);

        return orderResponse.result();
    }

    @Override
    public Order captureOrder(String orderId) throws IOException {
        final var captureRequest = new OrdersCaptureRequest(orderId);
        HttpResponse<Order> orderResponse = payPalHttpClient.execute(captureRequest);
        return orderResponse.result();
    }
}
