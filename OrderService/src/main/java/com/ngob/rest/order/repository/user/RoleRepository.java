package com.ngob.rest.order.repository.user;

import com.ngob.rest.order.entity.user.Role;
import com.ngob.rest.order.entity.user.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

/**
 * This is the role repository interface
 *
 * @author Mircea Stan
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, UUID> {

    Optional<Role> findByName(RoleName roleName);

}
