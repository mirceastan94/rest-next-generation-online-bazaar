package com.ngob.rest.order.entity.order;

import lombok.Getter;

/**
 * This is the order status enum
 *
 * @author Mircea Stan
 */
@Getter
public enum OrderStatus {
    CONFIRMED("Confirmed"),
    PENDING("Pending"),
    CANCELLED("Cancelled"),
    COMPLETED("Completed"),
    FAULTY("Faulty");

    private final String statusName;

    private OrderStatus(String statusName) {
        this.statusName = statusName;
    }

}
