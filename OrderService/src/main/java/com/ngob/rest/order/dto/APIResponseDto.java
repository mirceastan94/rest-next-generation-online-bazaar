package com.ngob.rest.order.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * This is the general clients API response class
 *
 * @author Mircea Stan
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class APIResponseDto {

    private Boolean success;
    private String message;

}