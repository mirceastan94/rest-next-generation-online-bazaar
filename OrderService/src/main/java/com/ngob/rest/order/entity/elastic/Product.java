package com.ngob.rest.order.entity.elastic;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * This is the elastic product class
 *
 * @author Mircea Stan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {

    private ProductDetails productDetails;
    private Integer quantity;
    private BigDecimal price;

}
