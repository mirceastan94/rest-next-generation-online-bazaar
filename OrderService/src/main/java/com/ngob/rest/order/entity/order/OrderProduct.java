package com.ngob.rest.order.entity.order;

import com.ngob.rest.order.entity.Product;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * This is the order - product join table entity class
 *
 * @author Mircea Stan
 */
@Entity(name = "order_product")
@NoArgsConstructor
@Getter
@Setter
public class OrderProduct {

    @EmbeddedId
    private OrderProductId orderProductId = new OrderProductId();

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("orderId")
    @JoinColumn(name = "order_id")
    private Order order;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("productId")
    @JoinColumn(name = "product_id")
    private Product product;

    @Column(name = "total_price")
    @NotNull
    private BigDecimal price;

    @Column
    @NotNull
    private Integer quantity;

    public OrderProduct(Order order, Product product, OrderProductId orderProductId) {
        this.order = order;
        this.product = product;
        this.orderProductId = orderProductId;
    }

    @Override
    public boolean equals(Object comparedObject) {
        if (comparedObject == null || getClass() != comparedObject.getClass())
            return false;

        if (this == comparedObject)
            return true;

        final var comparedOrderProduct = (OrderProduct) comparedObject;
        return Objects.equals(order, comparedOrderProduct.order)
                && Objects.equals(product, comparedOrderProduct.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(order, product);
    }
}
