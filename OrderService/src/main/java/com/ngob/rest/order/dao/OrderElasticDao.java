package com.ngob.rest.order.dao;

import com.ngob.rest.order.entity.elastic.Order;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

/**
 * This class is the Dao interface for the elastic order entities
 */
@Repository
public interface OrderElasticDao {

    void initializeIndexStructure() throws IOException, URISyntaxException;
    List<Order> findAllOrders() throws IOException;
    List<Order> findOrdersBy(String searchedParameter, String searchedValue) throws IOException;
    String indexNewOrder(Order order) throws IOException;
    String updateIndexedOrder(final Order order) throws IOException;
}
