package com.ngob.rest.order.service.user;

import com.ngob.rest.order.adapter.UserAdapter;
import com.ngob.rest.order.entity.user.User;
import com.ngob.rest.order.mapper.UserMapper;
import com.ngob.rest.order.repository.user.RoleRepository;
import com.ngob.rest.order.repository.user.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.ResourceNotFoundException;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static com.ngob.rest.order.utils.Utils.USER_REGISTERED_UPDATED;

/**
 * This is the user service implementation class
 *
 * @author Mircea Stan
 */
@Service
@Transactional
@Log4j2
public class UserServiceImpl implements UserService {

    @PersistenceContext
    private EntityManager entityManager;

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final AuthenticationManager authenticationManager;

    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository,
                           AuthenticationManager authenticationManager) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.authenticationManager = authenticationManager;
    }

    @RabbitListener(bindings = @QueueBinding(value = @Queue(value = "${queues.users.register.queue}"),
            exchange = @Exchange(value = "users.info"), key = "register"))
    public void registeredUser(final User registerUser) {
        log.info("Registering/updating user in orders microservice...");

        entityManager.detach(registerUser);

        final var userRole = roleRepository.findByName(registerUser.getRole().getName()).orElseThrow(()
                -> new ResourceNotFoundException("Role", "Name", registerUser.getRole().getName()));

        final var userToPersistOpt = userRepository.findByEmail(registerUser.getEmail());
        final var userToPersist = userToPersistOpt.orElseGet(() ->
                userRepository.findByEmail(registerUser.getPreviousEmail()).orElse(new User()));
        UserAdapter.enrichUser(registerUser, userToPersist);
        userToPersist.setRole(userRole);
        userToPersist.setUserDetails(registerUser.getUserDetails());

        final var userDetails = userToPersist.getUserDetails();
        userDetails.setUser(userToPersist);
        userDetails.getPaymentCards().stream().findFirst().ifPresent(paymentCard -> paymentCard.setUserDetails(userDetails));

        userRepository.saveAndFlush(userToPersist);

        log.info(String.format(USER_REGISTERED_UPDATED, userToPersist.getEmail()));
    }

    @RabbitListener(bindings = @QueueBinding(value = @Queue(value = "${queues.users.login.queue}"),
            exchange = @Exchange(value = "users.info"), key = "login"))
    public void loggedInUser(final Map<String, String> authenticationDetails) {
        log.info("Setting user authentication...");
        final var authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(authenticationDetails.get("email"),
                        authenticationDetails.get("password")));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        log.info("Authentication object set");
    }

    @Override
    public Boolean existsUser(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public Optional<User> getUser(UUID id) {
        return userRepository.findById(id);
    }

}
