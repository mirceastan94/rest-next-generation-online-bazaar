package com.ngob.rest.order.dto.product;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

/**
 * This class updates the stock count of a given product based on its ID
 */
@Getter
@Setter
@ToString
public class StockUpdateDto implements Serializable {

    @NotNull
    private UUID productId;

    @NotNull
    private Integer productStockCount;

}
