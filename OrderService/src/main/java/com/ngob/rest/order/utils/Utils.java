package com.ngob.rest.order.utils;

/**
 * This is the Utils class of the Orders service
 */
public class Utils {

    private Utils() {
    }
    public static final String USER_REGISTERED_UPDATED = "User with email '%s' has been registered/updated successfully!";
    public static final String OUT_OF_STOCK_PRODUCTS_FOUND = "Out of stock products found within this cart!";
    public static final String PAYMENT_ERROR = "Error while trying to perform the payment!";
    public static final String PAYMENT_INTENT_CHECKOUT = "CAPTURE";
    public static final String CURRENCY = "EUR";
    public static final String LONDON_ZONE = "Europe/London";
    public static final String ID_LABEL = "id";
    public static final String LOGO = "logo";
    public static final String COMMA = ", ";

    public static final String MAIL_ORDER_CONFIRMATION_TEMPLATE_NAME = "order-confirmation-email.html";
    public static final String MAIL_ORDER_CANCELLATION_TEMPLATE_NAME = "order-cancellation-email.html";

    public static final String ORDER_CONFIRMATION_TITLE_PATH = "confirmation.title";
    public static final String ORDER_CONFIRMATION_HEADS_UP_PATH = "confirmation.heads-up";
    public static final String ORDER_CONFIRMATION_DESCRIPTION_PATH = "confirmation.description";
    public static final String ORDER_CONFIRMATION_PRICE_PATH = "confirmation.price";
    public static final String ORDER_CONFIRMATION_DELIVERY_DATE_PATH = "confirmation.delivery-date";
    public static final String ORDER_CONFIRMATION_STATUS_DETAILS_PATH = "confirmation.status-details";
    public static final String ORDER_CONFIRMATION_WRAP_UP_PATH = "confirmation.wrap-up";
    public static final String ORDER_CONFIRMATION_OUTRO_PATH = "confirmation.outro";

    public static final String ORDER_CANCELLATION_TITLE_PATH = "cancellation.title";
    public static final String ORDER_CANCELLATION_HEADS_UP_PATH = "cancellation.heads-up";
    public static final String ORDER_CANCELLATION_DESCRIPTION_PATH = "cancellation.description";
    public static final String ORDER_CANCELLATION_WRAP_UP_PATH = "cancellation.wrap-up";
    public static final String ORDER_CANCELLATION_OUTRO_PATH = "cancellation.outro";

    public static final String MAIL_USER_NAME_LABEL = "userName";
    public static final String MAIL_ORDER_ID_LABEL = "orderId";
    public static final String MAIL_ORDER_PRICE_LABEL = "orderPrice";
    public static final String MAIL_ORDER_LINK_LABEL = "orderLink";
    public static final String MAIL_ORDER_DELIVERY_DATE_LABEL = "deliveryDate";
    public static final String MAIL_BRAND_LOGO_PATH = "static/images/logo.png";
    public static final String ORDERS_URI_LABEL = "/orders/";

}
