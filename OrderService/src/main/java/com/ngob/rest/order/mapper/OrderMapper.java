package com.ngob.rest.order.mapper;

import com.ngob.rest.order.dto.order.OrderDto;
import com.ngob.rest.order.dto.order.OrderRequestDto;
import com.ngob.rest.order.dto.product.CartProductDto;
import com.ngob.rest.order.entity.order.Order;
import com.ngob.rest.order.entity.order.OrderProduct;
import com.ngob.rest.order.repository.ProductRepository;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.elasticsearch.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * This is the order mapper class
 *
 * @author Mircea Stan
 */
@Component
public class OrderMapper extends ConfigurableMapper {

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductRepository productRepository;

    @Override
    protected void configure(MapperFactory mapperFactory) {

        mapperFactory.classMap(OrderRequestDto.class, Order.class)
                .field("totalCartPrice", "price")
                .field("userDto", "user")
                .customize(new CustomMapper<>() {
                    @Override
                    public void mapAtoB(OrderRequestDto orderRequestDto, Order order, MappingContext context) {
                        super.mapAtoB(orderRequestDto, order, context);
                        mapAsSet(orderRequestDto.getCartProductsDtos(), order);
                    }
                }).byDefault().register();

        mapperFactory.classMap(Order.class, OrderDto.class)
                .field("user", "userDto")
                .customize(new CustomMapper<>() {
                    @Override
                    public void mapAtoB(Order order, OrderDto orderDto, MappingContext context) {
                        super.mapAtoB(order, orderDto, context);
                        orderDto.setDateOfPurchase(order.getDateOfPurchase().toLocalDate().toString());
                        orderDto.setStatus(order.getStatus().name());
                    }
                }).byDefault().register();

    }

    public void mapAsSet(Set<CartProductDto> cartProductsDtos, Order order) {
        final var orderProducts = order.getProducts();
        cartProductsDtos.forEach(cartProductDto -> {
            final var product = productRepository.findById(cartProductDto.getProductDto().getId())
                    .orElseThrow(() -> new ResourceNotFoundException("Product", "ID", cartProductDto.getProductDto().getId()));

            final var orderProduct = new OrderProduct();
            orderProduct.setOrder(order);
            orderProduct.setProduct(product);
            orderProduct.setPrice(cartProductDto.getTotalPrice());
            orderProduct.setQuantity(cartProductDto.getQuantity());
            orderProducts.add(orderProduct);
        });
    }
}
