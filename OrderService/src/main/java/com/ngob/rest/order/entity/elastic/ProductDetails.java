package com.ngob.rest.order.entity.elastic;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This is the elastic product details class
 *
 * @author Mircea Stan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductDetails {

    private String id;
    private String name;
    private String description;
    private String category;
    private String subCategory;
    private Integer stockCount;
    private String brand;
    private Integer price;
    private Integer discountPrice;
    private String imageLocation;

}
