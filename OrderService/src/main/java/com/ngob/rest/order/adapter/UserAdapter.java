package com.ngob.rest.order.adapter;

import com.ngob.rest.order.entity.user.User;

/**
 * This class is an adapter of a user DTO instance
 */
public class UserAdapter {

    private UserAdapter() {
    }

    public static void enrichUser(User registerUserDto, User user) {
        user.setId(registerUserDto.getId());
        user.setEmail(registerUserDto.getEmail());
        user.setName(registerUserDto.getName());
        user.setUserPassword(registerUserDto.getUserPassword());
    }

}
