package com.ngob.rest.order.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

/**
 * This is the product entity class
 *
 * @author Mircea Stan
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "products")
public class Product {

    @Id
    @Column(updatable = false, unique = true)
    private UUID id;

    @NotBlank
    @Size(max = 255)
    private String name;

    @NotBlank
    @Size(max = 255)
    private String description;

    @NotBlank
    @Size(max = 255)
    private String category;

    @NotBlank
    @Size(max = 255)
    private String subCategory;

    @NotNull
    private Integer stockCount;

    @NotBlank
    @Size(max = 255)
    private String brand;

    @NotNull
    private Integer price;

    private Integer discountPrice;

    @NotBlank
    @Size(max = 255)
    private String imageLocation;

}
