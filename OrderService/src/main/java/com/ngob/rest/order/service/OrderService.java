package com.ngob.rest.order.service;

import com.ngob.rest.order.dto.APIResponseDto;
import com.ngob.rest.order.dto.order.OrderDto;
import com.ngob.rest.order.dto.order.OrderRequestDto;
import com.ngob.rest.order.dto.order.OrderResponseDto;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

/**
 * This is the order service interface
 *
 * @author Mircea Stan
 */
public interface OrderService {

    Set<OrderDto> getAllOrdersByUser(UUID userId);

    Set<OrderDto> getAllOrders();

    OrderResponseDto createOrder(OrderRequestDto orderRequestDto) throws IOException;

    APIResponseDto cancelOrder(UUID orderId) throws IOException;

    Long getOrdersCount();

    Set<OrderDto> getAllRecentOrders();
}
