package com.ngob.rest.order.dto.product;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * This is the cart product summary Dto class
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = false, of = "productDto")
public class CartProductSummaryDto {

    private ProductSummaryDto productDto;
    private Integer quantity;
    private BigDecimal totalPrice;

}
