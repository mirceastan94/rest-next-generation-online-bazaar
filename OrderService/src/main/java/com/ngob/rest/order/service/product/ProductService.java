package com.ngob.rest.order.service.product;

import com.ngob.rest.order.dto.product.CartProductDto;
import com.ngob.rest.order.entity.order.OrderProduct;
import com.ngob.rest.order.entity.Product;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.function.IntBinaryOperator;

public interface ProductService {

    List<Product> retrieveProductsByIDs(Set<UUID> productsUUIDs);

    void updateProductsStockCount(List<OrderProduct> products, IntBinaryOperator updateFunction);

    void updateUserProductsRelationship(UUID userId, Set<CartProductDto> inStockCartProductsDtos);
}
