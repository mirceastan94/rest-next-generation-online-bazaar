package com.ngob.rest.order.service.payment;

import com.paypal.orders.Order;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * This is the payment service interface
 *
 * @author Mircea Stan
 */
public interface PaymentService {

    Order createOrder(BigDecimal cartPrice) throws IOException;

    Order captureOrder(String orderId) throws IOException;

}
