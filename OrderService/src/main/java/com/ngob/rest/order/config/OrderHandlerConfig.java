package com.ngob.rest.order.config;

import com.ngob.rest.order.handler.OrderHandler;
import com.ngob.rest.order.handler.PaymentHandler;
import com.ngob.rest.order.handler.PersistenceHandler;
import com.ngob.rest.order.handler.StockHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * This is the Handler config class used for the Chain of Responsibility when dealing with orders processing
 */
@Configuration
public class OrderHandlerConfig {

    @Bean(name = "firstHandler")
    public OrderHandler createStockHandler() {
        return new StockHandler(createPaymentHandler());
    }

    @Bean
    public OrderHandler createPaymentHandler() {
        return new PaymentHandler(createPersistenceHandler());
    }

    @Bean
    public OrderHandler createPersistenceHandler() {
        return new PersistenceHandler();
    }

}
