package com.ngob.rest.order.service.user;

import com.ngob.rest.order.entity.user.User;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * This is the user service interface
 *
 * @author Mircea Stan
 */
public interface UserService {

    void registeredUser(final User registerUserDto);

    void loggedInUser(final Map<String, String>authenticationDetails);

    Boolean existsUser(String email);

    Optional<User> getUser(UUID id);

}
