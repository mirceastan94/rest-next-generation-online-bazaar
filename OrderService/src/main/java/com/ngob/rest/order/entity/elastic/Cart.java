package com.ngob.rest.order.entity.elastic;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * This is the elastic cart class
 *
 * @author Mircea Stan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Cart {

    List<Product> products = new ArrayList<>();

}
