package com.ngob.rest.order.dto.product;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

/**
 * This is the user product(s) update Dto class, used to signal which products a user bought within an offer
 *
 * @author Mircea Stan
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserProductsDto implements Serializable {

    @NotNull
    private UUID userId;

    @NotNull
    private Set<UUID> productsIds;

}