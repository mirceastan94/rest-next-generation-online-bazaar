package com.ngob.rest.order.handler;

import com.ngob.rest.order.dto.order.OrderRequestDto;
import com.ngob.rest.order.dto.order.OrderResponseDto;
import com.ngob.rest.order.dto.product.CartProductDto;
import com.ngob.rest.order.dto.product.ProductDto;
import com.ngob.rest.order.entity.Product;
import com.ngob.rest.order.service.product.ProductServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.ngob.rest.order.utils.Utils.OUT_OF_STOCK_PRODUCTS_FOUND;

/**
 * This is the stock handler class
 *
 * @author Mircea Stan
 */
@Slf4j
public class StockHandler extends OrderHandler {

    @Autowired
    private ProductServiceImpl productService;

    protected OrderHandler nextOrderHandler;

    public StockHandler(OrderHandler nextOrderHandler) {
        this.nextOrderHandler = nextOrderHandler;
    }

    @Override
    public void handleCurrentOperation(OrderRequestDto orderRequestDto, OrderResponseDto orderResponseDto) throws IOException {
        log.info("Initiating stock check...");

        final var cartProductsDtos = orderRequestDto.getCartProductsDtos();

        final var cartProducts = productService.retrieveProductsByIDs(cartProductsDtos.stream()
                .map(CartProductDto::getProductDto).map(ProductDto::getId).collect(Collectors.toSet()));
        final var inStockProducts = cartProducts.stream()
                .filter(product -> product.getStockCount() > 0)
                .collect(Collectors.toSet());
        final var outOfStockProductsIds = cartProducts.stream()
                .filter(product -> product.getStockCount() == 0)
                .map(Product::getId)
                .collect(Collectors.toSet());

        for (final CartProductDto currentCartProductDto : cartProductsDtos) {
            var productFromDB = inStockProducts.stream()
                    .filter(inStockProduct -> inStockProduct.getId().equals(currentCartProductDto.getProductDto().getId()))
                    .findFirst().orElse(null);
            if (Objects.nonNull(productFromDB) && productFromDB.getStockCount() > currentCartProductDto.getQuantity()) {
                orderResponseDto.getInStockCartProductsDtos().add(currentCartProductDto);
            } else {
                orderResponseDto.getOutOfStockCartProductsDtos().add(currentCartProductDto);
            }
        }

        final var outOfStockProductsDtos = cartProductsDtos
                .stream().filter(cartProductDto -> outOfStockProductsIds
                        .stream().anyMatch(inStockProductId -> inStockProductId.equals(cartProductDto.getProductDto().getId())))
                .collect(Collectors.toSet());
        orderResponseDto.getOutOfStockCartProductsDtos().addAll(outOfStockProductsDtos);

        if (orderResponseDto.getOutOfStockCartProductsDtos().isEmpty()) {
            log.info("Stock check completed, continuing with the next handler as no products are out of stock...");
            nextOrderHandler.handleCurrentOperation(orderRequestDto, orderResponseDto);
        } else {
            log.info("Out of stock products found, order processing flow interrupted!");
            orderResponseDto.setSuccess(false);
            orderResponseDto.setErrorMessage(OUT_OF_STOCK_PRODUCTS_FOUND);
        }
    }
}
