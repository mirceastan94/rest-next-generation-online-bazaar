package com.ngob.rest.order.service.email;

import com.ngob.rest.order.dto.APIResponseDto;
import com.ngob.rest.order.dto.mail.SendEmailDto;

import javax.mail.MessagingException;

/**
 * This is the interface of the Email service
 */
public interface EmailService {

    APIResponseDto sendOrderEmail(SendEmailDto emailDto) throws MessagingException;

}
