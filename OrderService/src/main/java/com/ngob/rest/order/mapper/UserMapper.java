package com.ngob.rest.order.mapper;

import com.ngob.rest.order.dto.user.PaymentCardDto;
import com.ngob.rest.order.dto.user.RoleDto;
import com.ngob.rest.order.dto.user.UserDetailsDto;
import com.ngob.rest.order.entity.user.*;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * This is the user mapper class
 *
 * @author Mircea Stan
 */
@Component
public class UserMapper extends ConfigurableMapper {

        @Override
        protected void configure(MapperFactory mapperFactory) {
                mapperFactory.classMap(UserDetailsDto.class, UserDetails.class).byDefault().customize(new CustomMapper<>() {
                        @Override
                        public void mapAtoB(UserDetailsDto userDetailsDto, UserDetails userDetails, MappingContext context) {
                                super.mapAtoB(userDetailsDto, userDetails, context);
                                if (Objects.nonNull(userDetailsDto.getPaymentCardDto()) && Objects.nonNull(userDetailsDto.getPaymentCardDto().getNumber())) {
                                        Set<PaymentCard> paymentCards = mapperFacade.mapAsSet(new HashSet<>(Collections.singletonList(userDetailsDto.getPaymentCardDto())),
                                                PaymentCard.class);
                                        paymentCards.forEach(paymentCard -> paymentCard.setUserDetails(userDetails));
                                        userDetails.getPaymentCards().clear();
                                        userDetails.getPaymentCards().addAll(paymentCards);
                                }
                                if (Objects.nonNull(userDetailsDto.getLanguage())) {
                                        userDetails.setLanguage(Language.valueOf(userDetailsDto.getLanguage()));
                                }
                        }

                        @Override
                        public void mapBtoA(UserDetails userDetails, UserDetailsDto userDetailsDto, MappingContext context) {
                                super.mapBtoA(userDetails, userDetailsDto, context);
                                if (Objects.nonNull(userDetails.getPaymentCards()) && !userDetails.getPaymentCards().isEmpty()) {
                                        var paymentCardDto = mapperFacade.map(userDetails.getPaymentCards().stream().findFirst().get(),
                                                PaymentCardDto.class);
                                        userDetailsDto.setPaymentCardDto(paymentCardDto);
                                }
                                userDetailsDto.setLanguage(userDetails.getLanguage().getLabel());
                        }
                }).register();

                mapperFactory.classMap(RoleDto.class, Role.class).byDefault().customize(new CustomMapper<>() {
                        @Override
                        public void mapAtoB(RoleDto roleDto, Role role, MappingContext context) {
                                role.setId(roleDto.getId());
                                role.setName(RoleName.valueOf(roleDto.getName()));
                        }

                        @Override
                        public void mapBtoA(Role role, RoleDto roleDto, MappingContext context) {
                                roleDto.setId(role.getId());
                                roleDto.setName(role.getName().name());
                        }
                }).register();

                mapperFactory.classMap(PaymentCardDto.class, PaymentCard.class).byDefault().register();
                mapperFactory.classMap(PaymentCard.class, PaymentCardDto.class).byDefault().register();
        }

}
