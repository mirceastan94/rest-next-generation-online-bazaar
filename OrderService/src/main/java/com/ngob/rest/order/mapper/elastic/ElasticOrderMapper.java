package com.ngob.rest.order.mapper.elastic;

import com.ngob.rest.order.dto.order.CartDto;
import com.ngob.rest.order.dto.order.OrderDto;
import com.ngob.rest.order.dto.order.OrderRequestDto;
import com.ngob.rest.order.dto.product.CartProductDto;
import com.ngob.rest.order.dto.product.CartProductSummaryDto;
import com.ngob.rest.order.dto.product.ProductDto;
import com.ngob.rest.order.dto.product.ProductSummaryDto;
import com.ngob.rest.order.dto.user.UserDto;
import com.ngob.rest.order.dto.user.UserSummaryDto;
import com.ngob.rest.order.entity.elastic.*;
import lombok.SneakyThrows;
import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

import java.util.*;

import static com.ngob.rest.order.utils.Utils.COMMA;

/**
 * This is the elastic order mapper class
 *
 * @author Mircea Stan
 */
@Component
public class ElasticOrderMapper extends ConfigurableMapper {

    @Override
    public void configure(MapperFactory mapperFactory) {
        mapperFactory.classMap(OrderRequestDto.class, Order.class)
                .field("totalCartPrice", "price")
                .field("userDto", "user")
                .customize(new CustomMapper<>() {
                    @Override
                    public void mapAtoB(OrderRequestDto orderRequestDto, Order order, MappingContext context) {
                        super.mapAtoB(orderRequestDto, order, context);
                        mapAsSet(orderRequestDto.getCartProductsDtos(), order.getCart());
                    }
                }).register();

        mapperFactory.classMap(UserDto.class, User.class).customize(new CustomMapper<>() {
                    @Override
                    public void mapAtoB(UserDto userDto, User user, MappingContext context) {
                        super.mapAtoB(userDto, user, context);
                        final var userDetailsDto = userDto.getUserDetailsDto();
                        user.setId(userDto.getId().toString());
                        user.setAddress(Objects.nonNull(userDetailsDto.getPlace())
                                ? userDetailsDto.getAddress().concat(COMMA).concat(userDetailsDto.getPlace())
                                : userDetailsDto.getAddress());
                    }
                })
                .byDefault().register();

        mapperFactory.classMap(ProductDto.class, ProductDetails.class).customize(new CustomMapper<>() {
                    @Override
                    public void mapAtoB(ProductDto productDto, ProductDetails productDetails, MappingContext context) {
                        super.mapAtoB(productDto, productDetails, context);
                        productDetails.setId(productDto.getId().toString());
                    }
                })
                .byDefault().register();

        mapperFactory.classMap(Order.class, OrderDto.class)
                .field("cart", "cartDto")
                .field("user", "userDto")
                .byDefault().register();

        mapperFactory.classMap(Cart.class, CartDto.class)
                .field("products", "cartProductDtos").register();

        mapperFactory.classMap(Product.class, CartProductSummaryDto.class)
                .field("productDetails", "productDto")
                .field("price", "totalPrice")
                .byDefault().register();

        mapperFactory.classMap(ProductDetails.class, ProductSummaryDto.class).customize(new CustomMapper<>() {
                    @SneakyThrows
                    @Override
                    public void mapAtoB(ProductDetails productDetails, ProductSummaryDto productSummaryDto, MappingContext context) {
                        super.mapAtoB(productDetails, productSummaryDto, context);
                        final var imageIS = getClass().getClassLoader().getResourceAsStream(productDetails.getImageLocation());
                        productSummaryDto.setImageData(Base64.getEncoder().encodeToString(imageIS.readAllBytes()));
                    }
                })
                .byDefault().register();

        mapperFactory.classMap(User.class, UserSummaryDto.class).byDefault().register();
    }

    public void mapAsSet(Set<CartProductDto> cartProductsDtos, Cart cart) {
        List<Product> products = new ArrayList<>();
        cartProductsDtos.forEach(cartProductDto -> {
            Product product = new Product();
            product.setProductDetails(this.map(cartProductDto.getProductDto(), ProductDetails.class));
            product.setQuantity(cartProductDto.getQuantity());
            product.setPrice(cartProductDto.getTotalPrice());
            products.add(product);
        });
        cart.getProducts().addAll(products);
    }

}
