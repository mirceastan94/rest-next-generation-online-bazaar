package com.ngob.rest.order.repository;

import com.ngob.rest.order.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * This is the product repository interface
 *
 * @author Mircea Stan
 */
public interface ProductRepository extends JpaRepository<Product, UUID> { }
