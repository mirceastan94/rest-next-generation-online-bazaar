package com.ngob.rest.order.entity.order;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * This is the order product join table embedded id class
 *
 * @author Mircea Stan
 */
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class OrderProductId implements Serializable {

    @Column(name = "order_id")
    private UUID orderId;

    @Column(name = "product_id")
    private UUID productId;

    @Override
    public boolean equals(Object comparedObj) {
        if (comparedObj == null || getClass() != comparedObj.getClass()) {
            return false;
        }

        if (this == comparedObj) {
            return true;
        }

        final var comparedOrderProductId = (OrderProductId) comparedObj;
        return Objects.equals(this.orderId, comparedOrderProductId.orderId)
                && Objects.equals(this.productId, comparedOrderProductId.productId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, productId);
    }

}
