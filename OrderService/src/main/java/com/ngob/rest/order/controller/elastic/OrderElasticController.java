package com.ngob.rest.order.controller.elastic;

import com.ngob.rest.order.dto.order.OrderDto;
import com.ngob.rest.order.service.elastic.OrderElasticService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

/**
 * This class is the controller for elastic order related operations
 */
@RestController
@RequestMapping("/es")
public class OrderElasticController {

    private final OrderElasticService orderElasticService;

    OrderElasticController(final OrderElasticService orderElasticService) {
        this.orderElasticService = orderElasticService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<OrderDto>> getAllOrders() throws IOException {
        return new ResponseEntity<>(orderElasticService.getAllOrders(), HttpStatus.OK);
    }

    @GetMapping("/{parameter}/{value}")
    public ResponseEntity<List<OrderDto>> getOrdersByParam(@PathVariable(value = "parameter") String searchedParameter,
                                                        @PathVariable(value = "value") String searchedValue)
            throws IOException {
        return new ResponseEntity<>(orderElasticService.getOrdersByParam(searchedParameter, searchedValue), HttpStatus.OK);
    }

}
