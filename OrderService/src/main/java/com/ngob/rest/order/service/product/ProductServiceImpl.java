package com.ngob.rest.order.service.product;

import com.ngob.rest.order.dto.product.CartProductDto;
import com.ngob.rest.order.dto.product.ProductDto;
import com.ngob.rest.order.dto.product.StockUpdateDto;
import com.ngob.rest.order.dto.product.UserProductsDto;
import com.ngob.rest.order.entity.order.OrderProduct;
import com.ngob.rest.order.entity.Product;
import com.ngob.rest.order.repository.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final RabbitTemplate rabbitTemplate;
    private ProductRepository productRepository;
    private Queue productsStockUpdateQueue;
    private Queue userProductsRelationshipQueue;

    @Override
    public List<Product> retrieveProductsByIDs(Set<UUID> productsUUIDs) {
        return productRepository.findAllById(productsUUIDs);
    }

    @Override
    public void updateProductsStockCount(List<OrderProduct> products, IntBinaryOperator updateFunction) {
        final var stockUpdateDtos = new HashSet<StockUpdateDto>();
        products.forEach(orderProduct -> {
            final var stockUpdateDto = new StockUpdateDto();
            stockUpdateDto.setProductId(orderProduct.getProduct().getId());
            stockUpdateDto.setProductStockCount(updateFunction.applyAsInt(orderProduct.getProduct().getStockCount(),
                    orderProduct.getQuantity()));
            stockUpdateDtos.add(stockUpdateDto);
        });
        rabbitTemplate.convertAndSend(productsStockUpdateQueue.getName(), stockUpdateDtos);
    }

    @Override
    public void updateUserProductsRelationship(UUID userId, Set<CartProductDto> inStockCartProductsDtos) {
        final Set<UUID> productsIds = inStockCartProductsDtos.stream().map(CartProductDto::getProductDto)
                .map(ProductDto::getId).collect(Collectors.toSet());
        rabbitTemplate.convertAndSend(userProductsRelationshipQueue.getName(), new UserProductsDto(userId, productsIds));
    }

}
