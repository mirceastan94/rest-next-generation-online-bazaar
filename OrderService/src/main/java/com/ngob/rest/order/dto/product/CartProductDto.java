package com.ngob.rest.order.dto.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * This is the cart product dto class
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = false, of = "productDto")
public class CartProductDto {

    @JsonProperty("product")
    private ProductDto productDto;
    private Integer quantity;
    private BigDecimal totalPrice;

}
