package com.ngob.rest.order.service.elastic;

import com.ngob.rest.order.dao.OrderElasticDao;
import com.ngob.rest.order.dto.order.OrderDto;
import com.ngob.rest.order.entity.elastic.Order;
import com.ngob.rest.order.mapper.elastic.ElasticOrderMapper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

/**
 * This is the elastic order service implementation class
 *
 * @author Mircea Stan
 */
@Service
public class OrderElasticServiceImpl implements OrderElasticService {

    private final ElasticOrderMapper elasticOrderMapper;
    private final OrderElasticDao orderElasticDao;

    OrderElasticServiceImpl(final ElasticOrderMapper elasticOrderMapper, final OrderElasticDao orderElasticDao) {
        this.elasticOrderMapper = elasticOrderMapper;
        this.orderElasticDao = orderElasticDao;
    }

    @Override
    @Scheduled(fixedRate = 60 * 60 * 1000, initialDelay = 0)
    public void initializeIndexStructure() throws IOException, URISyntaxException {
        this.orderElasticDao.initializeIndexStructure();
    }

    @Override
    public String indexOrder(Order order) throws IOException {
        return orderElasticDao.indexNewOrder(order);
    }

    @Override
    public List<OrderDto> getAllOrders() throws IOException {
        return elasticOrderMapper.mapAsList(orderElasticDao.findAllOrders(), OrderDto.class);
    }

    @Override
    public List<OrderDto> getOrdersByParam(String searchedParameter, String searchedValue) throws IOException {
        return elasticOrderMapper.mapAsList(orderElasticDao.findOrdersBy(searchedParameter, searchedValue), OrderDto.class);
    }

}
