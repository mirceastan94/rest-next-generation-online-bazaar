package com.ngob.rest.order.service.user;


import com.ngob.rest.order.entity.user.Role;
import com.ngob.rest.order.entity.user.RoleName;

import java.util.Optional;

/**
 * This is the role service interface
 *
 * @author Mircea Stan
 */
public interface RoleService {

    Optional<Role> findByName(RoleName roleName);

}
