package com.ngob.rest.order.handler;

import com.ngob.rest.order.dto.order.OrderRequestDto;
import com.ngob.rest.order.dto.order.OrderResponseDto;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.IOException;

/**
 * This is the order handler abstract class
 *
 * @author Mircea Stan
 */
@AllArgsConstructor
@NoArgsConstructor
public abstract class OrderHandler {

    protected OrderHandler nextOrderHandler;

    public abstract void handleCurrentOperation(OrderRequestDto orderRequestDto, OrderResponseDto orderResponseDto) throws IOException;

}
