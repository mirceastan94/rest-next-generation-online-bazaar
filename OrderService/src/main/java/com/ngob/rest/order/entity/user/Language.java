package com.ngob.rest.order.entity.user;

import lombok.Getter;

/**
 * This is the user language enum
 *
 * @author Mircea Stan
 */
@Getter
public enum Language {
    ENGLISH("en"),
    GERMAN("de"),
    ROMANIAN("ro");

    private String label;

    Language(String label) {
        this.label = label;
    }

}
