package com.ngob.rest.order.dto.order;

import com.ngob.rest.order.dto.product.CartProductSummaryDto;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * This is the cart Dto class
 */
@Getter
@Setter
public class CartDto {

    private List<CartProductSummaryDto> cartProductDtos = new ArrayList<>();

}
