package com.ngob.rest.order.mapper;

import com.ngob.rest.order.dto.product.ProductDto;
import com.ngob.rest.order.entity.Product;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

/**
 * This is the product mapper class
 *
 * @author Mircea Stan
 */
@Component
public class ProductMapper extends ConfigurableMapper {

    @Override
    protected void configure(MapperFactory mapperFactory) {
        mapperFactory.classMap(ProductDto.class, Product.class).byDefault().register();
        mapperFactory.classMap(Product.class, ProductDto.class).byDefault().register();
    }

}
