package com.ngob.rest.order.dto.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

/**
 * This is the user details Dto
 *
 * @author Mircea Stan
 */
@Getter
@Setter
@NoArgsConstructor
public class UserDetailsDto {

    private UUID id;
    private String phone;
    private String address;
    private String place;
    private String language;
    private String picture;
    private PaymentCardDto paymentCardDto;

}
