package com.ngob.rest.order.repository;


import com.ngob.rest.order.entity.order.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.UUID;

/**
 * This is the order repository interface
 *
 * @author Mircea Stan
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, UUID> {

    @Query(value = "SELECT * FROM orders o WHERE o.user_id = ?1 ORDER BY date_of_purchase DESC", nativeQuery = true)
    LinkedHashSet<Order> findAllByUser(UUID userId);

    LinkedHashSet<Order> findTop7ByDateOfPurchaseGreaterThanOrderByDateOfPurchaseDesc(LocalDateTime localDateTime);

}
