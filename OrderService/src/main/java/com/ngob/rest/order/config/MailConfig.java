package com.ngob.rest.order.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import java.nio.charset.StandardCharsets;

/**
 * This is the mail templates configuration class
 */
@Configuration
public class MailConfig {

    @Bean
    public ResourceBundleMessageSource createEmailMessageSource() {
        final var messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("i18n/orderEmail");
        messageSource.setDefaultEncoding(StandardCharsets.UTF_8.name());
        return messageSource;
    }

    @Bean
    public TemplateEngine createEmailTemplateEngine() {
        final var templateEngine = new SpringTemplateEngine();
        templateEngine.addTemplateResolver(createHTMLTemplateResolver());
        templateEngine.setTemplateEngineMessageSource(createEmailMessageSource());
        return templateEngine;
    }

    @Bean
    public ITemplateResolver createHTMLTemplateResolver() {
        final var templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setOrder(1);
        templateResolver.setPrefix("templates/");
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(TemplateMode.HTML);
        templateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
        templateResolver.setCacheable(false);
        return templateResolver;
    }

}
