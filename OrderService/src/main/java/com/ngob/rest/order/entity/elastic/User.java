package com.ngob.rest.order.entity.elastic;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This is the elastic user class
 *
 * @author Mircea Stan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {

    private String id;
    private String name;
    private String email;
    private String address;

}
