package com.ngob.rest.order.config;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;

/**
 * This is the RabbitMQ order config class
 */
@Configuration
public class RabbitMQOrderConfig implements RabbitListenerConfigurer {

    @Value("${queues.users.register.queue}")
    private String userRegisterQueue;

    @Value("${queues.users.login.queue}")
    private String userLoginQueue;

    @Value("${queues.products.retrieval.queue}")
    private String productRetrievalQueue;

    @Value("${queues.products.stock.update.queue}")
    private String productsStockUpdateQueue;

    @Value("${queues.user.products.relationship.queue}")
    private String userProductsRelationshipQueue;

    @Bean(name = "userRegisterQueue")
    public Queue createUserRegisterQueue() {
        return new Queue(userRegisterQueue);
    }

    @Bean(name = "userLoginQueue")
    public Queue createUserLoginQueue() {
        return new Queue(userLoginQueue);
    }

    @Bean(name = "productRetrievalQueue")
    public Queue createProductQueue() {
        return new Queue(productRetrievalQueue);
    }

    @Bean(name = "productsStockUpdateQueue")
    public Queue createProductsStockUpdateQueue() {
        return new Queue(productsStockUpdateQueue);
    }

    @Bean(name = "userProductsRelationshipQueue")
    public Queue createUserProductsRelationshipQueue() {
        return new Queue(userProductsRelationshipQueue);
    }

    @Bean
    public MappingJackson2MessageConverter orderMappingJackson2MessageConverter() {
        return new MappingJackson2MessageConverter();
    }

    @Bean
    public DefaultMessageHandlerMethodFactory orderHandlerMethodFactory() {
        DefaultMessageHandlerMethodFactory handlerMethodFactory = new DefaultMessageHandlerMethodFactory();
        handlerMethodFactory.setMessageConverter(orderMappingJackson2MessageConverter());
        return handlerMethodFactory;
    }

    @Override
    public void configureRabbitListeners(final RabbitListenerEndpointRegistrar rabbitListenerEndpointRegistrar) {
        rabbitListenerEndpointRegistrar.setMessageHandlerMethodFactory(orderHandlerMethodFactory());
    }
    @Bean
    public Jackson2JsonMessageConverter productJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

}
