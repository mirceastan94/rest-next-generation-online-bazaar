package com.ngob.rest.order.controller;

import com.ngob.rest.order.dto.APIResponseDto;
import com.ngob.rest.order.dto.mail.SendEmailDto;
import com.ngob.rest.order.dto.order.OrderDto;
import com.ngob.rest.order.dto.order.OrderRequestDto;
import com.ngob.rest.order.dto.order.OrderResponseDto;
import com.ngob.rest.order.service.OrderService;
import com.ngob.rest.order.service.email.EmailService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Set;
import java.util.UUID;

/**
 * This class is the controller for order related operations
 */
@RestController
@RequestMapping("/")
public class OrderController {

    final OrderService orderService;
    final EmailService emailService;

    public OrderController(OrderService orderService, EmailService emailService) {
        this.orderService = orderService;
        this.emailService = emailService;
    }

    @GetMapping("/list")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Set<OrderDto>> listAllOrders() {
        return ResponseEntity.ok(orderService.getAllOrders());
    }

    @GetMapping("/count")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Long> getOrdersCount() {
        return ResponseEntity.ok(orderService.getOrdersCount());
    }

    @GetMapping("/list/{userId}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Set<OrderDto>> listOrdersForUser(@PathVariable(value = "userId") UUID userId) {
        return ResponseEntity.ok(orderService.getAllOrdersByUser(userId));
    }

    @GetMapping("/list/recent")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Set<OrderDto>> listRecentOrders() {
        return ResponseEntity.ok(orderService.getAllRecentOrders());
    }

    @PostMapping("/generate")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<OrderResponseDto> generateNewOrder(@RequestBody @Valid OrderRequestDto orderRequestDto)
            throws IOException {
        return ResponseEntity.ok(orderService.createOrder(orderRequestDto));
    }

    @DeleteMapping("/cancel/{orderId}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<APIResponseDto> cancelOrder(@PathVariable(value = "orderId") UUID orderId)
            throws IOException {
        return ResponseEntity.ok(orderService.cancelOrder(orderId));
    }

    @PostMapping("/sendEmail")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<APIResponseDto> sendOrderConfirmationEmail(@Valid @RequestBody SendEmailDto emailDto)
            throws MessagingException {
        return ResponseEntity.ok(emailService.sendOrderEmail(emailDto));
    }

}
