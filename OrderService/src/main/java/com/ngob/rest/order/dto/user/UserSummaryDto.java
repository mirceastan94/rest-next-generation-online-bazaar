package com.ngob.rest.order.dto.user;

import lombok.Getter;
import lombok.Setter;

/**
 * This is the user summary Dto
 *
 * @author Mircea Stan
 */
@Getter
@Setter
public class UserSummaryDto {

    private String id;
    private String name;
    private String email;
    private String address;

}
