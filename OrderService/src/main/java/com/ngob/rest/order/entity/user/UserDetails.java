package com.ngob.rest.order.entity.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * This is the user details entity class
 *
 * @author Mircea Stan
 */
@NoArgsConstructor
@Entity
@Getter
@Setter
@Table(name = "user_details")
public class UserDetails implements Serializable {

    @Id
    @Column(updatable = false, unique = true)
    private UUID id;

    @OneToOne(mappedBy = "userDetails", cascade = CascadeType.ALL)
    private User user;

    @OneToMany(mappedBy = "userDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<PaymentCard> paymentCards = new HashSet<>();

    @Size(max = 255)
    private String phone;

    @Size(max = 255)
    private String address;

    @Size(max = 255)
    private String place;

    @Enumerated(EnumType.STRING)
    @Column(name = "lang")
    private Language language = Language.ENGLISH;

    public UserDetails(UUID id) {
        this.id = id;
    }

}
