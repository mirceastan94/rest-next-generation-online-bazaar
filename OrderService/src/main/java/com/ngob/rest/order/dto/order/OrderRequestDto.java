package com.ngob.rest.order.dto.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ngob.rest.order.dto.product.CartProductDto;
import com.ngob.rest.order.dto.user.UserDto;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Set;

/**
 * This is the Order Request Dto class
 */
@Getter
@Setter
public class OrderRequestDto {

    @NotEmpty
    @JsonProperty("cartProducts")
    private Set<CartProductDto> cartProductsDtos;

    @NotNull
    private BigDecimal totalCartPrice;

    @NotNull
    @JsonProperty("userProfile")
    private UserDto userDto;

}
