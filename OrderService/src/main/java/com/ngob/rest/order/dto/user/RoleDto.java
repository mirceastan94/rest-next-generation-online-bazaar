package com.ngob.rest.order.dto.user;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

/**
 * This is the role dto class
 *
 * @author Mircea Stan
 */
@Getter
@Setter
public class RoleDto {

    private UUID id;
    private String name;

}
