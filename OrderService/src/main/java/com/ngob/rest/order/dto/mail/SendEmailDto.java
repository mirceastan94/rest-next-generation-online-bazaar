package com.ngob.rest.order.dto.mail;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

/**
 * This is the email Dto class
 */
@Getter
@Setter
public class SendEmailDto {

    private UUID orderId;
    private String action;

}
