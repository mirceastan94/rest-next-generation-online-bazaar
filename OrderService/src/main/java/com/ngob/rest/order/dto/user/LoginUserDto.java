package com.ngob.rest.order.dto.user;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * This is the login request class
 *
 * @author Mircea Stan
 */
@Getter
@Setter
public class LoginUserDto {

    @NotBlank
    @Email
    private String email;

    @NotBlank
    private String password;

    private String language;

}
