package com.ngob.rest.order.service;

import com.ngob.rest.order.dao.OrderElasticDao;
import com.ngob.rest.order.dto.APIResponseDto;
import com.ngob.rest.order.dto.order.OrderDto;
import com.ngob.rest.order.dto.order.OrderRequestDto;
import com.ngob.rest.order.dto.order.OrderResponseDto;
import com.ngob.rest.order.dto.product.ProductDto;
import com.ngob.rest.order.entity.Product;
import com.ngob.rest.order.entity.order.Order;
import com.ngob.rest.order.entity.order.OrderProduct;
import com.ngob.rest.order.entity.order.OrderStatus;
import com.ngob.rest.order.exception.InternalServerErrorException;
import com.ngob.rest.order.handler.OrderHandler;
import com.ngob.rest.order.mapper.OrderMapper;
import com.ngob.rest.order.mapper.ProductMapper;
import com.ngob.rest.order.repository.OrderRepository;
import com.ngob.rest.order.repository.ProductRepository;
import com.ngob.rest.order.service.product.ProductService;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.ResourceNotFoundException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.function.IntBinaryOperator;

import static com.ngob.rest.order.utils.Utils.ID_LABEL;
import static com.ngob.rest.order.utils.Utils.LONDON_ZONE;

/**
 * This is the order service implementation class
 *
 * @author Mircea Stan
 */
@Service
@Transactional
@Log4j2
public class OrderServiceImpl implements OrderService {

    @Qualifier("firstHandler")
    @Autowired
    private OrderHandler orderHandler;

    private final ProductService productService;
    private final OrderMapper orderMapper;
    private final ProductMapper productMapper;
    private final OrderRepository orderRepository;
    private final OrderElasticDao orderElasticDao;
    private final ProductRepository productRepository;

    OrderServiceImpl(final ProductService productService, final OrderMapper orderMapper,
                     final ProductMapper productMapper, final OrderRepository orderRepository,
                     final OrderElasticDao orderElasticDao, final ProductRepository productRepository) {
        this.productService = productService;
        this.orderMapper = orderMapper;
        this.productMapper = productMapper;
        this.orderRepository = orderRepository;
        this.orderElasticDao = orderElasticDao;
        this.productRepository = productRepository;
    }

    @Override
    public Set<OrderDto> getAllOrdersByUser(UUID userId) {
        LinkedHashSet<Order> userOrders = orderRepository.findAllByUser(userId);
        final var orderDtos = new LinkedHashSet<OrderDto>();
        userOrders.forEach(order -> orderDtos.add(orderMapper.map(order, OrderDto.class)));
        return orderDtos;
    }

    @Override
    public Set<OrderDto> getAllOrders() {
        return new LinkedHashSet<>(orderMapper.mapAsSet(orderRepository.findAll(), OrderDto.class));
    }

    @Override
    public OrderResponseDto createOrder(final OrderRequestDto orderRequestDto) throws IOException {
        final var orderResponseDto = new OrderResponseDto();
        orderHandler.handleCurrentOperation(orderRequestDto, orderResponseDto);
        return orderResponseDto;
    }

    @RabbitListener(queues = "${queues.products.retrieval.queue}")
    public void receivedProduct(final ProductDto productDto) {
        log.info("Product received: {}", productDto);
        final var product = productRepository.findById(productDto.getId()).orElse(new Product());
        productMapper.map(productDto, product);
        productRepository.saveAndFlush(product);
        log.info("Product persisted in DB: {}", productDto);
    }

    /**
     * This method simulates order status updating after a specific timeframe
     */
    @Scheduled(cron = "0 0 10 * * *", zone = LONDON_ZONE)
    public void updateOrdersStatus() {
        log.info("Updating allOrders status...");
        final var allOrders = orderRepository.findAll();
        allOrders.stream()
                .filter(order -> !order.getStatus().getStatusName().equals(OrderStatus.COMPLETED.getStatusName()))
                .forEach(order -> {
                    if (order.getDateOfPurchase().plusDays(1).isBefore(LocalDateTime.now(ZoneId.of(LONDON_ZONE)))) {
                        order.setStatus(OrderStatus.PENDING);
                    }
                    if (order.getDateOfPurchase().plusDays(2).isBefore(LocalDateTime.now(ZoneId.of(LONDON_ZONE)))) {
                        order.setStatus(OrderStatus.COMPLETED);
                    }
                });
        orderRepository.saveAll(allOrders);
        log.info("Orders states updated");
    }

    public APIResponseDto cancelOrder(UUID orderId) throws IOException {
        log.info("Cancelling orderToCancel with ID '{}'", orderId);
        final var orderToCancel = orderRepository.findById(orderId).orElseThrow(() ->
                new ResourceNotFoundException("Order", "ID", orderId));
        final var elasticOrderToCancel = orderElasticDao.findOrdersBy(ID_LABEL, orderId.toString());
        if (Objects.isNull(elasticOrderToCancel) || elasticOrderToCancel.size() != 1) {
            throw new InternalServerErrorException("Elastic order missing or duplicated!");
        }
        orderToCancel.setStatus(OrderStatus.CANCELLED);
        orderRepository.save(orderToCancel);

        final var elasticOrder = elasticOrderToCancel.get(0);
        elasticOrder.setStatus(OrderStatus.CANCELLED);
        orderElasticDao.updateIndexedOrder(elasticOrder);

        // updates stock count and let products service know of the updated values
        List<OrderProduct> orderProducts = orderToCancel.getProducts();
        final IntBinaryOperator increaseStockCountOperator = Integer::sum;
        productService.updateProductsStockCount(orderProducts, increaseStockCountOperator);

        log.info("Order with ID '{}' has just been cancelled!", orderId);
        return new APIResponseDto(true, "Your order has been cancelled successfully!");
    }

    @Override
    public Long getOrdersCount() {
        return orderRepository.count();
    }

    @Override
    public Set<OrderDto> getAllRecentOrders() {
        final var orderDtos = new LinkedHashSet<OrderDto>();
        final LinkedHashSet<Order> userOrders = orderRepository
                .findTop7ByDateOfPurchaseGreaterThanOrderByDateOfPurchaseDesc(LocalDateTime.now().minusMonths(1));
        userOrders.forEach(order -> orderDtos.add(orderMapper.map(order, OrderDto.class)));
        return orderDtos;
    }

}
