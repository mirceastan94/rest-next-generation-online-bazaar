package com.ngob.rest.order.dto.order;

import com.ngob.rest.order.dto.user.UserSummaryDto;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * This is the Order Dto class
 */
@Getter
@Setter
public class OrderDto {

    @Id
    private String id;
    private String status;
    private BigDecimal price;
    private String dateOfPurchase;
    private CartDto cartDto;
    private UserSummaryDto userDto;

}
