package com.ngob.rest.order.entity.user;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.UUID;

/**
 * This is the user entity class
 *
 * @author Mircea Stan
 */
@Entity
@Getter
@Setter
@Table(name = "users")
public class User implements Serializable {

    @Id
    @Column(updatable = false, unique = true)
    private UUID id;

    @ManyToOne(cascade = CascadeType.MERGE, optional = false)
    @JoinColumn(name = "role_id")
    private Role role;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @PrimaryKeyJoinColumn
    private UserDetails userDetails;

    @NotBlank
    @Size(max = 255)
    private String name;

    @Size(max = 255)
    @Email
    private String previousEmail;

    @NotBlank
    @Size(max = 255)
    @Email
    private String email;

    @NotBlank
    @Size(max = 255)
    @Column(name = "password")
    private String userPassword;

}
