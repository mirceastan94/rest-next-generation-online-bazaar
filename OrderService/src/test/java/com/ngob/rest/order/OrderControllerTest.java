package com.ngob.rest.order;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ngob.rest.order.controller.OrderController;
import com.ngob.rest.order.dto.order.OrderRequestDto;
import com.ngob.rest.order.dto.order.OrderResponseDto;
import com.ngob.rest.order.dto.product.CartProductDto;
import com.ngob.rest.order.dto.user.UserDto;
import com.ngob.rest.order.service.OrderService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(OrderController.class)
public class OrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private OrderService orderService;

    @InjectMocks
    private OrderController orderController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(orderController).build();
    }

    @Test
    public void findAllOrders() throws Exception {
        when(orderService.getAllOrders()).thenReturn(new HashSet<>());

        mockMvc.perform(MockMvcRequestBuilders.get("/list")
                .with(jwt()))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"))
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void createOrder() throws Exception {

        Set<CartProductDto> cartProductDtos = new HashSet<>();
        cartProductDtos.add(new CartProductDto());

        final OrderRequestDto suppliedCreateOrderDto = new OrderRequestDto();
        suppliedCreateOrderDto.setTotalCartPrice(BigDecimal.TEN);
        suppliedCreateOrderDto.setUserDto(new UserDto());
        suppliedCreateOrderDto.setCartProductsDtos(cartProductDtos);

        when(orderService.createOrder(suppliedCreateOrderDto)).thenReturn(new OrderResponseDto());

        mockMvc.perform(MockMvcRequestBuilders.post("/generate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(suppliedCreateOrderDto))
                .with(jwt()))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
        verifyNoMoreInteractions(orderService);
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
