package com.ngob.rest.order;

import com.ngob.rest.order.dto.order.OrderDto;
import com.ngob.rest.order.entity.order.Order;
import com.ngob.rest.order.entity.order.OrderProduct;
import com.ngob.rest.order.entity.order.OrderStatus;
import com.ngob.rest.order.entity.user.User;
import com.ngob.rest.order.mapper.OrderMapper;
import com.ngob.rest.order.repository.OrderRepository;
import com.ngob.rest.order.service.OrderServiceImpl;
import org.elasticsearch.ResourceNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private OrderMapper orderMapper;

    @InjectMocks
    private OrderServiceImpl orderServiceImpl;

    @Test
    public void findAllOrdersTest() {
        final User firstSuppliedUser = new User();
        firstSuppliedUser.setName("testName");
        firstSuppliedUser.setEmail("testEmail@gmail.com");

        final User secondSuppliedUser = new User();
        secondSuppliedUser.setName("secondName");
        secondSuppliedUser.setEmail("secondEmail@gmail.com");

        final OrderProduct firstSuppliedOrderProduct = new OrderProduct();
        firstSuppliedOrderProduct.setPrice(BigDecimal.TEN);
        firstSuppliedOrderProduct.setQuantity(1);

        final OrderProduct secondSuppliedOrderProduct = new OrderProduct();
        secondSuppliedOrderProduct.setPrice(BigDecimal.ONE);
        secondSuppliedOrderProduct.setQuantity(2);

        final Order firstSuppliedOrder = new Order(UUID.randomUUID(), BigDecimal.TEN, LocalDateTime.now(),
                OrderStatus.CONFIRMED, firstSuppliedUser, Collections.singletonList(firstSuppliedOrderProduct));

        final Order secondSuppliedOrder = new Order(UUID.randomUUID(), BigDecimal.ONE, LocalDateTime.now(),
                OrderStatus.CONFIRMED, secondSuppliedUser, Collections.singletonList(secondSuppliedOrderProduct));

        final List<Order> suppliedOrders = List.of(firstSuppliedOrder, secondSuppliedOrder);

        Mockito.when(orderRepository.findAll()).thenReturn(suppliedOrders);

        final List<OrderDto> allOrders = new ArrayList<>(orderServiceImpl.getAllOrders());
        for (int currentOrderIndex = 0; currentOrderIndex < allOrders.size(); currentOrderIndex++) {
            final OrderDto currentOrderDto = allOrders.get(currentOrderIndex);
            final Order currentSuppliedOrder = suppliedOrders.get(currentOrderIndex);
            assertEquals(currentSuppliedOrder.getStatus().getStatusName(), currentOrderDto.getStatus());
            assertEquals(currentSuppliedOrder.getId().toString(), currentOrderDto.getId());
            assertEquals(currentSuppliedOrder.getDateOfPurchase().toString(), currentOrderDto.getDateOfPurchase());
            assertEquals(currentSuppliedOrder.getUser().getEmail(), currentOrderDto.getUserDto().getEmail());
        }
    }


    @Test(expected = ResourceNotFoundException.class)
    public void cancelNonExistentOrder() throws IOException {
        UUID orderId = UUID.randomUUID();

        Mockito.when(orderRepository.findById(orderId)).thenThrow(ResourceNotFoundException.class);

        orderServiceImpl.cancelOrder(orderId);
    }

}
