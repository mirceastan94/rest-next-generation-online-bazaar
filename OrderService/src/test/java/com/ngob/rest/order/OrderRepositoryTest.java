package com.ngob.rest.order;

import com.ngob.rest.order.entity.Product;
import com.ngob.rest.order.entity.order.Order;
import com.ngob.rest.order.entity.order.OrderProduct;
import com.ngob.rest.order.entity.order.OrderStatus;
import com.ngob.rest.order.entity.user.Role;
import com.ngob.rest.order.entity.user.RoleName;
import com.ngob.rest.order.entity.user.User;
import com.ngob.rest.order.repository.OrderRepository;
import com.ngob.rest.order.repository.ProductRepository;
import com.ngob.rest.order.repository.user.RoleRepository;
import com.ngob.rest.order.repository.user.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@DataJpaTest
@EnableAutoConfiguration(exclude = FlywayAutoConfiguration.class)
public class OrderRepositoryTest {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    User firstSuppliedUser;
    User secondSuppliedUser;

    @Before
    public void setUp() {
        final Role userRole = new Role(RoleName.ROLE_USER);
        roleRepository.save(userRole);

        firstSuppliedUser = new User();
        firstSuppliedUser.setId(UUID.fromString("7710fcfd-d3de-48f3-87b8-775a9a5de6a3"));
        firstSuppliedUser.setName("testName");
        firstSuppliedUser.setEmail("testEmail@gmail.com");
        firstSuppliedUser.setUserPassword("testPass");
        firstSuppliedUser.setRole(userRole);

        secondSuppliedUser = new User();
        secondSuppliedUser.setId(UUID.fromString("7710fcfd-d3de-48f3-87b8-775a9a5de6a1"));
        secondSuppliedUser.setName("secondName");
        secondSuppliedUser.setEmail("secondEmail@gmail.com");
        secondSuppliedUser.setUserPassword("testPass");
        secondSuppliedUser.setRole(userRole);

        userRepository.saveAll(Arrays.asList(firstSuppliedUser, secondSuppliedUser));

        final OrderProduct firstSuppliedOrderProduct = new OrderProduct();
        firstSuppliedOrderProduct.setPrice(BigDecimal.TEN);
        firstSuppliedOrderProduct.setQuantity(1);

        final OrderProduct secondSuppliedOrderProduct = new OrderProduct();
        secondSuppliedOrderProduct.setPrice(BigDecimal.ONE);
        secondSuppliedOrderProduct.setQuantity(2);

        final Order firstSuppliedOrder = new Order(UUID.randomUUID(), BigDecimal.TEN, LocalDateTime.now(),
                OrderStatus.CONFIRMED, firstSuppliedUser, Collections.singletonList(firstSuppliedOrderProduct));

        final Order secondSuppliedOrder = new Order(UUID.randomUUID(), BigDecimal.ONE, LocalDateTime.now(),
                OrderStatus.CONFIRMED, secondSuppliedUser, Collections.singletonList(secondSuppliedOrderProduct));

        final Product product = Product.builder()
                .id(UUID.randomUUID())
                .name("name")
                .brand("brand")
                .category("category")
                .subCategory("subCategory")
                .description("description")
                .imageLocation("imageLocation")
                .discountPrice(20)
                .price(30)
                .stockCount(200)
                .build();
        productRepository.saveAndFlush(product);

        firstSuppliedOrderProduct.setOrder(firstSuppliedOrder);
        firstSuppliedOrderProduct.setProduct(product);
        secondSuppliedOrderProduct.setOrder(secondSuppliedOrder);
        secondSuppliedOrderProduct.setProduct(product);

        List<Order> orders = Arrays.asList(firstSuppliedOrder, secondSuppliedOrder);
        orderRepository.saveAll(orders);
    }

    @After
    public void tearDown() {
        orderRepository.deleteAll();
    }

    @Test
    public void findAllOrders() {
        List<Order> orders = orderRepository.findAll();
        assertEquals(2, orders.size());
    }

    @Test
    public void findOrdersByUser() {
        Set<Order> orders = orderRepository.findAllByUser(firstSuppliedUser.getId());
        assertFalse(orders.isEmpty());
        assertEquals(1, orders.size());
    }

}
